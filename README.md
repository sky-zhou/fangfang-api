# fangfang-api

<div align=center>
<img src="https://img.shields.io/badge/golang-1.18-blue"/>
<img src="https://img.shields.io/badge/gin-1.8.1-lightBlue"/>
<img src="https://img.shields.io/badge/gorm-1.23.4-red"/>
</div>

#### 介绍

提供标准crud接口 api
告别基础crud代码，告别代码生成器

#### 软件架构

是基于 [Gin](https://github.com/gin-gonic/gin) 进行模块化设计的 API
框架，封装了常用的功能，使用简单，致力于进行快速的业务研发，同时增加了更多限制，约束项目组开发成员，规避混乱无序及自由随意的编码

#### 使用说明

1. clone 本项目到你的目录
   ```git clone https://gitee.com/sky-zhou/fangfang-api.git```
2. 修改config 中的配置文件 主要修改 mysql、redis链接配置
3. 执行命令
   ```
   go env -w GO111MODULE=on
   go env -w GOPROXY=https://goproxy.cn,direct
   go mod tidy
   go mod download
   ```
4. 启动项目
   ```go run main.go```
5. [重要] 使用postman 调用接口/api/system/ReloadRedisTable 加载缓存数据
   ![加载缓存](docs/images/postRedis.jpg)

#### 框架使用

1. 业务表单设计，可以通过sys_model复制表单进行表单的开发
2. 表单设计完成后，需要调用 /api/system/ReloadDbByTable、/api/system/ReloadRedisTable 来构建表单的配置字段信息
3. 可以使用所提供的标准接口对表单进行增删改查等操作
4. 文档地址 https://www.yuque.com/skyzhou-l9kvh/vk1wf4
#### swagger自动化API文档

1. 安装 swagger
   ```go get -u github.com/swaggo/swag/cmd/swag```
2. 生成API文档
   ```swag init```

#### 集成组件

1. 支持 [rate](https://golang.org/x/time/rate) 接口限流
1. 支持 [cors](https://github.com/rs/cors) 接口跨域
1. 支持 [Prometheus](https://github.com/prometheus/client_golang) 指标记录
1. 支持 [Swagger](https://github.com/swaggo/gin-swagger) 接口文档生成
1. 支持 trace 项目内部链路追踪
1. 支持 [pprof](https://github.com/gin-contrib/pprof) 性能剖析
1. 支持 errno 统一定义错误码
1. 支持 [zap](https://go.uber.org/zap) 日志收集
1. 支持 [viper](https://github.com/spf13/viper) 配置文件解析
1. 支持 [gorm](https://gorm.io/gorm) 数据库组件
1. 支持 [go-redis](https://github.com/go-redis/redis/v7) 组件
1. 支持 RESTful API 返回值规范
1. 支持 [cron](https://github.com/jakecoffman/cron) 定时任务，在后台可界面配置
1. 支持 [websocket](https://github.com/gorilla/websocket) 实时通讯

#### 特别感谢

1. https://github.com/flipped-aurora/gin-vue-admin
2. https://gorm.io/zh_CN/
3. https://github.com/gin-gonic/gin
4. https://github.com/xinliangnote/go-gin-api