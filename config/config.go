package config

import (
	"bytes"
	_ "embed"
	"io"
	"os"
	"path/filepath"
	"time"

	"gitee.com/sky-zhou/fangfang-api/pkg/env"
	"gitee.com/sky-zhou/fangfang-api/pkg/file"

	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

var config = new(Config)

type Config struct {
	Redis    Redis    `mapstructure:"redis" json:"redis" yaml:"redis"`          //redis配置文件
	Mysql    Mysql    `mapstructure:"mysql" json:"mysql" yaml:"mysql"`          //mysql配置
	System   System   `mapstructure:"system" json:"system" yaml:"system"`       //系统配置
	Language Language `mapstructure:"language" json:"language" yaml:"language"` //TODO:语言配置后期处理
	Hashids  Hashids  `mapstructure:"hashids" json:"hashids" yaml:"hashids"`    //hash密钥等
	Captcha  Captcha  `mapstructure:"captcha" json:"captcha" yaml:"captcha"`    //验证码
}

type Redis struct {
	DB           int    `mapstructure:"db" json:"db" yaml:"db"`                               // redis的哪个数据库
	Addr         string `mapstructure:"addr" json:"addr" yaml:"addr"`                         // 服务器地址:端口
	Password     string `mapstructure:"password" json:"password" yaml:"password"`             // 密码
	Maxretries   int    `mapstructure:"maxretries" json:"maxretries" yaml:"maxretries"`       // 最大尝试
	Minidleconns int    `mapstructure:"minidleconns" json:"minidleconns" yaml:"minidleconns"` // 最小连接数
	Poolsize     int    `mapstructure:"poolsize" json:"poolsize" yaml:"poolsize"`             // 缓冲大小
}

//mysql 读写分离
type Mysql struct {
	Base       Base  `mapstructure:"base" json:"base" yaml:"base"`
	Read       Read  `mapstructure:"read" json:"read" yaml:"read"`
	Write      Write `mapstructure:"write" json:"write" yaml:"write"`
	ColIsUpper bool  `mapstructure:"colIsUpper" json:"colIsUpper" yaml:"colIsUpper"` //数据字段名称是大写还是小写
}

type Base struct {
	MaxOpenConn     int           `mapstructure:"maxOpenConn" json:"maxOpenConn" yaml:"maxOpenConn"`             //连接池
	MaxIdleConn     int           `mapstructure:"maxIdleConn" json:"maxIdleConn" yaml:"maxIdleConn"`             //最大连接数
	ConnMaxLifeTime time.Duration `mapstructure:"connMaxLifeTime" json:"connMaxLifeTime" yaml:"connMaxLifeTime"` //链接超时时间
}

type Read struct {
	Addr string `mapstructure:"addr" json:"addr" yaml:"addr"`       //mysql地址
	User string `mapstructure:"user" json:"user" yaml:"user"`       //mysql用户
	Pass string `mapstructure:"passwd" json:"passwd" yaml:"passwd"` //mysql密码
	Name string `mapstructure:"dbname" json:"dbname" yaml:"dbname"` //mysql 数据库名称
}
type Write struct {
	Addr string `mapstructure:"addr" json:"addr" yaml:"addr"`       //mysql地址
	User string `mapstructure:"user" json:"user" yaml:"user"`       //mysql用户
	Pass string `mapstructure:"passwd" json:"passwd" yaml:"passwd"` //mysql密码
	Name string `mapstructure:"dbname" json:"dbname" yaml:"dbname"` //mysql 数据库名称
}

type System struct {
	Port          string `mapstructure:"port" json:"port" yaml:"port"`                            //服务开启端口
	IsNeedCaptcha bool   `mapstructure:"isNeedCaptcha" json:"isNeedCaptcha" yaml:"isNeedCaptcha"` //是否启用验证码
	IsCacheIncr   bool   `mapstructure:"isCacheIncr" json:"isCacheIncr" yaml:"isCacheIncr"`       //是否启用缓存生成id
	StoreModel    bool   `mapstructure:"StoreModel" json:"StoreModel" yaml:"StoreModel"`          //TODO:服务器存储方式后期实现
}

type Language struct {
	Local string `mapstructure:"local" json:"local" yaml:"local"` //TODO:多语言，后期实现
}

type Hashids struct {
	Length string `mapstructure:"length" json:"length" yaml:"length"` //hash长度
	Secret string `mapstructure:"secret" json:"secret" yaml:"secret"` //hash密钥
}
type Captcha struct {
	KeyLong   int `mapstructure:"key-long" json:"key-long" yaml:"key-long"`       // 验证码长度
	ImgWidth  int `mapstructure:"img-width" json:"img-width" yaml:"img-width"`    // 验证码宽度
	ImgHeight int `mapstructure:"img-height" json:"img-height" yaml:"img-height"` // 验证码高度
}

var (
	//go:embed configs_dev.yml
	devConfigs []byte

	//go:embed configs_test.yml
	testConfigs []byte

	//go:embed configs_fat.yml
	fatConfigs []byte

	//go:embed configs_pro.yml
	proConfigs []byte
)

func init() {
	var r io.Reader

	//根据环境变量或参数选择加载配置文件
	switch env.Active().Value() {
	case "dev":
		r = bytes.NewReader(devConfigs)
	case "test":
		r = bytes.NewReader(testConfigs)
	case "fat":
		r = bytes.NewReader(fatConfigs)
	case "pro":
		r = bytes.NewReader(proConfigs)
	default:
		r = bytes.NewReader(devConfigs)
	}

	viper.SetConfigType("yml")

	if err := viper.ReadConfig(r); err != nil {
		panic(err)
	}

	if err := viper.Unmarshal(config); err != nil {
		panic(err)
	}

	viper.SetConfigName("configs_" + env.Active().Value())
	viper.AddConfigPath("./config")

	configFile := "./config/configs_" + env.Active().Value() + ".yml"
	_, ok := file.IsExists(configFile)
	if !ok {
		if err := os.MkdirAll(filepath.Dir(configFile), 0766); err != nil {
			panic(err)
		}

		f, err := os.Create(configFile)
		if err != nil {
			panic(err)
		}
		defer f.Close()

		if err := viper.WriteConfig(); err != nil {
			panic(err)
		}
	}

	//监听配置文件
	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		if err := viper.Unmarshal(config); err != nil {
			panic(err)
		}
	})
}

//提供外部对config的访问
func Get() Config {
	return *config
}
