package config

import "time"

const (
	// ProjectName 项目名称
	ProjectName = "fangfang-api"

	// ProjectVersion 项目版本
	ProjectVersion = "v1.0.1"

	// ProjectAccessLogFile 项目访问日志存放文件
	ProjectAccessLogFile = "./logs/" + ProjectName + "-access.log"

	// ProjectCronLogFile 项目后台任务日志存放文件
	ProjectCronLogFile = "./logs/" + ProjectName + "-cron.log"

	// HeaderLoginToken 登录验证 Token，Header 中传递的参数
	HeaderLoginToken = "Token"

	// HeaderSignToken 签名验证 Authorization，Header 中传递的参数
	HeaderSignToken = "Authorization"

	// HeaderSignTokenDate 签名验证 Date，Header 中传递的参数
	HeaderSignTokenDate = "Authorization-Date"

	// HeaderSignTokenTimeout 签名有效期为 5 分钟
	HeaderSignTokenTimeout = time.Minute * 5

	// RedisKeyPrefixLoginUser Redis Key 前缀 - 登录用户信息
	RedisKeyPrefixLoginUser = ProjectName + ":login-user:"

	// RedisKeyPrefixUser Redis Key 前缀 - 用户路由
	RedisKeyPrefixUserMenu = ProjectName + ":user-menu:"

	// RedisKeyPrefixSignature Redis Key 前缀 - 签名验证信息
	RedisKeyPrefixSignature = ProjectName + ":signature:"

	// RedisKeySysData Redis Key 前缀 - 缓存数据信息
	RedisKeySysData = ProjectName + ":SysData:"

	// RedisKeySysTable Redis Key 前缀 - 缓存table配置
	RedisKeySysTable = ProjectName + ":SysTable:"

	// RedisKeySysTable Redis Key 前缀 - 缓存table配置
	RedisKeySysColumn = ProjectName + ":SysColumn:"

	// RedisKeySysTable Redis Key 前缀 - 缓存table配置
	RedisKeySysDict = ProjectName + ":SysDict:"

	// MaxRequestsPerSecond 每秒最大请求量
	MaxRequestsPerSecond = 10000

	// LoginSessionTTL 登录有效期为 24 小时
	LoginSessionTTL = time.Hour * 24

	//RedisCapchaKey 验证码缓存
	RedisCaptchaKey = ProjectName + ":Captcha:"

	// ZhCN 简体中文 - 中国
	ZhCN = "zh-cn"

	// EnUS 英文 - 美国
	EnUS = "en-us"
)
