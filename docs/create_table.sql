/*
Navicat MySQL Data Transfer

Source Server         : sky
Source Server Version : 50729
Source Host           : xh.skyzhou.cn:3306
Source Database       : fangfang_api

Target Server Type    : MYSQL
Target Server Version : 50729
File Encoding         : 65001

Date: 2022-07-23 09:07:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_authorized
-- ----------------------------
DROP TABLE IF EXISTS `sys_authorized`;
CREATE TABLE `sys_authorized` (
                                  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                                  `business_key` varchar(32) DEFAULT '' COMMENT '调用方key',
                                  `business_secret` varchar(60) DEFAULT '' COMMENT '调用方secret',
                                  `business_developer` varchar(60) DEFAULT '' COMMENT '调用方对接人',
                                  `remark` varchar(255) DEFAULT '' COMMENT '备注',
                                  `is_used` char(1) DEFAULT 'Y' COMMENT '是否启用 Y:是  N:否',
                                  `is_deleted` char(1) DEFAULT 'N' COMMENT '是否删除 N:否  Y:是',
                                  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                  `created_user` varchar(60) DEFAULT '' COMMENT '创建人',
                                  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                  `updated_user` varchar(60) DEFAULT '' COMMENT '更新人',
                                  PRIMARY KEY (`id`),
                                  UNIQUE KEY `unique_business_key` (`business_key`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='已授权的调用方表';

-- ----------------------------
-- Records of sys_authorized
-- ----------------------------
INSERT INTO `sys_authorized` VALUES ('1', 'pcweb', '12878dd962115106db6d', '管理员', '管理员', 'Y', 'N', '2022-07-06 03:41:18', 'admin', '2022-07-08 06:51:12', 'admin');

-- ----------------------------
-- Table structure for sys_authorized_api
-- ----------------------------
DROP TABLE IF EXISTS `sys_authorized_api`;
CREATE TABLE `sys_authorized_api` (
                                      `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                                      `business_key` varchar(32) NOT NULL DEFAULT '' COMMENT '调用方key',
                                      `method` varchar(30) NOT NULL DEFAULT '' COMMENT '请求方式',
                                      `api` varchar(100) NOT NULL DEFAULT '' COMMENT '请求地址',
                                      `is_deleted` char(1) NOT NULL DEFAULT 'N' COMMENT '是否删除 Y:是  N:否',
                                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                      `created_user` varchar(60) NOT NULL DEFAULT '' COMMENT '创建人',
                                      `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                      `updated_user` varchar(60) NOT NULL DEFAULT '' COMMENT '更新人',
                                      PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='已授权接口地址表';

-- ----------------------------
-- Records of sys_authorized_api
-- ----------------------------
INSERT INTO `sys_authorized_api` VALUES ('1', 'pcweb', 'POST', '/api/**', 'N', '2021-11-23 01:54:31', 'admin', '2022-07-19 09:36:01', 'admin');

-- ----------------------------
-- Table structure for sys_column
-- ----------------------------
DROP TABLE IF EXISTS `sys_column`;
CREATE TABLE `sys_column` (
                              `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                              `c_id` int(11) DEFAULT '1',
                              `sys_table_id` int(11) DEFAULT NULL COMMENT '所属表单',
                              `comment` varchar(255) DEFAULT NULL COMMENT '显示名称',
                              `mask` varchar(80) DEFAULT NULL COMMENT '读写规则(AMDQ)',
                              `orderno` int(10) DEFAULT NULL COMMENT '字段排序序号',
                              `dbname` varchar(255) DEFAULT NULL COMMENT '数据库名称',
                              `full_dbname` varchar(255) DEFAULT NULL COMMENT '数据库全名',
                              `coltype` varchar(255) DEFAULT NULL COMMENT '字段类型',
                              `is_ak` char(1) DEFAULT 'N' COMMENT '是否输入字段 Y:是 N:否',
                              `is_dk` char(1) DEFAULT 'N' COMMENT '是否显示字段 Y:是 N:否',
                              `is_null` char(1) DEFAULT 'Y' COMMENT '是否可以为空 Y:是 N:否',
                              `is_upper` char(1) DEFAULT 'N' COMMENT '是否大写 Y:是 N:否',
                              `is_pk` char(1) DEFAULT 'N' COMMENT '是否主键 Y:是 N:否',
                              `default_value` varchar(255) DEFAULT NULL COMMENT '默认值',
                              `sys_dict_name` varchar(80) DEFAULT NULL COMMENT '数据字典',
                              `fk_column_id` int(11) DEFAULT NULL COMMENT '外键关联',
                              `lenth` int(10) DEFAULT NULL COMMENT '长度',
                              `precision` int(10) DEFAULT NULL COMMENT '精度',
                              `hr_column_id` int(11) DEFAULT NULL COMMENT '所属字段组',
                              `display_type` tinyint(10) DEFAULT NULL COMMENT '显示类型',
                              `created_user` varchar(80) CHARACTER SET utf8 DEFAULT NULL COMMENT '创建人',
                              `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                              `updated_user` varchar(80) CHARACTER SET utf8 DEFAULT NULL COMMENT '更新人',
                              `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
                              `is_used` char(1) DEFAULT 'Y' COMMENT '是否启用 Y:是  N:否',
                              `is_deleted` char(1) DEFAULT 'N' COMMENT '是否删除 N未删 Y已删',
                              `deleted_user` varchar(80) DEFAULT NULL,
                              `deleted_at` timestamp NULL DEFAULT NULL COMMENT '删除时间',
                              PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=248 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='2字段配置表单';

-- ----------------------------
-- Records of sys_column
-- ----------------------------
INSERT INTO `sys_column` VALUES ('1', '1', '1', '主键', '11111111', '10', 'ID', 'SYS_TABLE.ID', 'int', 'N', 'N', 'N', 'N', 'N', '', null, '0', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:53', 'admin', '2022-07-09 16:46:53', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('2', '1', '1', '所属公司', '11111111', '20', 'C_ID', 'SYS_TABLE.C_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '@UserCid@', null, '59', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:53', 'admin', '2022-07-09 16:46:53', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('3', '1', '1', '是否启用 Y:是  N:否', '11111111', '30', 'IS_USED', 'SYS_TABLE.IS_USED', 'char', 'N', 'N', 'Y', 'N', 'N', 'Y', 'sys_is_used', '0', '1', '0', '0', '0', 'admin', '2022-07-09 16:46:53', 'admin', '2022-07-09 16:46:53', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('4', '1', '1', '是否删除 N未删 Y已删', '11111111', '40', 'IS_DELETED', 'SYS_TABLE.IS_DELETED', 'char', 'N', 'N', 'Y', 'N', 'N', 'N', 'sys_is_deleted', '0', '1', '0', '0', '0', 'admin', '2022-07-09 16:46:53', 'admin', '2022-07-09 16:46:53', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('5', '1', '1', '创建时间', '11111111', '50', 'CREATED_AT', 'SYS_TABLE.CREATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:53', 'admin', '2022-07-09 16:46:53', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('6', '1', '1', '创建人', '11111111', '60', 'CREATED_USER', 'SYS_TABLE.CREATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', null, '0', '60', '0', '0', '0', 'admin', '2022-07-09 16:46:53', 'admin', '2022-07-09 16:46:53', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('7', '1', '1', '更新时间', '11111111', '70', 'UPDATED_AT', 'SYS_TABLE.UPDATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:53', 'admin', '2022-07-09 16:46:53', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('8', '1', '1', '更新人', '11111111', '80', 'UPDATED_USER', 'SYS_TABLE.UPDATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', null, '0', '60', '0', '0', '0', 'admin', '2022-07-09 16:46:53', 'admin', '2022-07-09 16:46:53', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('9', '1', '1', '显示名称', '11111111', '90', 'DISNAME', 'SYS_TABLE.DISNAME', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:53', 'admin', '2022-07-09 16:46:53', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('10', '1', '1', '序号', '11111111', '100', 'ORDERNO', 'SYS_TABLE.ORDERNO', 'int', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:53', 'admin', '2022-07-09 16:46:53', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('11', '1', '1', '数据库表名称', '11111111', '110', 'NAME', 'SYS_TABLE.NAME', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('12', '1', '1', '主键', '11111111', '120', 'PK_COLUMN_ID', 'SYS_TABLE.PK_COLUMN_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '', null, '30', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('13', '1', '1', '实际数据库表', '11111111', '130', 'REL_TABLE_ID', 'SYS_TABLE.REL_TABLE_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '', null, '1', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('14', '1', '1', '过滤条件', '11111111', '140', 'FILTER', 'SYS_TABLE.FILTER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('15', '1', '1', '表单功能', '11111111', '150', 'TABLE_MASK', 'SYS_TABLE.TABLE_MASK', 'char', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('16', '1', '1', '显示健', '11111111', '160', 'AD_COLUMN_ID', 'SYS_TABLE.AD_COLUMN_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '', null, '30', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('17', '1', '1', '输入健', '11111111', '170', 'DK_COLUMN_ID', 'SYS_TABLE.DK_COLUMN_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '', null, '30', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('18', '1', '1', '网页链接', '11111111', '180', 'URL', 'SYS_TABLE.URL', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('19', '1', '1', '是否菜单 Y是N否', '11111111', '190', 'IS_MENU', 'SYS_TABLE.IS_MENU', 'char', 'N', 'N', 'Y', 'N', 'N', 'N', null, '0', '1', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('20', '1', '1', '安全目录', '11111111', '200', 'SYS_DIRECTORY_ID', 'SYS_TABLE.SYS_DIRECTORY_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'N', 'Y', 'admin', '2022-07-12 11:39:26');
INSERT INTO `sys_column` VALUES ('21', '1', '1', '父表', '11111111', '210', 'PARENT_TABLE_ID', 'SYS_TABLE.PARENT_TABLE_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '', null, '1', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('22', '1', '1', '新增后触发', '11111111', '220', 'TRIG_AC', 'SYS_TABLE.TRIG_AC', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('23', '1', '1', '修改后触发', '11111111', '230', 'TRIG_AM', 'SYS_TABLE.TRIG_AM', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('24', '1', '1', '删除前触发', '11111111', '240', 'TRIG_BM', 'SYS_TABLE.TRIG_BM', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('25', '1', '1', '提交程序', '11111111', '250', 'PROC_SUBMIT', 'SYS_TABLE.PROC_SUBMIT', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('26', '1', '1', '反提交程序', '11111111', '260', 'PROC_UNSUBMIT', 'SYS_TABLE.PROC_UNSUBMIT', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('27', '1', '1', '是否海量 N否Y海量', '11111111', '270', 'IS_BIG', 'SYS_TABLE.IS_BIG', 'char', 'N', 'N', 'Y', 'N', 'N', 'N', null, '0', '1', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('28', '1', '1', '扩展属性', '11111111', '280', 'PROPS', 'SYS_TABLE.PROPS', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '2550', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('29', '1', '1', '备注', '11111111', '290', 'COMMENTS', 'SYS_TABLE.COMMENTS', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('30', '1', '2', '主键', '11111111', '10', 'ID', 'SYS_COLUMN.ID', 'int', 'N', 'N', 'N', 'N', 'N', '', null, '0', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('31', '1', '2', '所属公司', '11111111', '20', 'C_ID', 'SYS_COLUMN.C_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '@UserCid@', null, '59', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('32', '1', '2', '所属表单', '11111111', '30', 'SYS_TABLE_ID', 'SYS_COLUMN.SYS_TABLE_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '', null, '1', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('33', '1', '2', '显示名称', '11111111', '40', 'COMMENT', 'SYS_COLUMN.COMMENT', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('34', '1', '2', '读写规则(CRUD)', '11111111', '50', 'MASK', 'SYS_COLUMN.MASK', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '80', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('35', '1', '2', '字段排序序号', '11111111', '60', 'ORDERNO', 'SYS_COLUMN.ORDERNO', 'int', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('36', '1', '2', '数据库名称', '11111111', '70', 'DBNAME', 'SYS_COLUMN.DBNAME', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('37', '1', '2', '数据库全名', '11111111', '80', 'FULL_DBNAME', 'SYS_COLUMN.FULL_DBNAME', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('38', '1', '2', '字段类型', '11111111', '90', 'COLTYPE', 'SYS_COLUMN.COLTYPE', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('39', '1', '2', '是否输入字段 Y:是 N:否', '11111111', '100', 'IS_AK', 'SYS_COLUMN.IS_AK', 'char', 'N', 'N', 'Y', 'N', 'N', 'N', null, '0', '1', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('40', '1', '2', '是否显示字段 Y:是 N:否', '11111111', '110', 'IS_DK', 'SYS_COLUMN.IS_DK', 'char', 'N', 'N', 'Y', 'N', 'N', 'N', null, '0', '1', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('41', '1', '2', '是否可以为空 Y:是 N:否', '11111111', '120', 'IS_NULL', 'SYS_COLUMN.IS_NULL', 'char', 'N', 'N', 'Y', 'N', 'N', 'Y', null, '0', '1', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('42', '1', '2', '是否大写 Y:是 N:否', '11111111', '130', 'IS_UPPER', 'SYS_COLUMN.IS_UPPER', 'char', 'N', 'N', 'Y', 'N', 'N', 'N', null, '0', '1', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('43', '1', '2', '是否主键 Y:是 N:否', '11111111', '140', 'IS_PK', 'SYS_COLUMN.IS_PK', 'char', 'N', 'N', 'Y', 'N', 'N', 'N', null, '0', '1', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('44', '1', '2', '默认值', '11111111', '150', 'DEFAULT_VALUE', 'SYS_COLUMN.DEFAULT_VALUE', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:54', 'admin', '2022-07-09 16:46:54', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('45', '1', '2', '数据字典', '11111111', '160', 'SYS_DICT_ID', 'SYS_COLUMN.SYS_DICT_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '', null, '109', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('46', '1', '2', '外键关联', '11111111', '170', 'FK_COLUMN_ID', 'SYS_COLUMN.FK_COLUMN_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '', null, '30', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('47', '1', '2', '长度', '11111111', '180', 'LENTH', 'SYS_COLUMN.LENTH', 'int', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('48', '1', '2', '精度', '11111111', '190', 'PRECISION', 'SYS_COLUMN.PRECISION', 'int', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('49', '1', '2', '所属字段组', '11111111', '200', 'HR_COLUMN_ID', 'SYS_COLUMN.HR_COLUMN_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '', null, '30', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('50', '1', '2', '显示类型', '11111111', '210', 'DISPLAY_TYPE', 'SYS_COLUMN.DISPLAY_TYPE', 'char', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '3', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('51', '1', '2', '创建人', '11111111', '220', 'CREATED_USER', 'SYS_COLUMN.CREATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', null, '0', '80', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('52', '1', '2', '创建时间', '11111111', '230', 'CREATED_AT', 'SYS_COLUMN.CREATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('53', '1', '2', '更新人', '11111111', '240', 'UPDATED_USER', 'SYS_COLUMN.UPDATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', null, '0', '80', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('54', '1', '2', '更新时间', '11111111', '250', 'UPDATED_AT', 'SYS_COLUMN.UPDATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('55', '1', '2', '是否启用 Y:是  N:否', '11111111', '260', 'IS_USED', 'SYS_COLUMN.IS_USED', 'char', 'N', 'N', 'Y', 'N', 'N', 'Y', 'sys_is_used', '0', '1', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('56', '1', '2', '是否删除 N未删 Y已删', '11111111', '270', 'IS_DELETED', 'SYS_COLUMN.IS_DELETED', 'char', 'N', 'N', 'Y', 'N', 'N', 'N', 'sys_is_deleted', '0', '1', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('57', '1', '2', '删除人', '11111111', '280', 'DELETED_USER', 'SYS_COLUMN.DELETED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', null, '0', '80', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('58', '1', '2', '删除时间', '11111111', '290', 'DELETED_AT', 'SYS_COLUMN.DELETED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('59', '1', '3', '主键', '11111111', '10', 'ID', 'SYS_COMPANY.ID', 'int', 'N', 'N', 'N', 'N', 'N', '', null, '0', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('60', '1', '3', '是否启用 Y:是  N:否', '11111111', '20', 'IS_USED', 'SYS_COMPANY.IS_USED', 'char', 'N', 'N', 'N', 'N', 'N', 'Y', 'sys_is_used', '0', '3', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('61', '1', '3', '是否删除 N未删 Y已删', '11111111', '30', 'IS_DELETED', 'SYS_COMPANY.IS_DELETED', 'char', 'N', 'N', 'N', 'N', 'N', 'N', 'sys_is_deleted', '0', '3', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('62', '1', '3', '创建时间', '11111111', '40', 'CREATED_AT', 'SYS_COMPANY.CREATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('63', '1', '3', '创建人', '11111111', '50', 'CREATED_USER', 'SYS_COMPANY.CREATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', null, '0', '80', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('64', '1', '3', '更新时间', '11111111', '60', 'UPDATED_AT', 'SYS_COMPANY.UPDATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('65', '1', '3', '更新人', '11111111', '70', 'UPDATED_USER', 'SYS_COMPANY.UPDATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', null, '0', '80', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('66', '1', '3', '删除时间', '11111111', '80', 'DELETED_AT', 'SYS_COMPANY.DELETED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('67', '1', '3', '删除人', '11111111', '90', 'DELETED_USER', 'SYS_COMPANY.DELETED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', null, '0', '80', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('68', '1', '3', '公司编号', '11111111', '100', 'CODE', 'SYS_COMPANY.CODE', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '80', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('69', '1', '3', '公司名称', '11111111', '110', 'NAME', 'SYS_COMPANY.NAME', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('70', '1', '4', '主键', '11111111', '10', 'ID', 'SYS_CRON_TASK.ID', 'int', 'N', 'N', 'N', 'N', 'N', '', null, '0', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('71', '1', '4', '任务名称', '11111111', '20', 'NAME', 'SYS_CRON_TASK.NAME', 'varchar', 'N', 'N', 'N', 'N', 'N', '', null, '0', '80', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('72', '1', '4', 'crontab 表达式', '11111111', '30', 'SPEC', 'SYS_CRON_TASK.SPEC', 'varchar', 'N', 'N', 'N', 'N', 'N', '', null, '0', '80', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('73', '1', '4', '执行命令', '11111111', '40', 'COMMAND', 'SYS_CRON_TASK.COMMAND', 'varchar', 'N', 'N', 'N', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:55', 'admin', '2022-07-09 16:46:55', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('74', '1', '4', '执行方式 1:shell 2:http', '11111111', '50', 'PROTOCOL', 'SYS_CRON_TASK.PROTOCOL', 'tinyint', 'N', 'N', 'N', 'N', 'N', '1', null, '0', '3', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('75', '1', '4', 'http 请求方式 1:get 2:post', '11111111', '60', 'HTTP_METHOD', 'SYS_CRON_TASK.HTTP_METHOD', 'tinyint', 'N', 'N', 'N', 'N', 'N', '1', null, '0', '3', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('76', '1', '4', '超时时间(单位:秒)', '11111111', '70', 'TIMEOUT', 'SYS_CRON_TASK.TIMEOUT', 'int', 'N', 'N', 'N', 'N', 'N', '60', null, '0', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('77', '1', '4', '重试次数', '11111111', '80', 'RETRY_TIMES', 'SYS_CRON_TASK.RETRY_TIMES', 'tinyint', 'N', 'N', 'N', 'N', 'N', '3', null, '0', '3', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('78', '1', '4', '重试间隔(单位:秒)', '11111111', '90', 'RETRY_INTERVAL', 'SYS_CRON_TASK.RETRY_INTERVAL', 'int', 'N', 'N', 'N', 'N', 'N', '60', null, '0', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('79', '1', '4', '执行结束是否通知 1:不通知 2:失败通知 3:结束通知 4:结果关键字匹配通知', '11111111', '100', 'NOTIFY_STATUS', 'SYS_CRON_TASK.NOTIFY_STATUS', 'tinyint', 'N', 'N', 'N', 'N', 'N', '0', null, '0', '3', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('80', '1', '4', '通知类型 1:邮件 2:webhook', '11111111', '110', 'NOTIFY_TYPE', 'SYS_CRON_TASK.NOTIFY_TYPE', 'tinyint', 'N', 'N', 'N', 'N', 'N', '1', null, '0', '3', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('81', '1', '4', '通知者邮箱地址(多个用,分割)', '11111111', '120', 'NOTIFY_RECEIVER_EMAIL', 'SYS_CRON_TASK.NOTIFY_RECEIVER_EMAIL', 'varchar', 'N', 'N', 'N', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('82', '1', '4', '通知匹配关键字(多个用,分割)', '11111111', '130', 'NOTIFY_KEYWORD', 'SYS_CRON_TASK.NOTIFY_KEYWORD', 'varchar', 'N', 'N', 'N', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('83', '1', '4', '备注', '11111111', '140', 'REMARK', 'SYS_CRON_TASK.REMARK', 'varchar', 'N', 'N', 'N', 'N', 'N', '', null, '0', '100', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('84', '1', '4', '是否启用 Y:是  N:否', '11111111', '150', 'IS_USED', 'SYS_CRON_TASK.IS_USED', 'char', 'N', 'N', 'N', 'N', 'N', 'Y', 'sys_is_used', '0', '3', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('85', '1', '4', '创建时间', '11111111', '160', 'CREATED_AT', 'SYS_CRON_TASK.CREATED_AT', 'timestamp', 'N', 'N', 'N', 'N', 'N', 'CURRENT_TIMESTAMP', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('86', '1', '4', '创建人', '11111111', '170', 'CREATED_USER', 'SYS_CRON_TASK.CREATED_USER', 'varchar', 'N', 'N', 'N', 'N', 'N', '@UserName@', null, '0', '60', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('87', '1', '4', '更新时间', '11111111', '180', 'UPDATED_AT', 'SYS_CRON_TASK.UPDATED_AT', 'timestamp', 'N', 'N', 'N', 'N', 'N', 'CURRENT_TIMESTAMP', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('88', '1', '4', '更新人', '11111111', '190', 'UPDATED_USER', 'SYS_CRON_TASK.UPDATED_USER', 'varchar', 'N', 'N', 'N', 'N', 'N', '@UserName@', null, '0', '60', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('89', '1', '5', '主键', '11111111', '10', 'ID', 'SYS_AUTHORIZED_API.ID', 'int', 'N', 'N', 'N', 'N', 'N', '', null, '0', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('90', '1', '5', '调用方key', '11111111', '20', 'BUSINESS_KEY', 'SYS_AUTHORIZED_API.BUSINESS_KEY', 'varchar', 'N', 'N', 'N', 'N', 'N', '', null, '0', '32', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('91', '1', '5', '请求方式', '11111111', '30', 'METHOD', 'SYS_AUTHORIZED_API.METHOD', 'varchar', 'N', 'N', 'N', 'N', 'N', '', null, '0', '30', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('92', '1', '5', '请求地址', '11111111', '40', 'API', 'SYS_AUTHORIZED_API.API', 'varchar', 'N', 'N', 'N', 'N', 'N', '', null, '0', '100', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('93', '1', '5', '是否删除 N未删 Y已删', '11111111', '50', 'IS_DELETED', 'SYS_AUTHORIZED_API.IS_DELETED', 'char', 'N', 'N', 'N', 'N', 'N', 'N', 'sys_is_deleted', '0', '1', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('94', '1', '5', '创建时间', '11111111', '60', 'CREATED_AT', 'SYS_AUTHORIZED_API.CREATED_AT', 'timestamp', 'N', 'N', 'N', 'N', 'N', 'CURRENT_TIMESTAMP', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('95', '1', '5', '创建人', '11111111', '70', 'CREATED_USER', 'SYS_AUTHORIZED_API.CREATED_USER', 'varchar', 'N', 'N', 'N', 'N', 'N', '@UserName@', null, '0', '60', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('96', '1', '5', '更新时间', '11111111', '80', 'UPDATED_AT', 'SYS_AUTHORIZED_API.UPDATED_AT', 'timestamp', 'N', 'N', 'N', 'N', 'N', 'CURRENT_TIMESTAMP', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('97', '1', '5', '更新人', '11111111', '90', 'UPDATED_USER', 'SYS_AUTHORIZED_API.UPDATED_USER', 'varchar', 'N', 'N', 'N', 'N', 'N', '@UserName@', null, '0', '60', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('98', '1', '6', '主键', '11111111', '10', 'ID', 'SYS_AUTHORIZED.ID', 'int', 'N', 'N', 'N', 'N', 'N', '', null, '0', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('99', '1', '6', '调用方key', '11111111', '20', 'BUSINESS_KEY', 'SYS_AUTHORIZED.BUSINESS_KEY', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '32', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('100', '1', '6', '调用方secret', '11111111', '30', 'BUSINESS_SECRET', 'SYS_AUTHORIZED.BUSINESS_SECRET', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '60', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('101', '1', '6', '调用方对接人', '11111111', '40', 'BUSINESS_DEVELOPER', 'SYS_AUTHORIZED.BUSINESS_DEVELOPER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '60', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('102', '1', '6', '备注', '11111111', '50', 'REMARK', 'SYS_AUTHORIZED.REMARK', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('103', '1', '6', '是否启用 Y:是  N:否', '11111111', '60', 'IS_USED', 'SYS_AUTHORIZED.IS_USED', 'char', 'N', 'N', 'Y', 'N', 'N', 'Y', 'sys_is_used', '0', '1', '0', '0', '0', 'admin', '2022-07-09 16:46:56', 'admin', '2022-07-09 16:46:56', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('104', '1', '6', '是否删除 N未删 Y已删', '11111111', '70', 'IS_DELETED', 'SYS_AUTHORIZED.IS_DELETED', 'char', 'N', 'N', 'Y', 'N', 'N', 'N', 'sys_is_deleted', '0', '1', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('105', '1', '6', '创建时间', '11111111', '80', 'CREATED_AT', 'SYS_AUTHORIZED.CREATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('106', '1', '6', '创建人', '11111111', '90', 'CREATED_USER', 'SYS_AUTHORIZED.CREATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', null, '0', '60', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('107', '1', '6', '更新时间', '11111111', '100', 'UPDATED_AT', 'SYS_AUTHORIZED.UPDATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('108', '1', '6', '更新人', '11111111', '110', 'UPDATED_USER', 'SYS_AUTHORIZED.UPDATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', null, '0', '60', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('109', '1', '7', '主键', '11111111', '10', 'ID', 'SYS_DICT.ID', 'int', 'N', 'N', 'N', 'N', 'N', '', null, '0', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('110', '1', '7', '所属公司', '11111111', '20', 'C_ID', 'SYS_DICT.C_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '@UserCid@', null, '59', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('111', '1', '7', '是否启用 Y:是  N:否', '11111111', '30', 'IS_USED', 'SYS_DICT.IS_USED', 'char', 'N', 'N', 'Y', 'N', 'N', 'Y', 'sys_is_used', '0', '3', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('112', '1', '7', '是否删除 N未删 Y已删', '11111111', '40', 'IS_DELETED', 'SYS_DICT.IS_DELETED', 'char', 'N', 'N', 'Y', 'N', 'N', 'N', 'sys_is_deleted', '0', '3', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('113', '1', '7', '创建时间', '11111111', '50', 'CREATED_AT', 'SYS_DICT.CREATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('114', '1', '7', '创建人', '11111111', '60', 'CREATED_USER', 'SYS_DICT.CREATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', null, '0', '80', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('115', '1', '7', '更新时间', '11111111', '70', 'UPDATED_AT', 'SYS_DICT.UPDATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('116', '1', '7', '更新人', '11111111', '80', 'UPDATED_USER', 'SYS_DICT.UPDATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', null, '0', '80', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('117', '1', '7', '删除时间', '11111111', '90', 'DELETED_AT', 'SYS_DICT.DELETED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('118', '1', '7', '删除人', '11111111', '100', 'DELETED_USER', 'SYS_DICT.DELETED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', null, '0', '80', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('119', '1', '7', '字典名称', '11111111', '110', 'DICT_NAME', 'SYS_DICT.DICT_NAME', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('120', '1', '7', '字段类型', '11111111', '120', 'DICT_TYPE', 'SYS_DICT.DICT_TYPE', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('121', '1', '7', '备注', '11111111', '130', 'DESCRIPTION', 'SYS_DICT.DESCRIPTION', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('122', '1', '8', '主键', '11111111', '10', 'ID', 'SYS_DICTITEM.ID', 'int', 'N', 'N', 'N', 'N', 'N', '', null, '0', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('123', '1', '8', '是否启用 Y:是  N:否', '11111111', '20', 'IS_USED', 'SYS_DICTITEM.IS_USED', 'char', 'N', 'N', 'Y', 'N', 'N', 'Y', 'sys_is_used', '0', '3', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('124', '1', '8', '是否删除 N未删 Y已删', '11111111', '30', 'IS_DELETED', 'SYS_DICTITEM.IS_DELETED', 'char', 'N', 'N', 'Y', 'N', 'N', 'N', 'sys_is_deleted', '0', '3', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('125', '1', '8', '创建时间', '11111111', '40', 'CREATED_AT', 'SYS_DICTITEM.CREATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('126', '1', '8', '创建人', '11111111', '50', 'CREATED_USER', 'SYS_DICTITEM.CREATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', null, '0', '80', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('127', '1', '8', '更新时间', '11111111', '60', 'UPDATED_AT', 'SYS_DICTITEM.UPDATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('128', '1', '8', '更新人', '11111111', '70', 'UPDATED_USER', 'SYS_DICTITEM.UPDATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', null, '0', '80', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('129', '1', '8', '删除时间', '11111111', '80', 'DELETED_AT', 'SYS_DICTITEM.DELETED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('130', '1', '8', '删除人', '11111111', '90', 'DELETED_USER', 'SYS_DICTITEM.DELETED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', null, '0', '80', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('131', '1', '8', '所属数据字典', '11111111', '100', 'SYS_DICT_ID', 'SYS_DICTITEM.SYS_DICT_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '', null, '109', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('132', '1', '8', '序号', '11111111', '110', 'ORDERNO', 'SYS_DICTITEM.ORDERNO', 'int', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('133', '1', '8', '字典标签', '11111111', '120', 'LABEL', 'SYS_DICTITEM.LABEL', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:57', 'admin', '2022-07-09 16:46:57', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('134', '1', '8', '字典值', '11111111', '130', 'VALUE', 'SYS_DICTITEM.VALUE', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('135', '1', '8', '单对象样式', '11111111', '140', 'CSS_CLASS', 'SYS_DICTITEM.CSS_CLASS', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('136', '1', '8', '列表样式', '11111111', '150', 'LIST_CLASS', 'SYS_DICTITEM.LIST_CLASS', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('137', '1', '8', '是否默认(N:非默认 Y:默认)', '11111111', '160', 'IS_DEFAULT', 'SYS_DICTITEM.IS_DEFAULT', 'char', 'N', 'N', 'Y', 'N', 'N', 'N', null, '0', '1', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('138', '1', '8', '备注', '11111111', '170', 'DESCRIPTION', 'SYS_DICTITEM.DESCRIPTION', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('139', '1', '9', '主键', '11111111', '10', 'ID', 'SYS_MODEL.ID', 'int', 'N', 'N', 'N', 'N', 'N', '', null, '0', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('140', '1', '9', '所属公司', '11111111', '20', 'C_ID', 'SYS_MODEL.C_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '@UserCid@', null, '59', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('141', '1', '9', '是否启用 Y:是  N:否', '11111111', '30', 'IS_USED', 'SYS_MODEL.IS_USED', 'char', 'N', 'N', 'Y', 'N', 'N', 'Y', 'sys_is_used', '0', '3', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('142', '1', '9', '是否删除 N未删 Y已删', '11111111', '40', 'IS_DELETED', 'SYS_MODEL.IS_DELETED', 'char', 'N', 'N', 'Y', 'N', 'N', 'N', 'sys_is_deleted', '0', '3', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('143', '1', '9', '创建时间', '11111111', '50', 'CREATED_AT', 'SYS_MODEL.CREATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('144', '1', '9', '创建人', '11111111', '60', 'CREATED_USER', 'SYS_MODEL.CREATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', null, '0', '80', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('145', '1', '9', '更新时间', '11111111', '70', 'UPDATED_AT', 'SYS_MODEL.UPDATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('146', '1', '9', '更新人', '11111111', '80', 'UPDATED_USER', 'SYS_MODEL.UPDATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', null, '0', '80', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('147', '1', '9', '删除时间', '11111111', '90', 'DELETED_AT', 'SYS_MODEL.DELETED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('148', '1', '9', '删除人', '11111111', '100', 'DELETED_USER', 'SYS_MODEL.DELETED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', null, '0', '80', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('149', '1', '10', '主键', '11111111', '10', 'ID', 'SYS_USER.ID', 'int', 'N', 'N', 'N', 'N', 'Y', '', null, '0', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('150', '1', '10', '所属公司', '11111111', '20', 'C_ID', 'SYS_USER.C_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '@UserCid@', null, '59', '10', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('151', '1', '10', '是否启用 Y:是  N:否', '11111111', '30', 'IS_USED', 'SYS_USER.IS_USED', 'char', 'N', 'N', 'Y', 'N', 'N', 'Y', 'sys_is_used', '0', '3', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('152', '1', '10', '是否删除 N未删 Y已删', '11111111', '40', 'IS_DELETED', 'SYS_USER.IS_DELETED', 'char', 'N', 'N', 'Y', 'N', 'N', 'N', 'sys_is_deleted', '0', '3', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('153', '1', '10', '创建时间', '11111111', '50', 'CREATED_AT', 'SYS_USER.CREATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('154', '1', '10', '创建人', '11111111', '60', 'CREATED_USER', 'SYS_USER.CREATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', null, '0', '60', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('155', '1', '10', '更新时间', '11111111', '70', 'UPDATED_AT', 'SYS_USER.UPDATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('156', '1', '10', '更新人', '11111111', '80', 'UPDATED_USER', 'SYS_USER.UPDATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', null, '0', '60', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('157', '1', '10', '删除时间', '11111111', '90', 'DELETED_AT', 'SYS_USER.DELETED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '0', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('158', '1', '10', '删除人', '11111111', '100', 'DELETED_USER', 'SYS_USER.DELETED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', null, '0', '80', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('159', '1', '10', '用户名', '11111111', '110', 'USER_NAME', 'SYS_USER.USER_NAME', 'varchar', 'Y', 'Y', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('160', '1', '10', '密码', '11111111', '120', 'PASSWORD', 'SYS_USER.PASSWORD', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('161', '1', '10', '手机号', '11111111', '130', 'PHONE', 'SYS_USER.PHONE', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('162', '1', '10', '真实姓名', '11111111', '140', 'NICK_NAME', 'SYS_USER.NICK_NAME', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', null, '0', '255', '0', '0', '0', 'admin', '2022-07-09 16:46:58', 'admin', '2022-07-09 16:46:58', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('163', '1', '11', '主键', '11111111', '10', 'ID', 'SYS_ROLE.ID', 'int', 'N', 'N', 'N', 'N', 'N', '', '', '0', '10', '0', '0', '0', 'admin', '2022-07-12 12:19:13', 'admin', '2022-07-12 12:19:13', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('164', '1', '11', '是否启用 Y:是  N:否', '11111111', '20', 'IS_USED', 'SYS_ROLE.IS_USED', 'char', 'N', 'N', 'Y', 'N', 'N', 'Y', 'sys_is_used', '0', '1', '0', '0', '0', 'admin', '2022-07-12 12:19:13', 'admin', '2022-07-12 12:19:13', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('165', '1', '11', '是否删除 N未删 Y已删', '11111111', '30', 'IS_DELETED', 'SYS_ROLE.IS_DELETED', 'char', 'N', 'N', 'Y', 'N', 'N', 'N', 'sys_is_deleted', '0', '1', '0', '0', '0', 'admin', '2022-07-12 12:19:13', 'admin', '2022-07-12 12:19:13', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('166', '1', '11', '创建时间', '11111111', '40', 'CREATED_AT', 'SYS_ROLE.CREATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', '', '0', '0', '0', '0', '0', 'admin', '2022-07-12 12:19:13', 'admin', '2022-07-12 12:19:13', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('167', '1', '11', '创建人', '11111111', '50', 'CREATED_USER', 'SYS_ROLE.CREATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', '', '0', '80', '0', '0', '0', 'admin', '2022-07-12 12:19:13', 'admin', '2022-07-12 12:19:13', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('168', '1', '11', '更新时间', '11111111', '60', 'UPDATED_AT', 'SYS_ROLE.UPDATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', '', '0', '0', '0', '0', '0', 'admin', '2022-07-12 12:19:13', 'admin', '2022-07-12 12:19:13', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('169', '1', '11', '更新人', '11111111', '70', 'UPDATED_USER', 'SYS_ROLE.UPDATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', '', '0', '80', '0', '0', '0', 'admin', '2022-07-12 12:19:13', 'admin', '2022-07-12 12:19:13', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('170', '1', '11', '删除时间', '11111111', '80', 'DELETED_AT', 'SYS_ROLE.DELETED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', '', '', '0', '0', '0', '0', '0', 'admin', '2022-07-12 12:19:13', 'admin', '2022-07-12 12:19:13', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('171', '1', '11', '删除人', '11111111', '90', 'DELETED_USER', 'SYS_ROLE.DELETED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', '', '0', '80', '0', '0', '0', 'admin', '2022-07-12 12:19:13', 'admin', '2022-07-12 12:19:13', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('172', '1', '11', '所属权限组', '11111111', '100', 'SYS_ROLEGROUP_ID', 'SYS_ROLE.SYS_ROLEGROUP_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '', '', '188', '10', '0', '0', '0', 'admin', '2022-07-12 12:19:13', 'admin', '2022-07-12 12:19:13', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('173', '1', '11', '功能权限', '11111111', '110', 'SYS_MENU_ID', 'SYS_ROLE.SYS_MENU_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '', '', '200', '10', '0', '0', '0', 'admin', '2022-07-12 12:19:13', 'admin', '2022-07-12 12:19:13', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('174', '1', '11', '权限', '11111111', '120', 'PERMISSION', 'SYS_ROLE.PERMISSION', 'int', 'N', 'N', 'Y', 'N', 'N', '', '', '0', '10', '0', '0', '0', 'admin', '2022-07-12 12:19:13', 'admin', '2022-07-12 12:19:13', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('175', '1', '11', '数据权限', '11111111', '130', 'SQLFILTER', 'SYS_ROLE.SQLFILTER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', '', '0', '255', '0', '0', '0', 'admin', '2022-07-12 12:19:13', 'admin', '2022-07-12 12:19:13', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('176', '1', '12', '主键', '11111111', '10', 'ID', 'SYS_ROLEGROUP_USER.ID', 'int', 'N', 'N', 'N', 'N', 'N', '', '', '0', '10', '0', '0', '0', 'admin', '2022-07-12 12:25:47', 'admin', '2022-07-12 12:25:47', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('177', '1', '12', '所属公司', '11111111', '20', 'C_ID', 'SYS_ROLEGROUP_USER.C_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '@UserCid@', '', '59', '10', '0', '0', '0', 'admin', '2022-07-12 12:25:47', 'admin', '2022-07-12 12:25:47', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('178', '1', '12', '是否启用 Y:是  N:否', '11111111', '30', 'IS_USED', 'SYS_ROLEGROUP_USER.IS_USED', 'char', 'N', 'N', 'Y', 'N', 'N', 'Y', 'sys_is_used', '0', '1', '0', '0', '0', 'admin', '2022-07-12 12:25:47', 'admin', '2022-07-12 12:25:47', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('179', '1', '12', '是否删除 N未删 Y已删', '11111111', '40', 'IS_DELETED', 'SYS_ROLEGROUP_USER.IS_DELETED', 'char', 'N', 'N', 'Y', 'N', 'N', 'N', 'sys_is_deleted', '0', '1', '0', '0', '0', 'admin', '2022-07-12 12:25:48', 'admin', '2022-07-12 12:25:48', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('180', '1', '12', '创建时间', '11111111', '50', 'CREATED_AT', 'SYS_ROLEGROUP_USER.CREATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', '', '0', '0', '0', '0', '0', 'admin', '2022-07-12 12:25:48', 'admin', '2022-07-12 12:25:48', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('181', '1', '12', '创建人', '11111111', '60', 'CREATED_USER', 'SYS_ROLEGROUP_USER.CREATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', '', '0', '80', '0', '0', '0', 'admin', '2022-07-12 12:25:48', 'admin', '2022-07-12 12:25:48', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('182', '1', '12', '更新时间', '11111111', '70', 'UPDATED_AT', 'SYS_ROLEGROUP_USER.UPDATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', '', '0', '0', '0', '0', '0', 'admin', '2022-07-12 12:25:48', 'admin', '2022-07-12 12:25:48', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('183', '1', '12', '更新人', '11111111', '80', 'UPDATED_USER', 'SYS_ROLEGROUP_USER.UPDATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', '', '0', '80', '0', '0', '0', 'admin', '2022-07-12 12:25:48', 'admin', '2022-07-12 12:25:48', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('184', '1', '12', '删除时间', '11111111', '90', 'DELETED_AT', 'SYS_ROLEGROUP_USER.DELETED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', '', '', '0', '0', '0', '0', '0', 'admin', '2022-07-12 12:25:48', 'admin', '2022-07-12 12:25:48', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('185', '1', '12', '删除人', '11111111', '100', 'DELETED_USER', 'SYS_ROLEGROUP_USER.DELETED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', '', '0', '80', '0', '0', '0', 'admin', '2022-07-12 12:25:48', 'admin', '2022-07-12 12:25:48', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('186', '1', '12', '权限组id', '11111111', '110', 'SYS_ROLEGROUP_ID', 'SYS_ROLEGROUP_USER.SYS_ROLEGROUP_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '', '', '188', '10', '0', '0', '0', 'admin', '2022-07-12 12:25:48', 'admin', '2022-07-12 12:25:48', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('187', '1', '12', '用户id', '11111111', '120', 'SYS_USER_ID', 'SYS_ROLEGROUP_USER.SYS_USER_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '', '', '149', '10', '0', '0', '0', 'admin', '2022-07-12 12:25:48', 'admin', '2022-07-12 12:25:48', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('188', '1', '13', '主键', '11111111', '10', 'ID', 'SYS_ROLEGROUP.ID', 'int', 'N', 'N', 'N', 'N', 'N', '', '', '0', '10', '0', '0', '0', 'admin', '2022-07-12 12:25:48', 'admin', '2022-07-12 12:25:48', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('189', '1', '13', '所属公司', '11111111', '20', 'C_ID', 'SYS_ROLEGROUP.C_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '@UserCid@', '', '59', '10', '0', '0', '0', 'admin', '2022-07-12 12:25:48', 'admin', '2022-07-12 12:25:48', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('190', '1', '13', '是否启用 Y:是  N:否', '11111111', '30', 'IS_USED', 'SYS_ROLEGROUP.IS_USED', 'char', 'N', 'N', 'Y', 'N', 'N', 'Y', 'sys_is_used', '0', '1', '0', '0', '0', 'admin', '2022-07-12 12:25:48', 'admin', '2022-07-12 12:25:48', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('191', '1', '13', '是否删除 N未删 Y已删', '11111111', '40', 'IS_DELETED', 'SYS_ROLEGROUP.IS_DELETED', 'char', 'N', 'N', 'Y', 'N', 'N', 'N', 'sys_is_deleted', '0', '1', '0', '0', '0', 'admin', '2022-07-12 12:25:48', 'admin', '2022-07-12 12:25:48', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('192', '1', '13', '创建时间', '11111111', '50', 'CREATED_AT', 'SYS_ROLEGROUP.CREATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', '', '0', '0', '0', '0', '0', 'admin', '2022-07-12 12:25:48', 'admin', '2022-07-12 12:25:48', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('193', '1', '13', '创建人', '11111111', '60', 'CREATED_USER', 'SYS_ROLEGROUP.CREATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', '', '0', '80', '0', '0', '0', 'admin', '2022-07-12 12:25:48', 'admin', '2022-07-12 12:25:48', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('194', '1', '13', '更新时间', '11111111', '70', 'UPDATED_AT', 'SYS_ROLEGROUP.UPDATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', '', '0', '0', '0', '0', '0', 'admin', '2022-07-12 12:25:48', 'admin', '2022-07-12 12:25:48', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('195', '1', '13', '更新人', '11111111', '80', 'UPDATED_USER', 'SYS_ROLEGROUP.UPDATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', '', '0', '80', '0', '0', '0', 'admin', '2022-07-12 12:25:48', 'admin', '2022-07-12 12:25:48', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('196', '1', '13', '删除时间', '11111111', '90', 'DELETED_AT', 'SYS_ROLEGROUP.DELETED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', '', '', '0', '0', '0', '0', '0', 'admin', '2022-07-12 12:25:48', 'admin', '2022-07-12 12:25:48', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('197', '1', '13', '删除人', '11111111', '100', 'DELETED_USER', 'SYS_ROLEGROUP.DELETED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', '', '0', '80', '0', '0', '0', 'admin', '2022-07-12 12:25:48', 'admin', '2022-07-12 12:25:48', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('198', '1', '13', '权限组名称', '11111111', '110', 'NAME', 'SYS_ROLEGROUP.NAME', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', '', '0', '255', '0', '0', '0', 'admin', '2022-07-12 12:25:48', 'admin', '2022-07-12 12:25:48', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('199', '1', '13', '权限组备注', '11111111', '120', 'DESCRIPTION', 'SYS_ROLEGROUP.DESCRIPTION', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', '', '0', '255', '0', '0', '0', 'admin', '2022-07-12 12:25:48', 'admin', '2022-07-12 12:25:48', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('200', '1', '14', '主键', '11111111', '10', 'ID', 'SYS_MENU.ID', 'int', 'N', 'N', 'N', 'N', 'N', '', '', '0', '10', '0', '0', '0', 'admin', '2022-07-12 12:25:48', 'admin', '2022-07-12 12:25:48', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('201', '1', '14', '所属公司', '11111111', '20', 'C_ID', 'SYS_MENU.C_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '@UserCid@', '', '59', '10', '0', '0', '0', 'admin', '2022-07-12 12:25:48', 'admin', '2022-07-12 12:25:48', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('202', '1', '14', '是否启用 Y:是  N:否', '11111111', '30', 'IS_USED', 'SYS_MENU.IS_USED', 'char', 'N', 'N', 'Y', 'N', 'N', 'Y', 'sys_is_used', '0', '1', '0', '0', '0', 'admin', '2022-07-12 12:25:49', 'admin', '2022-07-12 12:25:49', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('203', '1', '14', '是否删除 N未删 Y已删', '11111111', '40', 'IS_DELETED', 'SYS_MENU.IS_DELETED', 'char', 'N', 'N', 'Y', 'N', 'N', 'N', 'sys_is_deleted', '0', '1', '0', '0', '0', 'admin', '2022-07-12 12:25:49', 'admin', '2022-07-12 12:25:49', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('204', '1', '14', '创建时间', '11111111', '50', 'CREATED_AT', 'SYS_MENU.CREATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', '', '0', '0', '0', '0', '0', 'admin', '2022-07-12 12:25:49', 'admin', '2022-07-12 12:25:49', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('205', '1', '14', '创建人', '11111111', '60', 'CREATED_USER', 'SYS_MENU.CREATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', '', '0', '80', '0', '0', '0', 'admin', '2022-07-12 12:25:49', 'admin', '2022-07-12 12:25:49', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('206', '1', '14', '更新时间', '11111111', '70', 'UPDATED_AT', 'SYS_MENU.UPDATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', '', '0', '0', '0', '0', '0', 'admin', '2022-07-12 12:25:49', 'admin', '2022-07-12 12:25:49', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('207', '1', '14', '更新人', '11111111', '80', 'UPDATED_USER', 'SYS_MENU.UPDATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', '', '0', '80', '0', '0', '0', 'admin', '2022-07-12 12:25:49', 'admin', '2022-07-12 12:25:49', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('208', '1', '14', '删除时间', '11111111', '90', 'DELETED_AT', 'SYS_MENU.DELETED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', '', '', '0', '0', '0', '0', '0', 'admin', '2022-07-12 12:25:49', 'admin', '2022-07-12 12:25:49', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('209', '1', '14', '删除人', '11111111', '100', 'DELETED_USER', 'SYS_MENU.DELETED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', '', '0', '80', '0', '0', '0', 'admin', '2022-07-12 12:25:49', 'admin', '2022-07-12 12:25:49', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('210', '1', '14', '名称', '11111111', '110', 'NAME', 'SYS_MENU.NAME', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', '', '0', '255', '0', '0', '0', 'admin', '2022-07-12 12:25:49', 'admin', '2022-07-12 12:25:49', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('211', '1', '14', '链接', '11111111', '120', 'URL', 'SYS_MENU.URL', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', '', '0', '255', '0', '0', '0', 'admin', '2022-07-12 12:25:49', 'admin', '2022-07-12 12:25:49', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('212', '1', '14', 'ico', '11111111', '130', 'ICON', 'SYS_MENU.ICON', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', '', '0', '255', '0', '0', '0', 'admin', '2022-07-12 12:25:49', 'admin', '2022-07-12 12:25:49', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('213', '1', '14', '排序', '11111111', '140', 'ORDERNO', 'SYS_MENU.ORDERNO', 'int', 'N', 'N', 'Y', 'N', 'N', '', '', '0', '10', '0', '0', '0', 'admin', '2022-07-12 12:25:49', 'admin', '2022-07-12 12:25:49', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('214', '1', '14', '父路由', '11111111', '150', 'PARENT_ID', 'SYS_MENU.PARENT_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '', '', '200', '10', '0', '0', '0', 'admin', '2022-07-12 12:25:49', 'admin', '2022-07-12 12:25:49', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('215', '1', '14', '所属表单id', '11111111', '160', 'SYS_TABLE_ID', 'SYS_MENU.SYS_TABLE_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '', '', '1', '10', '0', '0', '0', 'admin', '2022-07-12 12:25:49', 'admin', '2022-07-12 12:25:49', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('217', '1', '18', '主键', '11111111', '10', 'ID', 'SYS_TABLEREF.ID', 'int', 'N', 'N', 'N', 'N', 'N', '', '', '0', '10', '0', '0', '0', 'admin', '2022-07-15 14:23:36', 'admin', '2022-07-15 14:23:36', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('218', '1', '18', '所属公司', '11111111', '20', 'C_ID', 'SYS_TABLEREF.C_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '@UserCid@', '', '59', '10', '0', '0', '0', 'admin', '2022-07-15 14:23:36', 'admin', '2022-07-15 14:23:36', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('219', '1', '18', '是否启用 Y:是 N:否', '11111111', '30', 'IS_USED', 'SYS_TABLEREF.IS_USED', 'char', 'N', 'N', 'Y', 'N', 'N', 'Y', '', '0', '1', '0', '0', '0', 'admin', '2022-07-15 14:23:36', 'admin', '2022-07-15 14:23:36', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('220', '1', '18', '是否删除 N未删 Y已删', '11111111', '40', 'IS_DELETED', 'SYS_TABLEREF.IS_DELETED', 'char', 'N', 'N', 'Y', 'N', 'N', 'N', '', '0', '1', '0', '0', '0', 'admin', '2022-07-15 14:23:36', 'admin', '2022-07-15 14:23:36', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('221', '1', '18', '创建时间', '11111111', '50', 'CREATED_AT', 'SYS_TABLEREF.CREATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', '', '0', '0', '0', '0', '0', 'admin', '2022-07-15 14:23:36', 'admin', '2022-07-15 14:23:36', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('222', '1', '18', '创建人', '11111111', '60', 'CREATED_USER', 'SYS_TABLEREF.CREATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', '', '0', '80', '0', '0', '0', 'admin', '2022-07-15 14:23:36', 'admin', '2022-07-15 14:23:36', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('223', '1', '18', '更新时间', '11111111', '70', 'UPDATED_AT', 'SYS_TABLEREF.UPDATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', '', '0', '0', '0', '0', '0', 'admin', '2022-07-15 14:23:36', 'admin', '2022-07-15 14:23:36', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('224', '1', '18', '更新人', '11111111', '80', 'UPDATED_USER', 'SYS_TABLEREF.UPDATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', '', '0', '80', '0', '0', '0', 'admin', '2022-07-15 14:23:36', 'admin', '2022-07-15 14:23:36', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('225', '1', '18', '删除时间', '11111111', '90', 'DELETED_AT', 'SYS_TABLEREF.DELETED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', '', '', '0', '0', '0', '0', '0', 'admin', '2022-07-15 14:23:36', 'admin', '2022-07-15 14:23:36', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('226', '1', '18', '删除人', '11111111', '100', 'DELETED_USER', 'SYS_TABLEREF.DELETED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', '', '0', '80', '0', '0', '0', 'admin', '2022-07-15 14:23:36', 'admin', '2022-07-15 14:23:36', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('227', '1', '18', '所属表单', '11111111', '110', 'SYS_TABLE_ID', 'SYS_TABLEREF.SYS_TABLE_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '', '', '1', '10', '0', '0', '0', 'admin', '2022-07-15 14:23:36', 'admin', '2022-07-15 14:23:36', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('228', '1', '18', '关联子表', '11111111', '120', 'SYS_REFTABLE_ID', 'SYS_TABLEREF.SYS_REFTABLE_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '', '', '1', '10', '0', '0', '0', 'admin', '2022-07-15 14:23:37', 'admin', '2022-07-15 14:23:37', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('229', '1', '18', '关联字段', '11111111', '130', 'SYS_REFCOLUMN_ID', 'SYS_TABLEREF.SYS_REFCOLUMN_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '', '', '30', '10', '0', '0', '0', 'admin', '2022-07-15 14:23:37', 'admin', '2022-07-15 14:23:37', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('230', '1', '18', '过滤', '11111111', '140', 'FILTER', 'SYS_TABLEREF.FILTER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', '', '0', '255', '0', '0', '0', 'admin', '2022-07-15 14:23:37', 'admin', '2022-07-15 14:23:37', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('231', '1', '18', '关联方式', '11111111', '150', 'ASSOC_TYPE', 'SYS_TABLEREF.ASSOC_TYPE', 'char', 'N', 'N', 'Y', 'N', 'N', '', '', '0', '1', '0', '0', '0', 'admin', '2022-07-15 14:23:37', 'admin', '2022-07-15 14:23:37', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('232', '1', '18', '显示名称', '11111111', '160', 'DISNAME', 'SYS_TABLEREF.DISNAME', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', '', '0', '255', '0', '0', '0', 'admin', '2022-07-15 14:23:37', 'admin', '2022-07-15 14:23:37', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('233', '1', '18', '行关联模式', '11111111', '170', 'INLINE_MODE', 'SYS_TABLEREF.INLINE_MODE', 'char', 'N', 'N', 'Y', 'N', 'N', '', '', '0', '2', '0', '0', '0', 'admin', '2022-07-15 14:23:37', 'admin', '2022-07-15 14:23:37', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('234', '1', '21', '主键', '11111111', '10', 'ID', 'SYS_DEPT.ID', 'int', 'N', 'N', 'N', 'N', 'N', '', '', '0', '10', '0', '0', '0', 'admin', '2022-07-21 15:12:05', 'admin', '2022-07-21 15:12:05', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('235', '1', '21', '所属公司', '11111111', '20', 'C_ID', 'SYS_DEPT.C_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '@UserCid@', '', '59', '10', '0', '0', '0', 'admin', '2022-07-21 15:12:05', 'admin', '2022-07-21 15:12:05', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('236', '1', '21', '是否启用 Y:是 N:否', '11111111', '30', 'IS_USED', 'SYS_DEPT.IS_USED', 'char', 'N', 'N', 'Y', 'N', 'N', 'Y', '', '0', '1', '0', '0', '0', 'admin', '2022-07-21 15:12:05', 'admin', '2022-07-21 15:12:05', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('237', '1', '21', '是否删除 N未删 Y已删', '11111111', '40', 'IS_DELETED', 'SYS_DEPT.IS_DELETED', 'char', 'N', 'N', 'Y', 'N', 'N', 'N', '', '0', '1', '0', '0', '0', 'admin', '2022-07-21 15:12:05', 'admin', '2022-07-21 15:12:05', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('238', '1', '21', '创建时间', '11111111', '50', 'CREATED_AT', 'SYS_DEPT.CREATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', '', '0', '0', '0', '0', '0', 'admin', '2022-07-21 15:12:05', 'admin', '2022-07-21 15:12:05', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('239', '1', '21', '创建人', '11111111', '60', 'CREATED_USER', 'SYS_DEPT.CREATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', '', '0', '80', '0', '0', '0', 'admin', '2022-07-21 15:12:05', 'admin', '2022-07-21 15:12:05', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('240', '1', '21', '更新时间', '11111111', '70', 'UPDATED_AT', 'SYS_DEPT.UPDATED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', 'CURRENT_TIMESTAMP', '', '0', '0', '0', '0', '0', 'admin', '2022-07-21 15:12:05', 'admin', '2022-07-21 15:12:05', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('241', '1', '21', '更新人', '11111111', '80', 'UPDATED_USER', 'SYS_DEPT.UPDATED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', '', '0', '80', '0', '0', '0', 'admin', '2022-07-21 15:12:05', 'admin', '2022-07-21 15:12:05', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('242', '1', '21', '删除时间', '11111111', '90', 'DELETED_AT', 'SYS_DEPT.DELETED_AT', 'timestamp', 'N', 'N', 'Y', 'N', 'N', '', '', '0', '0', '0', '0', '0', 'admin', '2022-07-21 15:12:05', 'admin', '2022-07-21 15:12:05', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('243', '1', '21', '删除人', '11111111', '100', 'DELETED_USER', 'SYS_DEPT.DELETED_USER', 'varchar', 'N', 'N', 'Y', 'N', 'N', '@UserName@', '', '0', '80', '0', '0', '0', 'admin', '2022-07-21 15:12:05', 'admin', '2022-07-21 15:12:05', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('244', '1', '21', '部门编号', '11111111', '110', 'CODE', 'SYS_DEPT.CODE', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', '', '0', '255', '0', '0', '0', 'admin', '2022-07-21 15:12:05', 'admin', '2022-07-21 15:12:05', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('245', '1', '21', '名称', '11111111', '120', 'NAME', 'SYS_DEPT.NAME', 'varchar', 'N', 'N', 'Y', 'N', 'N', '', '', '0', '255', '0', '0', '0', 'admin', '2022-07-21 15:12:06', 'admin', '2022-07-21 15:12:06', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('246', '1', '21', '上级', '11111111', '130', 'SYS_DEPT_ID', 'SYS_DEPT.SYS_DEPT_ID', 'int', 'N', 'N', 'Y', 'N', 'N', '', '', '234', '10', '0', '0', '0', 'admin', '2022-07-21 15:12:06', 'admin', '2022-07-21 15:12:06', 'Y', 'N', null, null);
INSERT INTO `sys_column` VALUES ('247', '1', '10', '用户所属部门', '11111111', '15', 'SYS_DEPT_ID', 'SYS_USER.SYS_DEPT_ID', 'int', 'N', 'N', 'Y', 'N', 'N', null, null, '21', '10', '0', '0', '0', 'admin', '2022-07-21 07:28:12', 'admin', '2022-07-21 07:28:12', 'Y', 'N', null, null);

-- ----------------------------
-- Table structure for sys_company
-- ----------------------------
DROP TABLE IF EXISTS `sys_company`;
CREATE TABLE `sys_company` (
                               `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                               `is_used` char(1) NOT NULL DEFAULT 'Y' COMMENT '是否启用 Y:是  N:否',
                               `is_deleted` char(1) NOT NULL DEFAULT 'N' COMMENT '是否删除 N未 Y已删',
                               `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                               `created_user` varchar(80) CHARACTER SET utf8 DEFAULT NULL COMMENT '创建人',
                               `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                               `updated_user` varchar(80) CHARACTER SET utf8 DEFAULT NULL COMMENT '更新人',
                               `deleted_at` timestamp NULL DEFAULT NULL COMMENT '删除时间',
                               `deleted_user` varchar(80) DEFAULT NULL,
                               `code` varchar(80) DEFAULT NULL,
                               `name` varchar(255) DEFAULT NULL COMMENT '公司名称',
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='公司信息';

-- ----------------------------
-- Records of sys_company
-- ----------------------------
INSERT INTO `sys_company` VALUES ('1', 'Y', 'N', '2022-07-06 03:34:38', 'admin', '2022-07-18 02:23:15', 'admin', null, null, '00001', 'XhsoftWare');

-- ----------------------------
-- Table structure for sys_cron_task
-- ----------------------------
DROP TABLE IF EXISTS `sys_cron_task`;
CREATE TABLE `sys_cron_task` (
                                 `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                                 `c_id` int(11) DEFAULT NULL COMMENT '所属机构',
                                 `name` varchar(80) NOT NULL DEFAULT '' COMMENT '任务名称',
                                 `spec` varchar(80) NOT NULL DEFAULT '' COMMENT 'crontab 表达式',
                                 `command` varchar(255) NOT NULL DEFAULT '' COMMENT '执行命令',
                                 `protocol` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '执行方式 1:shell 2:http',
                                 `http_method` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT 'http 请求方式 1:get 2:post',
                                 `timeout` int(11) unsigned NOT NULL DEFAULT '60' COMMENT '超时时间(单位:秒)',
                                 `retry_times` tinyint(1) NOT NULL DEFAULT '3' COMMENT '重试次数',
                                 `retry_interval` int(11) NOT NULL DEFAULT '60' COMMENT '重试间隔(单位:秒)',
                                 `notify_status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '执行结束是否通知 1:不通知 2:失败通知 3:结束通知 4:结果关键字匹配通知',
                                 `notify_type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '通知类型 1:邮件 2:webhook',
                                 `notify_receiver_email` varchar(255) NOT NULL DEFAULT '' COMMENT '通知者邮箱地址(多个用,分割)',
                                 `notify_keyword` varchar(255) NOT NULL DEFAULT '' COMMENT '通知匹配关键字(多个用,分割)',
                                 `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
                                 `is_used` char(1) NOT NULL DEFAULT 'Y' COMMENT '是否启用 Y:是  N:否',
                                 `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                 `created_user` varchar(60) NOT NULL DEFAULT '' COMMENT '创建人',
                                 `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                 `updated_user` varchar(60) NOT NULL DEFAULT '' COMMENT '更新人',
                                 PRIMARY KEY (`id`),
                                 KEY `idx_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台任务表';

-- ----------------------------
-- Records of sys_cron_task
-- ----------------------------

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
                            `id` int(12) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                            `c_id` int(12) DEFAULT NULL COMMENT '所属公司',
                            `is_used` char(1) DEFAULT 'Y' COMMENT '是否启用 Y:是 N:否',
                            `is_deleted` char(1) DEFAULT 'N' COMMENT '是否删除 N未删 Y已删',
                            `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                            `created_user` varchar(80) CHARACTER SET utf8 DEFAULT '' COMMENT '创建人',
                            `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                            `updated_user` varchar(80) CHARACTER SET utf8 DEFAULT '' COMMENT '更新人',
                            `deleted_at` timestamp NULL DEFAULT NULL COMMENT '删除时间',
                            `deleted_user` varchar(80) DEFAULT NULL COMMENT '删除人',
                            `code` varchar(255) DEFAULT NULL COMMENT '部门编号',
                            `name` varchar(255) DEFAULT NULL COMMENT '名称',
                            `sys_dept_id` int(11) DEFAULT NULL COMMENT '上级',
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='模板表单';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('1', '1', 'Y', 'N', '2022-07-21 07:33:34', 'admin', '2022-07-21 07:33:34', 'admin', null, null, '00001', '总部', null);

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
                            `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                            `c_id` int(11) DEFAULT NULL,
                            `is_used` char(1) DEFAULT 'Y' COMMENT '是否启用 Y:是  N:否',
                            `is_deleted` char(1) DEFAULT 'N' COMMENT '是否删除 N未删 Y已删',
                            `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                            `created_user` varchar(80) CHARACTER SET utf8 DEFAULT '' COMMENT '创建人',
                            `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                            `updated_user` varchar(80) CHARACTER SET utf8 DEFAULT '' COMMENT '更新人',
                            `deleted_at` timestamp NULL DEFAULT NULL COMMENT '删除时间',
                            `deleted_user` varchar(80) DEFAULT NULL COMMENT '删除者',
                            `dict_name` varchar(255) DEFAULT NULL COMMENT '字典名称',
                            `dict_type` varchar(255) DEFAULT NULL COMMENT '字段类型',
                            `description` varchar(255) DEFAULT NULL COMMENT '备注',
                            PRIMARY KEY (`id`),
                            UNIQUE KEY `UK_cid_dict_name` (`c_id`,`dict_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='数据字典';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('1', '1', 'Y', 'N', '2022-07-06 07:58:43', 'admin', '2022-07-18 01:42:00', 'admin', null, null, 'sys_is_used', null, '是否可用');
INSERT INTO `sys_dict` VALUES ('2', '1', 'Y', 'N', '2022-07-06 08:00:55', 'admin', '2022-07-18 01:42:00', 'admin', null, null, 'sys_is_deleted', null, '是否删除');

-- ----------------------------
-- Table structure for sys_dictitem
-- ----------------------------
DROP TABLE IF EXISTS `sys_dictitem`;
CREATE TABLE `sys_dictitem` (
                                `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                                `is_used` char(1) DEFAULT 'Y' COMMENT '是否启用 Y:是  N:否',
                                `is_deleted` char(1) DEFAULT 'N' COMMENT '是否删除 N未删 Y已删',
                                `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                `created_user` varchar(80) CHARACTER SET utf8 DEFAULT '' COMMENT '创建人',
                                `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                `updated_user` varchar(80) CHARACTER SET utf8 DEFAULT '' COMMENT '更新人',
                                `deleted_at` timestamp NULL DEFAULT NULL COMMENT '删除时间',
                                `deleted_user` varchar(80) DEFAULT NULL,
                                `sys_dict_id` int(11) DEFAULT NULL COMMENT '所属数据字典',
                                `orderno` int(10) DEFAULT NULL COMMENT '序号',
                                `label` varchar(255) DEFAULT NULL COMMENT '字典标签',
                                `value` varchar(255) DEFAULT NULL COMMENT '字典值',
                                `css_class` varchar(255) DEFAULT NULL COMMENT '单对象样式',
                                `list_class` varchar(255) DEFAULT NULL COMMENT '列表样式',
                                `is_default` char(1) DEFAULT NULL COMMENT '是否默认(0:非默认 1:默认)',
                                `description` varchar(255) DEFAULT NULL COMMENT '备注',
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='数据字典明细';

-- ----------------------------
-- Records of sys_dictitem
-- ----------------------------
INSERT INTO `sys_dictitem` VALUES ('1', 'Y', 'N', '2022-07-06 07:59:12', 'admin', '2022-07-14 03:06:18', 'admin', null, null, '1', '1', '可用', 'Y', null, null, 'Y', null);
INSERT INTO `sys_dictitem` VALUES ('2', 'Y', 'N', '2022-07-06 07:59:19', 'admin', '2022-07-14 03:06:11', 'admin', null, null, '1', '2', '不可用', 'N', null, null, null, null);
INSERT INTO `sys_dictitem` VALUES ('3', 'Y', 'N', '2022-07-06 08:01:31', 'admin', '2022-07-14 03:06:21', 'admin', null, null, '2', '1', '未删除', 'N', null, null, 'Y', null);
INSERT INTO `sys_dictitem` VALUES ('4', 'Y', 'N', '2022-07-06 08:01:51', 'admin', '2022-07-14 03:06:16', 'admin', null, null, '2', '2', '已删除', 'Y', null, null, null, null);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
                            `id` int(12) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                            `c_id` int(12) DEFAULT NULL COMMENT '所属公司',
                            `is_used` char(1) DEFAULT 'Y' COMMENT '是否启用 Y:是  N:否',
                            `is_deleted` char(1) DEFAULT 'N' COMMENT '是否删除 N未删 Y已删',
                            `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                            `created_user` varchar(80) CHARACTER SET utf8 DEFAULT '' COMMENT '创建人',
                            `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                            `updated_user` varchar(80) CHARACTER SET utf8 DEFAULT '' COMMENT '更新人',
                            `deleted_at` timestamp NULL DEFAULT NULL COMMENT '删除时间',
                            `deleted_user` varchar(80) DEFAULT NULL COMMENT '删除人',
                            `name` varchar(255) DEFAULT NULL COMMENT '名称',
                            `url` varchar(255) DEFAULT NULL COMMENT '链接',
                            `icon` varchar(255) DEFAULT NULL COMMENT 'ico',
                            `orderno` int(11) DEFAULT NULL COMMENT '排序',
                            `parent_id` int(11) DEFAULT NULL COMMENT '父路由',
                            `sys_table_id` int(11) DEFAULT NULL COMMENT '所属表单id',
                            `type` char(1) DEFAULT 'M' COMMENT 'M:菜单 A:按钮',
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='菜单表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '1', 'Y', 'N', '2022-07-12 02:42:00', 'admin', '2022-07-18 02:19:08', 'admin', null, null, '系统管理', null, null, '10', null, null, 'M');
INSERT INTO `sys_menu` VALUES ('2', '1', 'Y', 'N', '2022-07-15 10:02:26', 'admin', '2022-07-18 02:19:11', 'admin', null, null, '公司信息', null, null, '10', '1', '3', 'M');
INSERT INTO `sys_menu` VALUES ('3', '1', 'Y', 'N', '2022-07-15 10:03:16', 'admin', '2022-07-18 02:19:14', 'admin', null, null, '用户信息', null, null, '20', '1', '10', 'M');
INSERT INTO `sys_menu` VALUES ('4', '1', 'Y', 'N', '2022-07-15 10:03:50', 'admin', '2022-07-18 02:19:16', 'admin', null, null, '菜单管理', null, null, '30', '1', '14', 'M');
INSERT INTO `sys_menu` VALUES ('5', '1', 'Y', 'N', '2022-07-15 10:04:24', 'admin', '2022-07-18 02:19:17', 'admin', null, null, '配置表单', null, null, '40', '1', '1', 'M');
INSERT INTO `sys_menu` VALUES ('6', '1', 'Y', 'N', '2022-07-15 10:04:41', 'admin', '2022-07-18 02:19:19', 'admin', null, null, '配置字段', null, null, '50', '1', '2', 'M');
INSERT INTO `sys_menu` VALUES ('7', '1', 'Y', 'N', '2022-07-15 10:05:32', 'admin', '2022-07-18 02:19:21', 'admin', null, null, '数据字典', null, null, '60', '1', '7', 'M');
INSERT INTO `sys_menu` VALUES ('8', '1', 'Y', 'N', '2022-07-15 10:06:08', 'admin', '2022-07-18 02:19:23', 'admin', null, null, '权限组管理', null, null, '70', '1', '13', 'M');
INSERT INTO `sys_menu` VALUES ('9', '1', 'Y', 'N', '2022-07-15 10:07:15', 'admin', '2022-07-18 02:19:25', 'admin', null, null, '后台任务管理', null, null, '80', '1', '4', 'M');
INSERT INTO `sys_menu` VALUES ('10', '1', 'Y', 'N', '2022-07-15 10:09:07', 'admin', '2022-07-18 02:19:27', 'admin', null, null, '接口管理', null, null, '20', null, null, 'M');
INSERT INTO `sys_menu` VALUES ('11', '1', 'Y', 'N', '2022-07-15 10:09:46', 'admin', '2022-07-18 02:19:29', 'admin', null, null, '授权接口调用方', null, null, '10', '10', '6', 'M');
INSERT INTO `sys_menu` VALUES ('12', '1', 'Y', 'N', '2022-07-15 10:09:53', 'admin', '2022-07-18 02:36:49', 'admin', null, null, '授权调用接口', null, null, '20', '10', '5', 'M');
INSERT INTO `sys_menu` VALUES ('13', '1', 'Y', 'N', '2022-07-21 07:17:08', 'admin', '2022-07-21 07:18:08', 'admin', null, null, '部门管理', null, null, '15', '1', '21', 'M');

-- ----------------------------
-- Table structure for sys_model
-- ----------------------------
DROP TABLE IF EXISTS `sys_model`;
CREATE TABLE `sys_model` (
                             `id` int(12) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                             `c_id` int(12) DEFAULT NULL COMMENT '所属公司',
                             `is_used` char(1) DEFAULT 'Y' COMMENT '是否启用 Y:是 N:否',
                             `is_deleted` char(1) DEFAULT 'N' COMMENT '是否删除 N未删 Y已删',
                             `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                             `created_user` varchar(80) CHARACTER SET utf8 DEFAULT '' COMMENT '创建人',
                             `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                             `updated_user` varchar(80) CHARACTER SET utf8 DEFAULT '' COMMENT '更新人',
                             `deleted_at` timestamp NULL DEFAULT NULL COMMENT '删除时间',
                             `deleted_user` varchar(80) DEFAULT NULL COMMENT '删除人',
                             PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='模板表单';

-- ----------------------------
-- Records of sys_model
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
                            `id` int(12) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                            `is_used` char(1) DEFAULT 'Y' COMMENT '是否启用 Y:是  N:否',
                            `is_deleted` char(1) DEFAULT 'N' COMMENT '是否删除 N未删 Y已删',
                            `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                            `created_user` varchar(80) CHARACTER SET utf8 DEFAULT '' COMMENT '创建人',
                            `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                            `updated_user` varchar(80) CHARACTER SET utf8 DEFAULT '' COMMENT '更新人',
                            `deleted_at` timestamp NULL DEFAULT NULL COMMENT '删除时间',
                            `deleted_user` varchar(80) DEFAULT NULL COMMENT '删除人',
                            `sys_rolegroup_id` int(12) DEFAULT NULL COMMENT '所属权限组',
                            `sys_menu_id` int(12) DEFAULT NULL COMMENT '功能权限',
                            `permission` int(10) DEFAULT NULL COMMENT '权限',
                            `sqlfilter` varchar(255) DEFAULT NULL COMMENT '数据权限',
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='权限组权限';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', 'Y', 'N', '2022-07-18 01:22:46', 'admin', '2022-07-18 02:00:04', 'admin', null, null, '1', '2', '31', '');
INSERT INTO `sys_role` VALUES ('2', 'Y', 'N', '2022-07-18 01:22:50', 'admin', '2022-07-18 02:00:04', 'admin', null, null, '1', '3', '31', null);
INSERT INTO `sys_role` VALUES ('3', 'Y', 'N', '2022-07-18 01:22:50', 'admin', '2022-07-18 02:00:04', 'admin', null, null, '1', '4', '31', null);
INSERT INTO `sys_role` VALUES ('4', 'Y', 'N', '2022-07-18 01:22:51', 'admin', '2022-07-18 02:00:04', 'admin', null, null, '1', '5', '31', null);
INSERT INTO `sys_role` VALUES ('5', 'Y', 'N', '2022-07-18 01:22:51', 'admin', '2022-07-18 02:00:04', 'admin', null, null, '1', '6', '31', null);
INSERT INTO `sys_role` VALUES ('6', 'Y', 'N', '2022-07-18 01:22:51', 'admin', '2022-07-18 02:00:04', 'admin', null, null, '1', '7', '31', null);
INSERT INTO `sys_role` VALUES ('7', 'Y', 'N', '2022-07-18 01:22:51', 'admin', '2022-07-18 02:00:04', 'admin', null, null, '1', '8', '31', null);
INSERT INTO `sys_role` VALUES ('8', 'Y', 'N', '2022-07-18 01:22:52', 'admin', '2022-07-18 02:00:04', 'admin', null, null, '1', '9', '31', null);
INSERT INTO `sys_role` VALUES ('9', 'Y', 'N', '2022-07-18 01:22:52', 'admin', '2022-07-18 02:00:04', 'admin', null, null, '1', '11', '31', null);
INSERT INTO `sys_role` VALUES ('10', 'Y', 'N', '2022-07-18 01:23:16', 'admin', '2022-07-18 02:00:04', 'admin', null, null, '1', '12', '31', null);

-- ----------------------------
-- Table structure for sys_rolegroup
-- ----------------------------
DROP TABLE IF EXISTS `sys_rolegroup`;
CREATE TABLE `sys_rolegroup` (
                                 `id` int(12) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                                 `c_id` int(12) DEFAULT NULL COMMENT '所属公司',
                                 `is_used` char(1) DEFAULT 'Y' COMMENT '是否启用 Y:是  N:否',
                                 `is_deleted` char(1) DEFAULT 'N' COMMENT '是否删除 N未删 Y已删',
                                 `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                 `created_user` varchar(80) CHARACTER SET utf8 DEFAULT '' COMMENT '创建人',
                                 `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                 `updated_user` varchar(80) CHARACTER SET utf8 DEFAULT '' COMMENT '更新人',
                                 `deleted_at` timestamp NULL DEFAULT NULL COMMENT '删除时间',
                                 `deleted_user` varchar(80) DEFAULT NULL COMMENT '删除人',
                                 `name` varchar(255) DEFAULT NULL COMMENT '权限组名称',
                                 `description` varchar(255) DEFAULT NULL COMMENT '权限组备注',
                                 PRIMARY KEY (`id`),
                                 KEY `N_CID_Name` (`id`,`name`) USING HASH
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户权限组';

-- ----------------------------
-- Records of sys_rolegroup
-- ----------------------------
INSERT INTO `sys_rolegroup` VALUES ('1', '1', 'Y', 'N', '2022-07-12 02:45:39', 'admin', '2022-07-12 02:45:39', 'admin', null, null, '管理员', null);

-- ----------------------------
-- Table structure for sys_rolegroup_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_rolegroup_user`;
CREATE TABLE `sys_rolegroup_user` (
                                      `id` int(12) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                                      `c_id` int(12) DEFAULT NULL COMMENT '所属公司',
                                      `is_used` char(1) DEFAULT 'Y' COMMENT '是否启用 Y:是 N:否',
                                      `is_deleted` char(1) DEFAULT 'N' COMMENT '是否删除 N未删 Y已删',
                                      `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                      `created_user` varchar(80) CHARACTER SET utf8 DEFAULT '' COMMENT '创建人',
                                      `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                      `updated_user` varchar(80) CHARACTER SET utf8 DEFAULT '' COMMENT '更新人',
                                      `deleted_at` timestamp NULL DEFAULT NULL COMMENT '删除时间',
                                      `deleted_user` varchar(80) DEFAULT NULL COMMENT '删除人',
                                      `sys_rolegroup_id` int(11) DEFAULT NULL COMMENT '权限组id',
                                      `sys_user_id` int(11) DEFAULT NULL COMMENT '用户id',
                                      PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户所属权限组';

-- ----------------------------
-- Records of sys_rolegroup_user
-- ----------------------------
INSERT INTO `sys_rolegroup_user` VALUES ('1', '1', 'Y', 'N', '2022-07-12 03:35:01', 'admin', '2022-07-12 03:35:01', 'admin', null, null, '1', '1');

-- ----------------------------
-- Table structure for sys_table
-- ----------------------------
DROP TABLE IF EXISTS `sys_table`;
CREATE TABLE `sys_table` (
                             `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                             `c_id` int(11) DEFAULT NULL COMMENT '所属公司',
                             `is_used` char(1) COLLATE utf8mb4_bin DEFAULT 'Y' COMMENT '是否启用 Y:是  N:否',
                             `is_deleted` char(1) COLLATE utf8mb4_bin DEFAULT 'N' COMMENT '是否删除 Y是 N否',
                             `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                             `created_user` varchar(60) CHARACTER SET utf8 DEFAULT '' COMMENT '创建人',
                             `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                             `updated_user` varchar(60) CHARACTER SET utf8 DEFAULT '' COMMENT '更新人',
                             `disname` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '显示名称',
                             `orderno` int(255) DEFAULT NULL COMMENT '序号',
                             `name` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '数据库表名称',
                             `pk_column_id` int(11) DEFAULT NULL COMMENT '主键',
                             `rel_table_id` int(11) DEFAULT NULL COMMENT '实际数据库表',
                             `filter` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '过滤条件',
                             `table_mask` char(10) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '表单功能',
                             `ad_column_id` int(11) DEFAULT NULL COMMENT '显示健',
                             `dk_column_id` int(11) DEFAULT NULL COMMENT '输入健',
                             `url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '网页链接',
                             `is_menu` char(1) COLLATE utf8mb4_bin DEFAULT 'N' COMMENT '是否菜单 Y是N否',
                             `parent_table_id` int(11) DEFAULT NULL COMMENT '父表',
                             `trig_ac` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '新增后触发',
                             `trig_am` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '修改后触发',
                             `trig_bm` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '删除前触发',
                             `proc_submit` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '提交程序',
                             `proc_unsubmit` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '反提交程序',
                             `is_big` char(1) COLLATE utf8mb4_bin DEFAULT 'N' COMMENT '是否海量 N否Y海量',
                             `props` varchar(2550) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '扩展属性',
                             `comments` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
                             PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='1配置表单';

-- ----------------------------
-- Records of sys_table
-- ----------------------------
INSERT INTO `sys_table` VALUES ('1', '1', 'Y', 'N', '2022-07-09 16:46:53', 'admin', '2022-07-12 06:17:00', 'admin', '配置表单', '10', 'SYS_TABLE', '1', '0', '', 'CRUD', '11', '11', '', 'Y', '0', '', '', '', '', '', 'N', '', '');
INSERT INTO `sys_table` VALUES ('2', '1', 'Y', 'N', '2022-07-09 16:46:54', 'admin', '2022-07-12 06:17:03', 'admin', '字段配置表单', '20', 'SYS_COLUMN', '30', '0', '', 'CRUD', '37', '37', '', 'Y', '0', '', '', '', '', '', 'N', '', '');
INSERT INTO `sys_table` VALUES ('3', '1', 'Y', 'N', '2022-07-09 16:46:55', 'admin', '2022-07-12 03:07:02', 'admin', '公司信息', '30', 'SYS_COMPANY', '59', '0', '', 'CRUD', '69', '69', '', 'Y', '0', '', '', '', '', '', 'N', '', '');
INSERT INTO `sys_table` VALUES ('4', '1', 'Y', 'N', '2022-07-09 16:46:55', 'admin', '2022-07-09 09:01:09', 'admin', '后台任务表', '40', 'SYS_CRON_TASK', '70', '0', '', 'CRUD', '71', '71', '', 'N', '0', '', '', '', '', '', 'N', '', '');
INSERT INTO `sys_table` VALUES ('5', '1', 'Y', 'N', '2022-07-09 16:46:56', 'admin', '2022-07-12 03:10:02', 'admin', '已授权接口地址表', '50', 'SYS_AUTHORIZED_API', '89', '0', '', 'CRUD', '90', '90', '', 'Y', '0', '', '', '', '', '', 'N', '', '');
INSERT INTO `sys_table` VALUES ('6', '1', 'Y', 'N', '2022-07-09 16:46:56', 'admin', '2022-07-12 03:10:06', 'admin', '已授权的调用方表', '60', 'SYS_AUTHORIZED', '98', '0', '', 'CRUD', '99', '99', '', 'Y', '0', '', '', '', '', '', 'N', '', '');
INSERT INTO `sys_table` VALUES ('7', '1', 'Y', 'N', '2022-07-09 16:46:57', 'admin', '2022-07-12 03:07:10', 'admin', '数据字典', '70', 'SYS_DICT', '110', '0', '', 'CRUD', '119', '119', '', 'Y', '7', '', '', '', '', '', 'N', '', '');
INSERT INTO `sys_table` VALUES ('8', '1', 'Y', 'N', '2022-07-09 16:46:57', 'admin', '2022-07-09 09:02:55', 'admin', '数据字典明细', '80', 'SYS_DICTITEM', '122', '0', '', 'CRUD', '133', '133', '', 'N', '0', '', '', '', '', '', 'N', '', '');
INSERT INTO `sys_table` VALUES ('9', '1', 'Y', 'N', '2022-07-09 16:46:58', 'admin', '2022-07-09 09:03:12', 'admin', '模板表单', '90', 'SYS_MODEL', '139', '0', '', 'CRUD', '139', '139', '', 'N', '0', '', '', '', '', '', 'N', '', '');
INSERT INTO `sys_table` VALUES ('10', '1', 'Y', 'N', '2022-07-09 16:46:58', 'admin', '2022-07-12 03:07:14', 'admin', '用户表', '100', 'SYS_USER', '149', '0', '', 'CRUD', '159', '159', '', 'Y', '0', '', '', '', '', '', 'N', '', '');
INSERT INTO `sys_table` VALUES ('11', '1', 'Y', 'N', '2022-07-12 12:19:13', 'admin', '2022-07-12 06:21:51', 'admin', '权限组权限', '110', 'SYS_ROLE', '163', '0', '', 'CRUD', '163', '163', '', 'N', '13', '', '', '', '', '', 'N', '', '');
INSERT INTO `sys_table` VALUES ('12', '1', 'Y', 'N', '2022-07-12 12:25:47', 'admin', '2022-07-12 06:21:57', 'admin', '用户所属权限组', '120', 'SYS_ROLEGROUP_USER', '176', '0', '', 'CRUD', '176', '176', '', 'N', '13', '', '', '', '', '', 'N', '', '');
INSERT INTO `sys_table` VALUES ('13', '1', 'Y', 'N', '2022-07-12 12:25:48', 'admin', '2022-07-12 06:21:15', 'admin', '用户权限组', '130', 'SYS_ROLEGROUP', '188', '0', '', 'CRUD', '198', '198', '', 'Y', '0', '', '', '', '', '', 'N', '', '');
INSERT INTO `sys_table` VALUES ('14', '1', 'Y', 'N', '2022-07-12 12:25:48', 'admin', '2022-07-12 06:21:18', 'admin', '菜单表', '140', 'SYS_MENU', '200', '0', '', 'CRUD', '210', '210', '', 'Y', '0', '', '', '', '', '', 'N', '', '');
INSERT INTO `sys_table` VALUES ('18', '1', 'Y', 'N', '2022-07-15 14:23:36', 'admin', '2022-07-15 07:12:32', 'admin', '关联表', '180', 'SYS_TABLEREF', '217', '0', '', 'CRUD', '217', '217', '', 'N', '1', '', '', '', '', '', 'N', '', '');
INSERT INTO `sys_table` VALUES ('21', '1', 'Y', 'N', '2022-07-21 15:12:05', 'admin', '2022-07-21 07:19:09', 'admin', '部门信息', '190', 'SYS_DEPT', '234', '0', '', 'CRUD', '244', '244', '', 'Y', '0', '', '', '', '', '', 'N', '', '');

-- ----------------------------
-- Table structure for sys_tableref
-- ----------------------------
DROP TABLE IF EXISTS `sys_tableref`;
CREATE TABLE `sys_tableref` (
                                `id` int(12) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
                                `c_id` int(12) DEFAULT NULL COMMENT '所属公司',
                                `is_used` char(1) DEFAULT 'Y' COMMENT '是否启用 Y:是 N:否',
                                `is_deleted` char(1) DEFAULT 'N' COMMENT '是否删除 N未删 Y已删',
                                `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                `created_user` varchar(80) CHARACTER SET utf8 DEFAULT '' COMMENT '创建人',
                                `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                `updated_user` varchar(80) CHARACTER SET utf8 DEFAULT '' COMMENT '更新人',
                                `deleted_at` timestamp NULL DEFAULT NULL COMMENT '删除时间',
                                `deleted_user` varchar(80) DEFAULT NULL COMMENT '删除人',
                                `sys_table_id` int(11) DEFAULT NULL COMMENT '所属表单',
                                `sys_reftable_id` int(11) DEFAULT NULL COMMENT '关联子表',
                                `sys_refcolumn_id` int(11) DEFAULT NULL COMMENT '关联字段',
                                `filter` varchar(255) DEFAULT NULL COMMENT '过滤',
                                `assoc_type` char(1) DEFAULT NULL COMMENT '关联方式',
                                `disname` varchar(255) DEFAULT NULL COMMENT '显示名称',
                                `inline_mode` char(2) DEFAULT NULL COMMENT '行关联模式',
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='关联表';

-- ----------------------------
-- Records of sys_tableref
-- ----------------------------
INSERT INTO `sys_tableref` VALUES ('1', '1', 'Y', 'N', '2022-07-15 06:33:21', 'admin', '2022-07-18 01:32:16', 'admin', null, null, '1', '18', '227', null, 'n', '关联表', 'Y');
INSERT INTO `sys_tableref` VALUES ('2', '1', 'Y', 'N', '2022-07-15 06:34:24', 'admin', '2022-07-18 01:32:16', 'admin', null, null, '7', '8', '131', null, 'n', '字典明细', 'Y');
INSERT INTO `sys_tableref` VALUES ('3', '1', 'Y', 'N', '2022-07-15 06:50:52', 'admin', '2022-07-18 01:32:16', 'admin', null, null, '13', '12', '186', null, 'n', '权限组用户明细', 'Y');
INSERT INTO `sys_tableref` VALUES ('4', '1', 'Y', 'N', '2022-07-15 06:52:11', 'admin', '2022-07-18 01:32:16', 'admin', null, null, '13', '11', '172', null, 'n', '权限组权限明细', 'Y');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
                            `id` bigint(12) NOT NULL AUTO_INCREMENT COMMENT '主键',
                            `c_id` bigint(11) DEFAULT NULL COMMENT '所属公司',
                            `is_used` char(1) DEFAULT 'Y' COMMENT '是否启用 Y:是  N:否',
                            `is_deleted` char(1) DEFAULT 'N' COMMENT '是否删除 N未 Y删除',
                            `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                            `created_user` varchar(60) CHARACTER SET utf8 DEFAULT '' COMMENT '创建人',
                            `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                            `updated_user` varchar(60) CHARACTER SET utf8 DEFAULT '' COMMENT '更新人',
                            `deleted_at` timestamp NULL DEFAULT NULL COMMENT '删除时间',
                            `deleted_user` varchar(80) DEFAULT NULL,
                            `user_name` varchar(255) DEFAULT NULL COMMENT '用户名',
                            `password` varchar(255) DEFAULT NULL COMMENT '密码',
                            `phone` varchar(255) DEFAULT NULL COMMENT '手机号',
                            `nick_name` varchar(255) DEFAULT NULL COMMENT '真实姓名',
                            `sys_dept_id` int(11) DEFAULT NULL COMMENT '所属部门',
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', '1', 'Y', 'N', '2022-07-06 08:05:41', 'admin', '2022-07-21 07:33:41', 'admin', null, null, 'admin', '52326758754fb5176cf81ae86b3841cd1039a2abdb03240e61f6fb36359bdf0e', '18705699753', 'skyzhou', '1');
