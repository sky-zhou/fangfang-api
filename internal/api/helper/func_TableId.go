package helper

import (
	"net/http"
	"strconv"

	"gitee.com/sky-zhou/fangfang-api/internal/code"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
)

type TableNameRequest struct {
	TableName string `uri:"str" binding:"required"` // 需要获取的表名
}

type TableId struct {
	TableId string `json:"TableId"` // 返回表单id
}

// GetTableId 获取表单id
// @Summary 获取表单id
// @Description 获取表单id
// @Tags Helper
// @Accept application/x-www-form-urlencoded
// @Produce json
// @Param str path string true "需要获取的表单Id"
// @Success 200 {object} TableId
// @Failure 400 {object} code.Failure
// @Router /helper/GetTableId/{str} [get]
func (h *handler) GetTableId() core.HandlerFunc {
	return func(ctx core.Context) {
		req := new(TableNameRequest)
		res := new(TableId)
		if err := ctx.ShouldBindURI(req); err != nil {
			ctx.AbortWithError(core.Error(
				http.StatusBadRequest,
				code.ParamBindError,
				code.Text(code.ParamBindError)).WithError(err),
			)
			return
		}
		res.TableId = strconv.FormatInt(h.cache.Incr(req.TableName), 10)

		ctx.Payload(res)
	}
}
