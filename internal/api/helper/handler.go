package helper

import (
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/redis"
	"gitee.com/sky-zhou/fangfang-api/internal/service/sys_authorized"

	"go.uber.org/zap"
)

var _ Handler = (*handler)(nil)

type Handler interface {
	i()

	// Md5 加密
	// @Tags Helper
	// @Router /helper/md5/{str} [get]
	Md5() core.HandlerFunc

	// Sign 签名
	// @Tags Helper
	// @Router /helper/sign [post]
	Sign() core.HandlerFunc

	// getTableID 获取表单id
	// @Tags Helper
	// @Router /helper/GetTableId/{} [get]
	GetTableId() core.HandlerFunc
}

type handler struct {
	logger            *zap.Logger
	db                mysql.Repo
	cache             redis.Repo
	authorizedService sys_authorized.Service
}

func New(logger *zap.Logger, db mysql.Repo, cache redis.Repo) Handler {
	return &handler{
		logger:            logger,
		db:                db,
		cache:             cache,
		authorizedService: sys_authorized.New(db, cache),
	}
}

func (h *handler) i() {}
