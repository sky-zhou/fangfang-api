package system

import (
	"gitee.com/sky-zhou/fangfang-api/pkg/env"
	"net/http"

	"gitee.com/sky-zhou/fangfang-api/internal/code"
	"gitee.com/sky-zhou/fangfang-api/internal/model/req"
	"gitee.com/sky-zhou/fangfang-api/internal/model/res"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
)

//{
//    "table": "SYS_DICT",
//    "mdata": {
//        "DICT_NAME": "sys_displaytype",
//        "DESCRIPTION": "字段显示控件"
//    },
//    "idata": [
//        {
//            "SYS_DICTITEM": {
//                "ORDERNO": 1,
//                "LABEL": "输入框",
//                "VALUE": "INTPUT"
//            }
//        },
//        {
//            "SYS_DICTITEM": {
//                "ORDERNO": 2,
//                "LABEL": "下拉单选",
//                "VALUE": "SELECT"
//            }
//        }
//    ]
//}

// CreateObject CreateApi 标准新增
// @Summary 标准表单新增
// @Tags system
// @accept application/json
// @Produce application/json
// @Param data body req.CreateRequest true "表名,主表字段，明细"
// @Success 200 {object} res.Res
// @Failure 400 {object} code.Failure
// @Router /api/system/CreateApi [post]
func (h *handler) CreateObject() core.HandlerFunc {
	return func(c core.Context) {
		rq := new(req.CreateRequest)
		data := new(res.CreateResponse)
		if err := c.ShouldBindJSON(rq); err != nil {
			c.AbortWithError(core.Error(
				http.StatusBadRequest,
				code.ParamBindError,
				code.Text(code.ParamBindError)).WithError(err),
			)
			return
		}

		id, err := h.service.Create(c, rq)
		if err != nil {
			if env.Active().IsPro() {
				c.AbortWithError(core.Error(
					http.StatusBadRequest,
					code.AuthorizedCreateError,
					code.Text(code.AuthorizedCreateError)).WithError(err),
				)
			}
			c.AbortWithError(core.Error(
				http.StatusBadRequest,
				code.AuthorizedCreateError,
				err.Error(),
			))
			return
		}

		data.Id = id

		res.OkWithData(data, c)

		//c.Payload(res)
	}
}
