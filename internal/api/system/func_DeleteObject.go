package system

import (
	"gitee.com/sky-zhou/fangfang-api/internal/code"
	"gitee.com/sky-zhou/fangfang-api/internal/model/req"
	"gitee.com/sky-zhou/fangfang-api/internal/model/res"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"net/http"
)

// DeleteObject DeleteApi 标准删除
// @Summary 标准表单删除(逻辑)
// @Tags system
// @accept application/json
// @Produce application/json
// @Param data body req.DeleteReq true "表名,行记录id"
// @Success 200 {object} res.Res
// @Failure 400 {object} code.Failure
// @Router /api/system/DeleteApi [post]
func (h *handler) DeleteObject() core.HandlerFunc {
	return func(c core.Context) {
		rq := new(req.DeleteReq)
		//rs := new(res.DeleteRes)
		if err := c.ShouldBindJSON(rq); err != nil {
			c.AbortWithError(core.Error(
				http.StatusBadRequest,
				code.ParamBindError,
				code.Text(code.ParamBindError)).WithError(err),
			)
			return
		}
		rs := h.service.DeleteObject(c, rq.Table, rq.Ids)
		//if err != nil {
		//	if env.Active().IsPro() {
		//		c.AbortWithError(core.Error(
		//			http.StatusBadRequest,
		//			code.AuthorizedCreateError,
		//			code.Text(code.AuthorizedCreateError)).WithError(err),
		//		)
		//	}
		//	c.AbortWithError(core.Error(
		//		http.StatusBadRequest,
		//		code.AuthorizedCreateError,
		//		err.Error(),
		//	))
		//	return
		//}

		//rs.Count = count

		res.OkWithData(rs, c)
	}
}
