package system

import (
	"gitee.com/sky-zhou/fangfang-api/config"
	"gitee.com/sky-zhou/fangfang-api/internal/model/res"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/redis"

	"github.com/mojocn/base64Captcha"
)

// GetCaptcha 生成验证码
// @Tags system
// @Summary 生成验证码
// @accept application/json
// @Produce application/json
// @Success 200 {object} res.Res
// @Router /api/system/GetCaptcha [post]
func (h *handler) GetCaptcha() core.HandlerFunc {
	return func(c core.Context) {

		var store = redis.NewCaptchaRedisStore(h.cache, c)

		driver := base64Captcha.NewDriverDigit(config.Get().Captcha.ImgHeight, config.Get().Captcha.ImgWidth, config.Get().Captcha.KeyLong, 0.7, 80)

		cp := base64Captcha.NewCaptcha(driver, store)

		if id, b64s, err := cp.Generate(); err != nil {
			res.FailWithMessage("验证码获取失败", c)
		} else {
			res.OkWithDetailed(res.SysCaptchaResponse{
				CaptchaId:     id,
				PicPath:       b64s,
				CaptchaLength: config.Get().Captcha.KeyLong,
			}, "验证码获取成功", c)
		}

	}
}
