package system

import (
	"gitee.com/sky-zhou/fangfang-api/internal/code"
	"gitee.com/sky-zhou/fangfang-api/internal/model/req"
	"gitee.com/sky-zhou/fangfang-api/internal/model/res"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"gitee.com/sky-zhou/fangfang-api/pkg/env"
	"net/http"
)

// GetObject  获取单对象接口
// @Summary 标准表单新增
// @Tags system
// @accept application/json
// @Produce application/json
// @Param data body req.GetObj true "表名,ID,是否解析外键"
// @Success 200 {object} res.Res
// @Failure 400 {object} code.Failure
// @Router /api/system/GetObject [post]
func (h *handler) GetObject() core.HandlerFunc {
	return func(c core.Context) {
		rq := new(req.GetObj)
		if err := c.ShouldBindJSON(rq); err != nil {
			c.AbortWithError(core.Error(
				http.StatusBadRequest,
				code.ParamBindError,
				code.Text(code.ParamBindError)).WithError(err),
			)
			return
		}
		data, err := h.service.GetObject(rq, c)
		if err != nil {
			if env.Active().IsPro() {
				c.AbortWithError(core.Error(
					http.StatusBadRequest,
					code.AuthorizedCreateError,
					code.Text(code.AuthorizedCreateError)).WithError(err),
				)
			}
			c.AbortWithError(core.Error(
				http.StatusBadRequest,
				code.AuthorizedCreateError,
				err.Error(),
			))
			return
		}

		res.OkWithData(data, c)
	}
}
