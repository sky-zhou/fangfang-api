package system

import (
	"gitee.com/sky-zhou/fangfang-api/internal/code"
	"gitee.com/sky-zhou/fangfang-api/internal/model/res"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"net/http"
)

// GetUserMenu 获取用户可见菜单
// @Summary 获取用户可见菜单过滤权限
// @Tags system
// @accept application/json
// @Produce application/json
// @Success 200 {object} res.Res
// @Failure 400 {object} code.Failure
// @Router /api/system/GetUserMenu [post]
func (h *handler) GetUserMenu() core.HandlerFunc {
	return func(c core.Context) {
		menu, err := h.userService.GetUserMenu(c.SessionUserInfo().UserID, c)
		if err != nil {
			c.AbortWithError(core.Error(
				http.StatusBadRequest,
				code.AdminMenuListError,
				code.Text(code.AdminMenuListError)).WithError(err),
			)
			return
		}
		res.OkWithData(menu, c)
	}
}
