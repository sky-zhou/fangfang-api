package system

import (
	"gitee.com/sky-zhou/fangfang-api/config"
	"gitee.com/sky-zhou/fangfang-api/internal/model/req"
	"gitee.com/sky-zhou/fangfang-api/internal/model/res"
	"gitee.com/sky-zhou/fangfang-api/internal/proposal"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/redis"
	"gitee.com/sky-zhou/fangfang-api/pkg/errors"
	"gitee.com/sky-zhou/fangfang-api/pkg/utils"
	"net/http"
	"strconv"

	"gitee.com/sky-zhou/fangfang-api/internal/code"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
)

type loginResponse struct {
	Token string `json:"token"` // 用户身份标识
}

// Login 用户登录接口
// @Summary 用户登录接口
// @Tags system
// @accept application/json
// @Produce application/json
// @Param data body req.LoginReq true "用户名,手机号,公司id,密码,验证码"
// @Success 200 {object} res.Res
// @Failure 400 {object} code.Failure
// @Router /api/system/Login [post]
func (h *handler) Login() core.HandlerFunc {
	return func(c core.Context) {
		rq := new(req.LoginReq)
		rp := new(loginResponse)
		if err := c.ShouldBindJSON(rq); err != nil {
			c.AbortWithError(core.Error(
				http.StatusBadRequest,
				code.ParamBindError,
				code.Text(code.ParamBindError)).WithError(err),
			)
			return
		}

		info, err := h.userService.GetUserInfo(c, *rq)
		if err != nil {
			c.AbortWithError(core.Error(
				http.StatusBadRequest,
				code.AdminLoginError,
				code.Text(code.AdminLoginError)).WithError(err),
			)
			return
		}

		if info == nil {
			c.AbortWithError(core.Error(
				http.StatusBadRequest,
				code.AdminLoginError,
				code.Text(code.AdminLoginError)).WithError(errors.New("未查询出符合条件的用户")),
			)
			return
		}

		token := h.userService.GenerateLoginToken(info.Id)

		// 用户信息
		sessionUserInfo := &proposal.SessionUserInfo{
			UserID:     info.Id,
			UserCId:    info.CId,
			UserName:   info.UserName,
			UserDeptId: info.SysDeptId,
		}

		// 将用户信息记录到 Redis 中
		err = h.cache.Set(config.RedisKeyPrefixLoginUser+token, string(sessionUserInfo.Marshal()), config.LoginSessionTTL, redis.WithTrace(c.Trace()))
		if err != nil {
			c.AbortWithError(core.Error(
				http.StatusBadRequest,
				code.AdminLoginError,
				code.Text(code.AdminLoginError)).WithError(err),
			)
			return
		}

		//获取用户菜单权限记录到redis中
		menu, err := h.userService.GetUserMenu(sessionUserInfo.UserID, c)
		if err != nil {
			c.AbortWithError(core.Error(
				http.StatusBadRequest,
				code.AdminLoginError,
				code.Text(code.AdminLoginError)).WithError(err),
			)
			return
		}
		err = h.cache.Set(config.RedisKeyPrefixUserMenu+strconv.FormatInt(sessionUserInfo.UserID, 10), string(utils.JsonMarshal(menu)), config.LoginSessionTTL, redis.WithTrace(c.Trace()))
		if err != nil {
			c.AbortWithError(core.Error(
				http.StatusBadRequest,
				code.AdminLoginError,
				code.Text(code.AdminLoginError)).WithError(err),
			)
			return
		}

		rp.Token = token
		res.OkWithData(rp, c)
		//c.Payload(rp)
	}
}
