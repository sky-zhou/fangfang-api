package system

import (
	"net/http"

	"gitee.com/sky-zhou/fangfang-api/internal/code"
	"gitee.com/sky-zhou/fangfang-api/internal/model/req"
	"gitee.com/sky-zhou/fangfang-api/internal/model/res"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
)

// ReloadDbByTable  重载配置表单
// @Summary 重载配置表单(如在数据库中新增一张表单，可以通过该接口动态生成配置)
// @Tags system
// @accept application/json
// @Produce application/json
// @Param data body req.ReloadTableRequest true "表名"
// @Success 200 {object} res.Res
// @Failure 400 {object} code.Failure
// @Router /api/system/ReloadDbByTable [post]
func (h *handler) ReloadDbByTable() core.HandlerFunc {
	return func(c core.Context) {
		rq := new(req.ReloadTableRequest)

		if err := c.ShouldBindJSON(rq); err != nil {
			c.AbortWithError(core.Error(
				http.StatusBadRequest,
				code.ParamBindError,
				code.Text(code.ParamBindError)).WithError(err),
			)
			return
		}
		err := h.service.ReloadMysqlTable(c, rq)
		if err != nil {
			c.AbortWithError(core.Error(
				http.StatusBadRequest,
				code.AuthorizedCreateError,
				code.Text(code.AuthorizedCreateError)).WithError(err),
			)
			return
		}
		res.Ok(c)
		//c.Payload(res)
	}
}
