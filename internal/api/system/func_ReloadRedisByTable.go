package system

import (
	"gitee.com/sky-zhou/fangfang-api/internal/model/res"
	"net/http"

	"gitee.com/sky-zhou/fangfang-api/internal/code"
	"gitee.com/sky-zhou/fangfang-api/internal/model/req"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
)

// ReloadRedisTable  重载缓存表单
// @Summary 根据表单字段配置，重载缓存中的数据
// @Tags system
// @accept application/json
// @Produce application/json
// @Param data body req.ReloadTableRequest true "表名"
// @Success 200 {object} res.Res
// @Failure 400 {object} code.Failure
// @Router /api/system/ReloadRedisTable [post]
func (h *handler) ReloadRedisTable() core.HandlerFunc {
	return func(c core.Context) {
		rq := new(req.ReloadTableRequest)

		if err := c.ShouldBindJSON(rq); err != nil {
			c.AbortWithError(core.Error(
				http.StatusBadRequest,
				code.ParamBindError,
				code.Text(code.ParamBindError)).WithError(err),
			)
			return
		}
		err := h.service.ReloadRedisTable(c, rq)
		if err != nil {
			c.AbortWithError(core.Error(
				http.StatusBadRequest,
				code.AuthorizedCreateError,
				code.Text(code.AuthorizedCreateError)).WithError(err),
			)
			return
		}
		res.Ok(c)
	}
}
