package system

import (
	"gitee.com/sky-zhou/fangfang-api/internal/code"
	"gitee.com/sky-zhou/fangfang-api/internal/model/req"
	"gitee.com/sky-zhou/fangfang-api/internal/model/res"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"gitee.com/sky-zhou/fangfang-api/pkg/env"
	"net/http"
)

// UpdateAPI  标准更新接口
// @Summary 标准更新接口
// @Tags system
// @accept application/json
// @Produce application/json
// @Param data body req.UpdateRequest true "表名,主表需要更新的字段,明细表需要更新的记录"
// @Success 200 {object} res.Res
// @Failure 400 {object} code.Failure
// @Router /api/system/UpdateAPI [post]
func (h *handler) UpdateObject() core.HandlerFunc {
	return func(c core.Context) {
		rq := new(req.UpdateRequest)

		if err := c.ShouldBindJSON(rq); err != nil {
			c.AbortWithError(core.Error(
				http.StatusBadRequest,
				code.ParamBindError,
				code.Text(code.ParamBindError)).WithError(err),
			)
			return
		}
		err := h.service.Update(c, rq)
		if err != nil {
			if env.Active().IsPro() {
				c.AbortWithError(core.Error(
					http.StatusBadRequest,
					code.AuthorizedCreateError,
					code.Text(code.AuthorizedCreateError)).WithError(err),
				)
			}
			c.AbortWithError(core.Error(
				http.StatusBadRequest,
				code.AuthorizedCreateError,
				err.Error(),
			))
			return
		}

		res.Ok(c)

		//c.Payload(res)
	}
}
