package system

import (
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/redis"
	"gitee.com/sky-zhou/fangfang-api/internal/service/sys_service"
	"gitee.com/sky-zhou/fangfang-api/internal/service/sys_user"

	"go.uber.org/zap"
)

var _ Handler = (*handler)(nil)

type Handler interface {
	i()

	// CreateApi 新增
	// @Tags system
	// @Router /api/system/CreateApi [post]
	CreateObject() core.HandlerFunc

	// UpdateApi 修改
	// @Tags system
	// @Router /api/system/UpdateApi [post]
	UpdateObject() core.HandlerFunc

	// DeleteApi 删除
	// @Tags system
	// @Router /api/system/DeleteApi [post]
	DeleteObject() core.HandlerFunc

	// ReloadRedisTable 重载缓存数据
	// @Tags system
	// @Router /api/system/ReloadRedisTable [post]
	ReloadRedisTable() core.HandlerFunc

	// ReloadDbByTable 重载表单配置
	// @Tags system
	// @Router /api/system/ReloadDbByTable [post]
	ReloadDbByTable() core.HandlerFunc

	// Login 用户登录
	// @Tags system
	// @Router /api/system/Login [post]
	Login() core.HandlerFunc

	// QueryList 列表查询
	// @Tags system
	// @Router /api/system/QueryList [post]
	QueryList() core.HandlerFunc

	// GetCaptcha 获取单对象接口
	// @Tags system
	// @Router /api/system/GetObject [post]
	GetObject() core.HandlerFunc

	// GetUserMenu 获取登录用户菜单
	// @Tags system
	// @Router /api/system/GetUserMenu [post]
	GetUserMenu() core.HandlerFunc

	// GetCaptcha 获取验证码
	// @Tags system
	// @Router /api/system/GetCaptcha [post]
	GetCaptcha() core.HandlerFunc
}

type handler struct {
	logger      *zap.Logger
	db          mysql.Repo
	cache       redis.Repo
	service     sys_service.Service
	userService sys_user.Service
}

func New(logger *zap.Logger, db mysql.Repo, cache redis.Repo) Handler {
	return &handler{
		logger:      logger,
		db:          db,
		cache:       cache,
		service:     sys_service.New(db, cache),
		userService: sys_user.New(db, cache),
	}
}

func (h *handler) i() {}
