package consts

const (
	IsUsedY = "Y" // 启用
	IsUsedN = "N" // 禁用
)

const (
	IsDeleteN = "N" // 未删
	IsDeleteY = "Y" // 已删
)

const (
	IsBigY = "Y" // 是海量
	IsBigN = "N" // 非海量
)

const (
	IsMenuYes = "Y" // 是菜单
	IsMenuNo  = "N" // 非菜单
)

const (
	IsYes = "Y" //
	IsNo  = "N" //
)

const (
	Read     int64 = 1
	Write    int64 = 2
	Submit   int64 = 4
	Unsubmit int64 = 8
	Imp      int64 = 16
)

const (
	C = "C" //新增
	R = "R" //读取
	U = "U" //更新
	D = "D" //删除
	S = "S" //提交
	B = "B" //反提交
	I = "I" //导出
	V = "V" //作废
)
