package req

//标准新增请求
//{
//    "table": "SYS_DICT",
//    "mdata": {
//        "DICT_NAME": "sys_displaytype6",
//        "DESCRIPTION": "字段显示控件6"
//    },
//    "idata": [
//        {
//            "SYS_DICTITEM": {
//                "ORDERNO": 1,
//                "LABEL": "输入框",
//                "VALUE": "INTPUT"
//            }
//        },
//        {
//            "SYS_DICTITEM": {
//                "ORDERNO": 2,
//                "LABEL": "下拉单选",
//                "VALUE": "SELECT"
//            }
//        }
//    ]
//}
type CreateRequest struct {
	Table string                 `json:"table"` //表单名称
	Mdata map[string]interface{} `json:"mdata"` //父记录对象
	//TODO:后期增加实现主表加明细
	Idata []map[string]interface{} `json:"idata"` //明细数据
}

//表单重载
type ReloadTableRequest struct {
	Tables []string `json:"table"` //表单名称大写list
}
