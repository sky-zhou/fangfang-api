package req

type DeleteReq struct {
	Table string  `json:"table"` //表单名称
	Ids   []int64 `json:"ids"`   //id list
}
