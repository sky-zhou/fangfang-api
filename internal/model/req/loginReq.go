package req

//{
//    "username":"admin",
//    "password":"abc123",
//    "cap":{
//        "captcha":"jfadjlfd",
//        "captchaId":"fdafjldskf"
//    }
//}
//说明：

//登录
type LoginReq struct {
	Username string  `json:"username"` //用户名称
	Phone    string  `json:"phone"`    //手机号
	CId      int64   `json:"cid"`      //客户id
	Password string  `json:"password"` //密码
	Captcha  Captcha `json:"cap"`      //验证码
}

type Captcha struct {
	Captcha   string `json:"captcha"`   // 验证码
	CaptchaId string `json:"captchaId"` // 验证码ID"`
}
