package req

import "gitee.com/sky-zhou/fangfang-api/internal/repo/mysql"

//{
//    "table": "SYS_COLUMN",
//    "fixColumn": [
//        {
//            "col": "IS_USED",
//            "key": "Y",
//            "pdc": "="
//        },
//        {
//            "col": "C_ID",
//            "key": "1",
//            "pdc": "="
//        }
//    ],
//    "order": [{
//        "col":"ID",
//        "asc":false
//    }],
//    "isNeedFk": true,
//    "page": 1,
//    "row": 10,
//    "isNeedTot":true
//}
//说明pdc 可以不传，不穿情况根据字段类型进行判断

//查询
type Query struct {
	Table     string     `json:"table"`     //表单名称
	FixColum  []Fixcol   `json:"fixColumn"` //条件
	IsNeedFk  bool       `json:"isNeedFk"`  //是否解析外键
	Page      int        `json:"page"`      //页
	Row       int        `json:"row"`       //行数
	Order     []OrderCol `json:"order"`     //排序
	IsNeedTot bool       `json:"isNeedTot"` //是否需要返回合计
}

type Fixcol struct {
	Col string          `json:"col"` //字段名称
	Key interface{}     `json:"key"` //查询值
	Pdc mysql.Predicate `json:"pdc"` //关系
}

type OrderCol struct {
	Col string `json:"col"`
	Asc bool   `json:"asc"`
}
