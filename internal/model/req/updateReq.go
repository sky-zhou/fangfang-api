package req

//{
//    "table": "SYS_DICT",
//    "mdata": {
//        "ID":12,
//        "DICT_NAME": "sys_displaytype6",
//        "DESCRIPTION": "字段显示控55"
//    },
//    "idata": [
//        {
//            "SYS_DICTITEM": {
//                "ID":24,
//                "ORDERNO": 3,
//                "LABEL": "输入框5",
//                "VALUE": "INTPUT5"
//            }
//        },
//         {
//            "SYS_DICTITEM": {
//                "ORDERNO": 5,
//                "LABEL": "输入框6",
//                "VALUE": "INTPUT6"
//            }
//        }
//    ]
//}
//说明：更新记录，主记录必须需要有”ID“字段，如果没有，则默认为新增

//修改新增请求
type UpdateRequest struct {
	Table string                 `json:"table"` //表单名称
	Mdata map[string]interface{} `json:"mdata"` //主记录更新对象
	//TODO:后期增加实现主表加明细
	Idata []map[string]interface{} `json:"idata"` //明细记录
}
