package res

import (
	"net/http"

	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
)

type CreateResponse struct {
	Id int64 `json:"ID"` // 主键ID
}

type DeleteRes struct {
	//Count      int64   `json:"Count"`
	SCount     int64    `json:"sCount"`
	ECount     int64    `json:"eCount"`
	ErrMessage []string `json:"ErrMessage"`
}

type QueryRes struct {
	Rows   []map[string]interface{} `json:"rows"`
	TotRow int64                    `json:"totRow"`
}

type Res struct {
	Code int         `json:"code"` //返回状态码
	Data interface{} `json:"data"` //返回数据
	Msg  string      `json:"msg"`  //返回消息
}

type SysCaptchaResponse struct {
	CaptchaId     string `json:"captchaId"`
	PicPath       string `json:"picPath"`
	CaptchaLength int    `json:"captchaLength"`
}

const (
	ERROR   = 3000
	SUCCESS = 0
)

func Result(code int, data interface{}, msg string, c core.Context) {
	c.JSON(http.StatusOK, Res{
		code,
		data,
		msg,
	})
}

func Ok(c core.Context) {
	Result(SUCCESS, map[string]interface{}{}, "操作成功", c)
}

func OkWithMessage(message string, c core.Context) {
	Result(SUCCESS, map[string]interface{}{}, message, c)
}

func OkWithData(data interface{}, c core.Context) {
	Result(SUCCESS, data, "操作成功", c)
}
func OkWithDetailed(data interface{}, message string, c core.Context) {
	Result(SUCCESS, data, message, c)
}

func Fail(c core.Context) {
	Result(ERROR, map[string]interface{}{}, "操作失败", c)
}

func FailWithMessage(message string, c core.Context) {
	Result(ERROR, map[string]interface{}{}, message, c)
}

func FailWithDetailed(data interface{}, message string, c core.Context) {
	Result(ERROR, data, message, c)
}
