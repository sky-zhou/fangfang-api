package authorized

import "time"

// Authorized 已授权的调用方表
type Authorized struct {
	Id                int32     `json:"id" gorm:"column:id"`                                 // 主键
	CId               int       `json:"c_id" gorm:"column:c_id"`                             //所属公司
	BusinessKey       string    `json:"business_key" gorm:"column:business_key"`             // 调用方key
	BusinessSecret    string    `json:"business_secret" gorm:"column:business_secret"`       // 调用方secret
	BusinessDeveloper string    `json:"business_developer" gorm:"column:business_developer"` // 调用方对接人
	Remark            string    `json:"remark" gorm:"column:remark"`                         // 备注
	IsUsed            string    `json:"is_used" gorm:"column:is_used"`                       // 是否启用 Y:是  N:否
	IsDeleted         string    `json:"is_deleted" gorm:"column:is_deleted"`                 // 是否删除 Y:是  N:否
	CreatedAt         time.Time `json:"created_at" gorm:"time;column:created_at"`            // 创建时间
	CreatedUser       string    `json:"created_user" gorm:"column:created_user"`             // 创建人
	UpdatedAt         time.Time `json:"updated_at" gorm:"time;column:update_at"`             // 更新时间
	UpdatedUser       string    `json:"updated_user" gorm:"column:updated_user"`             // 更新人
}
