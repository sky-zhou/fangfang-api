package authorized_api

import "time"

// AuthorizedApi 已授权接口地址表
type AuthorizedApi struct {
	Id          int32     `json:"id" gorm:"column:id"`                      // 主键
	BusinessKey string    `json:"business_key" gorm:"column:business_key"`  // 调用方key
	Method      string    `json:"method" gorm:"column:method"`              // 请求方式
	Api         string    `json:"api" gorm:"column:api"`                    // 请求地址
	IsDeleted   string    `json:"is_deleted" gorm:"column:is_deleted"`      // 是否删除 1:是  -1:否
	CreatedAt   time.Time `json:"created_at" gorm:"time;column:created_at"` // 创建时间
	CreatedUser string    `json:"created_user" gorm:"column:created_user"`  // 创建人
	UpdatedAt   time.Time `json:"updated_at" gorm:"time;column:updated_at"` // 更新时间
	UpdatedUser string    `json:"updated_user" gorm:"column:updated_user"`  // 更新人
}
