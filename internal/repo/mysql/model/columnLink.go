package model

type ColumnLink struct {
	Column
	FkColumn   Column
	FkTable    Table  `gorm:"-"` //外键表
	FkDkColumn Column `gorm:"-"` //关联表的显示字段
}
