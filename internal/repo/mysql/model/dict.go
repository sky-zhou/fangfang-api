package model

import "gorm.io/gorm"

type Dict struct {
	Id          int        `gorm:"column:id"`          // 主键
	CId         int        `gorm:"column:c_id"`        //
	DictName    string     `gorm:"column:dict_name"`   // 字典名称
	DictType    string     `gorm:"column:dict_type"`   // 字段类型
	Description string     `gorm:"column:description"` // 备注
	Dictitem    []Dictitem `gorm:"-"`
	Default     string     `gorm:"-"`
}

type Dictitem struct {
	Id          int    `gorm:"column:id"`          // 主键
	SysDictId   int    `gorm:"column:sys_dict_id"` //
	Orderno     int    `gorm:"column:orderno"`     // 序号
	Label       string `gorm:"column:label"`       // 字典标签
	Value       string `gorm:"column:value"`       // 字典值
	CssClass    string `gorm:"column:css_class"`   // 单对象样式
	ListClass   string `gorm:"column:list_class"`  // 列表样式
	IsDefault   string `gorm:"column:is_default"`  // 是否默认(0:非默认 1:默认)
	Description string `gorm:"column:description"` // 备注
}

//获取所有的数据字典
func GetAllDicts(db *gorm.DB) (dicts []Dict, err error) {
	err = db.Table("sys_dict").Where("is_used = 'Y'").Scan(&dicts).Error
	if err != nil {
		return
	}
	for _, v := range dicts {
		println(v.CId)
	}
	return dicts, nil
}

//为单个数据字典设置默认值
func (d *Dict) SetDefault() {
	for i, val := range d.Dictitem {
		//这种方式确保始终存在默认值
		if i == 0 {
			d.Default = val.Value
		}
		if val.IsDefault == "Y" {
			d.Default = val.Value
		}
	}
}
