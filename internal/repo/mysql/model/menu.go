package model

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
)

type Menu struct {
	Id         int64  `gorm:"column:id"`           // 主键
	Name       string `gorm:"column:name"`         // 路由名称
	Url        string `gorm:"column:url"`          //路由地址或commpent
	Icon       string `gorm:"column:icon"`         //路由ico
	Orderno    int64  `gorm:"column:orderno"`      //路由排序
	ParentId   int64  `gorm:"column:parent_id"`    //父路由
	SysTableId int64  `gorm:"column:sys_table_id"` //所属表单
	Type       string `gorm:"column:type"`         //类型
	Permission int64  `gorm:"column:permission"`   //权限
	TableName  string `gorm:"column:table_name"`   //表单名称
	DataPrem   Prem   `gorm:"column:data_prem"`    //表单名称
	TableMask  string `gorm:"column:table_mask"`
}

//demo
//{"display":"(公司名称 含有 $user_cid$)","filter":{"clink":"sys_company.id","condition":"=$user_cid$"}}
type Prem struct {
	DisPlay string `json:"display"`
	Filter  Expr   `json:"filter"`
}

type Expr struct {
	Clink     string `json:"clink"`
	Condition string `json:"condition"`
	Pdc       string `json:"pdc"`
}

func (c Prem) Value() (driver.Value, error) {
	b, err := json.Marshal(c)
	return string(b), err
}

func (c *Prem) Scan(input interface{}) error {
	b, ok := input.([]byte)
	if !ok {
		return fmt.Errorf("value is not []byte, value: %v", input)
	}
	if len(b) == 0 {
		return nil
	}
	return json.Unmarshal(input.([]byte), c)
}

// Marshal 序列化到JSON
func (m *Menu) Marshal() (jsonRaw []byte) {
	jsonRaw, _ = json.Marshal(m)
	return
}
