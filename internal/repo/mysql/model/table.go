package model

import (
	"gitee.com/sky-zhou/fangfang-api/pkg/builder"
)

type Table struct {
	Id            int      `gorm:"column:id"`              // 主键
	CId           int      `gorm:"column:c_id"`            //
	IsUsed        string   `gorm:"column:is_used"`         // 是否启用 Y:是  N:否
	Disname       string   `gorm:"column:disname"`         // 显示名称
	Orderno       int      `gorm:"column:orderno"`         // 序号
	Name          string   `gorm:"column:name"`            // 数据库表名称
	PkColumnId    int      `gorm:"column:pk_column_id"`    // 主键
	RelTableId    int      `gorm:"column:rel_table_id"`    // 实际数据库表
	Filter        string   `gorm:"column:filter"`          // 过滤条件
	TableMask     string   `gorm:"column:table_mask"`      // 表单功能
	AdColumnId    int      `gorm:"column:ad_column_id"`    // 显示健
	DkColumnId    int      `gorm:"column:dk_column_id"`    // 输入健
	Url           string   `gorm:"column:url"`             // 网页链接
	IsMenu        string   `gorm:"column:is_menu"`         // 是否菜单 Y是N否
	ParentTableId int      `gorm:"column:parent_table_id"` // 父表
	TrigAc        string   `gorm:"column:trig_ac"`         // 新增后触发
	TrigAm        string   `gorm:"column:trig_am"`         // 修改后触发
	TrigBm        string   `gorm:"column:trig_bm"`         // 删除前触发
	ProcSubmit    string   `gorm:"column:proc_submit"`     // 提交程序
	ProcUnsubmit  string   `gorm:"column:proc_unsubmit"`   // 反提交程序
	IsBig         string   `gorm:"column:is_big"`          // N否Y海量
	Props         string   `gorm:"column:props"`           // 扩展属性
	Comments      string   `gorm:"column:comments"`        // 备注
	Cols          []Column `gorm:"-"`
}

func (t *Table) IsBigF() bool {
	if t.IsBig == "Y" {
		return true
	}
	return false
}

//根据table构建结构体
func (t *Table) CreateStruct() *builder.Struct {
	pe := builder.NewBuilder()
	//循环字段创建结构体
	for _, v := range t.Cols {
		if t.CheckColumCreate(v.Dbname) {
			continue
		}
		AddBuilderColum(pe, v)
	}
	return pe.Build()
}

//根据table构建结构体
//func (t *Table) CreateStructByAct(act string) *builder.Struct {
//	pe := builder.NewBuilder()
//	//循环字段创建结构体
//	for _, v := range t.Cols {
//		if t.CheckColumCreate(v.Dbname) {
//			continue
//		}
//		AddBuilderColum(pe, v)
//	}
//	return pe.Build()
//}

//根据table构建结构体
func (t *Table) CreateStructForUpdate(m map[string]interface{}) *builder.Struct {
	pe := builder.NewBuilder()
	//循环字段创建结构体
	for _, v := range t.Cols {
		for k := range m {
			if v.Dbname == k {
				if t.CheckColumUpdate(v.Dbname) {
					continue
				}
				AddBuilderColum(pe, v)
				continue
			}
		}
		if v.Dbname == "UPDATED_AT" {
			AddBuilderColum(pe, v)
			continue
		}
		if v.Dbname == "UPDATED_USER" {
			AddBuilderColum(pe, v)
			continue
		}
	}
	return pe.Build()
}

//根据table构建结构体
func (t *Table) CreateStructForDelete() *builder.Struct {
	pe := builder.NewBuilder()
	//循环字段创建结构体
	for _, v := range t.Cols {
		if v.Dbname == "DELETED_AT" {
			AddBuilderColum(pe, v)
			continue
		}
		if v.Dbname == "DELETED_USER" {
			AddBuilderColum(pe, v)
			continue
		}
		if v.Dbname == "IS_DELETED" {
			AddBuilderColum(pe, v)
			continue
		}
		if v.Dbname == "IS_USED" {
			AddBuilderColum(pe, v)
			continue
		}

	}
	return pe.Build()
}

func (s *Table) CheckColumCreate(tablename string) bool {
	var colList = []string{"DELETED_AT", "DELETED_USER"}
	for _, key := range colList {
		if tablename == key {
			return true
		}
	}
	return false
}

func (s *Table) CheckColumUpdate(tablename string) bool {
	var colList = []string{"DELETED_AT", "DELETED_USER", "CREATED_AT", "CREATED_USER"}
	for _, key := range colList {
		if tablename == key {
			return true
		}
	}
	return false
}

func AddBuilderColum(pe *builder.Builder, v Column) {
	switch v.Coltype {
	case "int":
		pe.AddInt64(v.Dbname)
	case "tinyint":
		pe.AddInt64(v.Dbname)
	case "char":
		pe.AddString(v.Dbname)
	case "varchar":
		pe.AddString(v.Dbname)
	case "float":
		pe.AddFloat64(v.Dbname)
	case "timestamp":
		pe.AddString(v.Dbname)
	case "decimal":
		pe.AddFloat64(v.Dbname)
	default:
		pe.AddString(v.Dbname)
	}
}
