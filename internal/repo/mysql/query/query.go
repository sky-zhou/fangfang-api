package query

import (
	"fmt"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/model"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/redis"
	"gitee.com/sky-zhou/fangfang-api/pkg/utils"
	"gorm.io/gorm"
	"strconv"
	"strings"
)

//单表查询
func NewQueryBuilder(table string, cache redis.Repo, ctx core.Context) *queryBuilder {
	qb := new(queryBuilder)
	//获取table缓存模型
	Table, _ := cache.GetTable(strings.ToUpper(table), redis.WithTrace(ctx.Trace()))
	qb.cols = Table.Cols
	qb.table = *Table
	for _, v := range Table.Cols {
		if v.FkColumnId > 0 {
			//设置外键关联字段
			fkcol, _ := cache.GetColumn(v.FkColumnId, redis.WithTrace(ctx.Trace()))
			var colLink = &model.ColumnLink{Column: v}
			colLink.FkColumn = *fkcol

			//设置外键关联表
			fkTable, _ := cache.GetTableById(fkcol.SysTableId, redis.WithTrace(ctx.Trace()))
			colLink.FkTable = *fkTable

			//设置外键关联dk
			fkDkColum, _ := cache.GetColumn(fkTable.DkColumnId, redis.WithTrace(ctx.Trace()))
			colLink.FkDkColumn = *fkDkColum

			qb.colLinks = append(qb.colLinks, *colLink)
		}
	}
	return qb
}

//单表
type queryBuilder struct {
	table    model.Table
	cols     []model.Column
	colLinks []model.ColumnLink
	order    []string
	where    []struct {
		prefix string
		value  interface{}
	}
	limit  int
	offset int
}

//复杂表单查询
//例如： SELECT sys_user.id id,sys_user.user_name user_name,sys_user.c_id c_id,
//		a1.NAME c_id__name
//		FROM sys_user
//		LEFT JOIN sys_company a1 ON a1.id = sys_user.c_id
func (qb *queryBuilder) buildQueryR(isCount bool, db *gorm.DB) *gorm.DB {
	ret := db
	ret = ret.Table(qb.table.Name)
	//构造sql
	//var sql string

	var cols []string
	for _, col := range qb.cols {
		cols = append(cols, col.FullDbname+" "+col.Dbname)
	}
	for i, col := range qb.colLinks {
		cols = append(cols, "a"+strconv.Itoa(i)+"."+col.FkDkColumn.Dbname+" "+col.Dbname+"__DK")
		ret = ret.Joins("left join " + col.FkTable.Name + " " + "a" + strconv.Itoa(i) + " on  a" + strconv.Itoa(i) + "." + col.FkColumn.Dbname + "=" + col.FullDbname)
	}
	ret = ret.Select(cols)

	//设置查询条件
	//TODO:后期增加多值查询
	for _, where := range qb.where {
		ret = ret.Where(where.prefix, where.value)
	}
	//增加默认查询条件
	ret.Where(qb.table.Name + "." + "is_deleted = 'N'")

	//设置默认排序
	for _, order := range qb.order {
		ret = ret.Order(order)
	}
	if isCount {
		return ret
	}
	ret = ret.Limit(qb.limit).Offset(qb.offset)
	return ret
}

//单表
func (qb *queryBuilder) buildQuery(isCount bool, db *gorm.DB) *gorm.DB {
	ret := db
	ret = ret.Table(qb.table.Name)

	//设置需要查询的字段
	var cols []string
	for _, col := range qb.table.Cols {
		cols = append(cols, col.Dbname)
	}
	ret = ret.Select(utils.ListToString(cols))

	//设置查询条件
	//TODO:后期增加多值查询
	for _, where := range qb.where {
		ret = ret.Where(where.prefix, where.value)
	}

	//增加默认查询条件
	ret.Where("is_deleted = 'N'")

	//设置默认排序
	for _, order := range qb.order {
		ret = ret.Order(order)
	}
	if isCount {
		return ret
	}
	ret = ret.Limit(qb.limit).Offset(qb.offset)
	return ret
}

//关联表单列表查询(有外键效果)
func (qb *queryBuilder) GetListR(db *gorm.DB) ([]map[string]interface{}, error) {

	ret := []map[string]interface{}{}
	//执行查询
	res := qb.buildQueryR(false, db).Scan(&ret)
	if res.Error != nil && res.Error == gorm.ErrRecordNotFound {
		ret = nil
	}
	return ret, res.Error
}

//关联表单单对象查询(有外键效果)
func (qb *queryBuilder) GetObjectR(db *gorm.DB) (map[string]interface{}, error) {
	ret := map[string]interface{}{}
	qb.limit = 1
	//执行查询
	res := qb.buildQueryR(false, db).Take(&ret)
	if res.Error != nil && res.Error == gorm.ErrRecordNotFound {
		ret = nil
	}
	return ret, res.Error
}

//单表单单对象查询(无外键效果)
func (qb *queryBuilder) GetObject(db *gorm.DB) (map[string]interface{}, error) {
	ret := map[string]interface{}{}
	qb.limit = 1
	//执行查询
	res := qb.buildQuery(false, db).Take(&ret)
	if res.Error != nil && res.Error == gorm.ErrRecordNotFound {
		ret = nil
	}
	return ret, res.Error
}

//单表单列表查询(无外键效果)
func (qb *queryBuilder) GetList(db *gorm.DB) ([]map[string]interface{}, error) {

	ret := []map[string]interface{}{}
	//执行查询
	res := qb.buildQuery(false, db).Scan(&ret)
	if res.Error != nil && res.Error == gorm.ErrRecordNotFound {
		ret = nil
	}
	return ret, res.Error
}

//获取该查询条件下的总数
func (qb *queryBuilder) Count(db *gorm.DB) (int64, error) {
	var ret int64
	//执行查询
	res := qb.buildQuery(true, db).Count(&ret)
	if res.Error != nil && res.Error == gorm.ErrRecordNotFound {
		ret = 0
	}
	return ret, res.Error
}

//增加where查询条件
func (qb *queryBuilder) Where(col string, value interface{}, p mysql.Predicate) *queryBuilder {

	qb.where = append(qb.where, struct {
		prefix string
		value  interface{}
	}{
		fmt.Sprintf("%v %v ?", col, p),
		value,
	})

	return qb
}

//增加order by
func (qb *queryBuilder) OrderBy(col string, asc bool) *queryBuilder {
	order := "DESC"
	if asc {
		order = "ASC"
	}
	qb.order = append(qb.order, col+" "+order)
	return qb
}

func (qb *queryBuilder) Limit(limit int) *queryBuilder {
	qb.limit = limit
	return qb
}

func (qb *queryBuilder) Offset(offset int) *queryBuilder {
	qb.offset = offset
	return qb
}
