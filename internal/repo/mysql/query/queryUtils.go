package query

import (
	"gitee.com/sky-zhou/fangfang-api/internal/proposal"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/model"
	"gitee.com/sky-zhou/fangfang-api/pkg/builder"
	"gitee.com/sky-zhou/fangfang-api/pkg/utils"
)

func PreaseUserEnv(d string, user proposal.SessionUserInfo) interface{} {
	var val interface{}
	switch d {
	case "@UserId@":
		val = utils.PraseInterfaceToString(user.UserID)
	case "@UserCid@":
		val = utils.PraseInterfaceToString(user.UserCId)
	case "@UserName@":
		val = utils.PraseInterfaceToString(user.UserName)
	case "@UserDeptID@":
		val = utils.PraseInterfaceToString(user.UserDeptId)
	default:
		val = d
	}
	return val
}

//根据字段判断具体字段与值之间的关系
//TODO:后期进行不同字段类型进行不同的关系返回
func GetPdc(v model.Column) mysql.Predicate {
	switch v.Coltype {
	case "int":
		return mysql.EqualPredicate
	case "tinyint":
		return mysql.EqualPredicate
	case "char":
		return mysql.EqualPredicate
	case "varchar":
		return mysql.LikePredicate
	case "float":
		return mysql.EqualPredicate
	case "timestamp":
		return mysql.EqualPredicate
	default:
		return mysql.EqualPredicate
	}
}

//根据传入参数、默认值、用户环境变量给字段赋值
func GetColumnValue(row *builder.Instance, v *model.Column, value interface{}, user proposal.SessionUserInfo) {
	var val interface{}

	if value != nil {
		val = utils.PraseInterfaceToString(value)
	} else {
		if len(v.DefaultValue) > 0 {
			val = PreaseUserEnv(v.DefaultValue, user)
		} else {
			return
		}
	}
	colname := v.Dbname
	row.SetInterface(colname, v.Coltype, val)
}
