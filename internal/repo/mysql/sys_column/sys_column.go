package sys_column

import "time"

// SysColumn 模板表单
type SysColumn struct {
	Id           int64  `gorm:"column:id"`            // 主键
	CId          int64  `gorm:"column:c_id"`          // 所属客户
	SysTableId   int64  `gorm:"column:sys_table_id"`  // 所属表单
	Comment      string `gorm:"column:comment"`       // 显示名称
	Mask         string `gorm:"column:mask"`          // 读写规则(AMDQ)
	Orderno      int    `gorm:"column:orderno"`       // 字段排序序号
	Dbname       string `gorm:"column:dbname"`        // 数据库名称
	FullDbname   string `gorm:"column:full_dbname"`   // 数据库全名
	Coltype      string `gorm:"column:coltype"`       // 字段类型
	IsAk         string `gorm:"column:is_ak"`         // 是否输入字段
	IsDk         string `gorm:"column:is_dk"`         // 是否显示字段
	IsNull       string `gorm:"column:is_null"`       // 是否可以为空
	IsUpper      string `gorm:"column:is_upper"`      // 是否大写
	IsPk         string `gorm:"column:is_pk"`         // 是否主键
	DefaultValue string `gorm:"column:default_value"` // 默认值
	SysDictName  string `gorm:"column:sys_dict_name"` // 数据字典
	FkColumnId   int64  `gorm:"column:fk_column_id"`  // 外键关联
	Lenth        int    `gorm:"column:lenth"`         // 长度
	Precision    int    `gorm:"column:preci"`         // 精度
	HrColumnId   int64  `gorm:"column:hr_column_id"`  // 所属字段组
	DisplayType  int    `gorm:"column:display_type"`  // 显示类型

	IsUsed      string     `gorm:"column:is_used"`           // 是否删除 1未删 -1已删
	IsDeleted   string     `gorm:"column:is_deleted"`        // 是否删除 1未删 -1已删
	CreatedAt   time.Time  `gorm:"time;column:created_at"`   // 创建时间
	CreatedUser string     `gorm:"column:created_user"`      // 创建人
	UpdatedAt   time.Time  `gorm:"time;column:updated_at"`   // 更新时间
	UpdatedUser string     `gorm:"time;column:updated_user"` // 更新人
	DeletedAt   *time.Time `gorm:"time;column:deleted_at"`   // 删除时间
}

func (*SysColumn) TableName() string {
	return "sys_column"
}
