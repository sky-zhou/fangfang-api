package sys_dict

import "time"

// SysDict 模板表单
type SysDict struct {
	Id          int       // 主键
	CId         int       //
	IsUsed      int       // 是否启用 1:是  -1:否
	IsDeleted   int       // 是否删除 1未删 -1已删
	CreatedAt   time.Time `gorm:"time"` // 创建时间
	CreatedUser string    // 创建人
	UpdatedAt   time.Time `gorm:"time"` // 更新时间
	UpdatedUser string    // 更新人
	DeletedAt   time.Time `gorm:"time"` // 删除时间
	DeletedUser string    //
	DictName    string    // 字典名称
	DictType    string    // 字段类型
	Description string    // 备注
}
