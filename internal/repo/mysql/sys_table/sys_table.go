package sys_table

import "time"

// SysTable 配置表单
type SysTable struct {
	Id            int64  `gorm:"column:id"`              // 主键
	CId           int64  `gorm:"column:c_id"`            //
	IsUsed        string `gorm:"column:is_used"`         // 是否启用 Y:是  N:否
	Disname       string `gorm:"column:disname"`         // 显示名称
	Orderno       int    `gorm:"column:orderno"`         // 序号
	Name          string `gorm:"column:name"`            // 数据库表名称
	PkColumnId    int64  `gorm:"column:pk_column_id"`    // 主键
	RelTableId    int64  `gorm:"column:rel_table_id"`    // 实际数据库表
	Filter        string `gorm:"column:filter"`          // 过滤条件
	TableMask     string `gorm:"column:table_mask"`      // 表单功能
	AdColumnId    int64  `gorm:"column:ad_column_id"`    // 显示健
	DkColumnId    int64  `gorm:"column:dk_column_id"`    // 输入健
	Url           string `gorm:"column:url"`             // 网页链接
	IsMenu        string `gorm:"column:is_menu"`         // 是否菜单 Y是N否
	ParentTableId int64  `gorm:"column:parent_table_id"` // 父表
	TrigAc        string `gorm:"column:trig_ac"`         // 新增后触发
	TrigAm        string `gorm:"column:trig_am"`         // 修改后触发
	TrigBm        string `gorm:"column:trig_bm"`         // 删除前触发
	ProcSubmit    string `gorm:"column:proc_submit"`     // 提交程序
	ProcUnsubmit  string `gorm:"column:proc_unsubmit"`   // 反提交程序
	IsBig         string `gorm:"column:is_big"`          // N否Y海量
	Props         string `gorm:"column:props"`           // 扩展属性
	Comments      string `gorm:"column:comments"`        // 备注

	IsDeleted   string    `gorm:"column:is_deleted"`        // 是否删除 1未删 -1已删
	CreatedAt   time.Time `gorm:"time;column:created_at"`   // 创建时间
	CreatedUser string    `gorm:"column:created_user"`      // 创建人
	UpdatedAt   time.Time `gorm:"time;column:updated_at"`   // 更新时间
	UpdatedUser string    `gorm:"time;column:updated_user"` // 更新人
}

func (*SysTable) TableName() string {
	return "sys_table"
}
