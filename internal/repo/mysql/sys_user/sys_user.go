package sys_user

import "time"

// SysUser 用户表
type SysUser struct {
	Id          int64     `gorm:"column:id"`              // 主键
	CId         int64     `gorm:"column:c_id"`            //
	IsUsed      string    `gorm:"column:is_used"`         // 是否启用 Y:是  N:否
	IsDeleted   string    `gorm:"column:is_deleted"`      // 是否删除 1未删 -1已删
	CreatedAt   time.Time `gorm:"time;column:created_at"` // 创建时间
	CreatedUser string    `gorm:"column:created_user"`    // 创建人
	UpdatedAt   time.Time `gorm:"time;column:updated_at"` // 更新时间
	UpdatedUser string    `gorm:"column:updated_user"`    // 更新人

	UserName  string `gorm:"column:user_name"`   // 用户名
	Password  string `gorm:"column:password"`    // 密码
	Phone     string `gorm:"column:phone"`       // 手机号
	NickName  string `gorm:"column:nick_name"`   // 真实姓名
	SysDeptId int64  `gorm:"column:sys_dept_id"` // 真实姓名
}
