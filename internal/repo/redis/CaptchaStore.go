package redis

import (
	"gitee.com/sky-zhou/fangfang-api/config"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"time"
)

func NewCaptchaRedisStore(cache Repo, ctx core.Context) *RedisStore {
	return &RedisStore{
		Expiration: time.Second * 180,
		PreKey:     config.RedisCaptchaKey,
		Cache:      cache,
		Context:    ctx,
	}
}

type RedisStore struct {
	Expiration time.Duration
	PreKey     string
	Context    core.Context
	Cache      Repo
}

func (rs *RedisStore) Set(id string, value string) error {
	err := rs.Cache.Set(rs.PreKey+id, value, rs.Expiration, WithTrace(rs.Context.Trace()))
	return err
}

func (rs *RedisStore) Get(key string, clear bool) string {

	return ""
}

func (rs *RedisStore) Verify(id, answer string, clear bool) bool {
	key := rs.PreKey + id
	v := rs.Get(key, clear)
	return v == answer
}
