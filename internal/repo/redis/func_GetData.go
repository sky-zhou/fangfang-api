package redis

import (
	"encoding/json"
	"gitee.com/sky-zhou/fangfang-api/config"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/model"
	"strconv"
)

func (s *cacheRepo) GetTable(table string, options ...Option) (*model.Table, error) {
	var key = config.RedisKeySysTable + table
	data, err := s.Get(key, options...)
	if err != nil {
		return nil, err
	}
	var t model.Table
	err = json.Unmarshal([]byte(data), &t)
	if err != nil {
		return nil, err
	}
	return &t, nil
}

func (s *cacheRepo) GetTableById(table int, options ...Option) (*model.Table, error) {

	var key = config.RedisKeySysTable + strconv.Itoa(table)
	data, err := s.Get(key, options...)
	if err != nil {
		return nil, err
	}
	var t model.Table
	err = json.Unmarshal([]byte(data), &t)
	if err != nil {
		return nil, err
	}
	return &t, nil
}

func (s *cacheRepo) GetColumn(colId int, options ...Option) (*model.Column, error) {
	var key = config.RedisKeySysColumn + strconv.Itoa(colId)
	data, err := s.Get(key, options...)
	if err != nil {
		return nil, err
	}
	var t model.Column
	err = json.Unmarshal([]byte(data), &t)
	if err != nil {
		return nil, err
	}
	return &t, nil
}

func (s *cacheRepo) GetDict(dict string, options ...Option) (*model.Dict, error) {
	var key = config.RedisKeySysDict + dict
	data, err := s.Get(key, options...)
	if err != nil {
		return nil, err
	}
	var t model.Dict
	err = json.Unmarshal([]byte(data), &t)
	if err != nil {
		return nil, err
	}
	return &t, nil
}

func (s *cacheRepo) GerUserMenu(userId int64, options ...Option) (*[]model.Menu, error) {
	var menu = new([]model.Menu)
	var key = config.RedisKeyPrefixUserMenu + strconv.FormatInt(userId, 10)

	//获取缓存
	CacheMenu, err := s.Get(key, options...)
	if err != nil {
		return nil, err
	}
	if err := json.Unmarshal([]byte(CacheMenu), &menu); err != nil {
		return nil, err
	}
	return menu, nil
}
