package router

import (
	"gitee.com/sky-zhou/fangfang-api/internal/metrics"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/cron"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/redis"
	"gitee.com/sky-zhou/fangfang-api/internal/router/interceptor"
	"gitee.com/sky-zhou/fangfang-api/pkg/errors"

	"go.uber.org/zap"
)

type Server struct {
	Mux        core.Mux
	Db         mysql.Repo
	Cache      redis.Repo
	CronServer cron.Server
}
type resource struct {
	mux          core.Mux
	logger       *zap.Logger
	db           mysql.Repo
	cache        redis.Repo
	interceptors interceptor.Interceptor
	cronServer   cron.Server
}

func NewHTTPServer(logger *zap.Logger, cronLogger *zap.Logger) (*Server, error) {
	if logger == nil {
		return nil, errors.New("logger required")
	}

	r := new(resource)
	r.logger = logger

	// 初始化 DB
	dbRepo, err := mysql.New()
	if err != nil {
		logger.Fatal("new db err", zap.Error(err))
	}
	r.db = dbRepo

	// 初始化 Cache
	cacheRepo, err := redis.New()
	if err != nil {
		logger.Fatal("new cache err", zap.Error(err))
	}
	r.cache = cacheRepo

	// 初始化 CRON Server
	cronServer, err := cron.New(cronLogger, dbRepo, cacheRepo)
	if err != nil {
		logger.Fatal("new cron err", zap.Error(err))
	}
	cronServer.Start()
	r.cronServer = cronServer

	mux, err := core.New(logger,
		core.WithEnableCors(),
		core.WithEnableRate(),
		core.WithRecordMetrics(metrics.RecordHandler(logger)),
	)

	if err != nil {
		panic(err)
	}

	r.mux = mux
	r.interceptors = interceptor.New(logger, r.cache, r.db)

	// 设置 API 路由
	setApiRouter(r)

	// 设置 Socket 路由
	setSocketRouter(r)

	s := new(Server)
	s.Mux = mux
	s.Db = r.db
	s.Cache = r.cache
	s.CronServer = r.cronServer

	return s, nil

}
