package router

import (
	"gitee.com/sky-zhou/fangfang-api/internal/api/helper"
	"gitee.com/sky-zhou/fangfang-api/internal/api/system"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
)

func setApiRouter(r *resource) {
	// helper
	helperHandler := helper.New(r.logger, r.db, r.cache)

	helpers := r.mux.Group("/helper")
	{
		helpers.GET("/md5/:str", helperHandler.Md5())
		helpers.GET("/GetTableId/:str", helperHandler.GetTableId())
		helpers.POST("/sign", helperHandler.Sign())
	}

	//system Handler
	systemHandler := system.New(r.logger, r.db, r.cache)
	systems := r.mux.Group("/api/system", r.interceptors.CheckSignature())
	{
		systems.POST("/Login", systemHandler.Login())
		systems.POST("/GetCaptcha", systemHandler.GetCaptcha())
		//原因是第一次使用是缓存为空
		systems.POST("/ReloadRedisTable", systemHandler.ReloadRedisTable())
	}

	//校验用户权限
	systemUserHandler := system.New(r.logger, r.db, r.cache)
	systemUsers := r.mux.Group("/api/system", core.WrapAuthHandler(r.interceptors.CheckLogin), r.interceptors.CheckSignature())
	{
		systemUsers.POST("/CreateApi", systemUserHandler.CreateObject())
		systemUsers.POST("/UpdateApi", systemUserHandler.UpdateObject())
		systemUsers.POST("/DeleteApi", systemUserHandler.DeleteObject())
		systemUsers.POST("/QueryList", systemUserHandler.QueryList())
		systemUsers.POST("/GetObject", systemUserHandler.GetObject())
		systemUsers.POST("/ReloadDbByTable", systemUserHandler.ReloadDbByTable())
	}
}
