package sys_authorized

import (
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/authorized"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/query"
	"gitee.com/sky-zhou/fangfang-api/pkg/utils"
)

func (s *service) Detail(ctx core.Context, id int32) (info *authorized.Authorized, err error) {
	qb := query.NewQueryBuilder("SYS_AUTHORIZED", s.cache, ctx)
	if qb == nil {
		return nil, err
	}
	qb.Where("ID", id, mysql.EqualPredicate)

	obj, err := qb.GetObject(s.db.GetDbR().WithContext(ctx.RequestContext()))
	if err != nil {
		return nil, err
	}
	if err := utils.CopeyMapToStruct(true, obj, info); err != nil {
		return nil, err
	}
	return
}
