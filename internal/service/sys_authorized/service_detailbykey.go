package sys_authorized

import (
	"encoding/json"
	"gitee.com/sky-zhou/fangfang-api/config"
	"gitee.com/sky-zhou/fangfang-api/internal/model/consts"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/authorized"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/authorized_api"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/query"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/redis"
	"gitee.com/sky-zhou/fangfang-api/pkg/utils"
)

// CacheAuthorizedData 缓存结构
type CacheAuthorizedData struct {
	Key    string         `json:"key"`     // 调用方 key
	Secret string         `json:"secret"`  // 调用方 secret
	IsUsed string         `json:"is_used"` // 调用方启用状态 1=启用 -1=禁用
	Apis   []cacheApiData `json:"apis"`    // 调用方授权的 Apis
}

type cacheApiData struct {
	Method string `json:"method"` // 请求方式
	Api    string `json:"api"`    // 请求地址
}

func (s *service) DetailByKey(ctx core.Context, key string) (cacheData *CacheAuthorizedData, err error) {
	// 查询缓存
	cacheKey := config.RedisKeyPrefixSignature + key

	if !s.cache.Exists(cacheKey) {
		// 查询调用方信息
		data, err := query.NewQueryBuilder("SYS_AUTHORIZED", s.cache, ctx).
			Where("IS_USED", consts.IsUsedY, mysql.EqualPredicate).
			Where("BUSINESS_KEY", key, mysql.EqualPredicate).
			GetObject(s.db.GetDbR().WithContext(ctx.RequestContext()))

		if err != nil {
			return nil, err
		}
		var authorizedInfo = new(authorized.Authorized)
		if err := utils.CopeyMapToStruct(true, data, authorizedInfo); err != nil {
			return nil, err
		}

		var authorizedApiInfo interface{}
		dataApi, err := query.NewQueryBuilder("SYS_AUTHORIZED_API", s.cache, ctx).
			Where("BUSINESS_KEY", key, mysql.EqualPredicate).
			OrderBy("ID", false).
			GetList(s.db.GetDbR().WithContext(ctx.RequestContext()))
		if err != nil {
			return nil, err
		}
		err = utils.CopeyListMapToStruct(true, dataApi, &authorizedApiInfo)
		if err != nil {
			return nil, err
		}

		// 设置缓存 data
		cacheData = new(CacheAuthorizedData)
		cacheData.Key = key
		cacheData.Secret = authorizedInfo.BusinessSecret
		cacheData.IsUsed = authorizedInfo.IsUsed
		cacheData.Apis = make([]cacheApiData, len(authorizedApiInfo.([]interface{})))

		for k, v := range authorizedApiInfo.([]interface{}) {
			val := new(authorized_api.AuthorizedApi)
			utils.CopeyMapToStruct(false, v, &val)
			data := cacheApiData{
				Method: val.Method,
				Api:    val.Api,
			}
			cacheData.Apis[k] = data
		}

		cacheDataByte, _ := json.Marshal(cacheData)

		err = s.cache.Set(cacheKey, string(cacheDataByte), config.LoginSessionTTL, redis.WithTrace(ctx.Trace()))
		if err != nil {
			return nil, err
		}

		return cacheData, nil
	}

	value, err := s.cache.Get(cacheKey, redis.WithTrace(ctx.RequestContext().Trace))
	if err != nil {
		return nil, err
	}

	cacheData = new(CacheAuthorizedData)
	err = json.Unmarshal([]byte(value), cacheData)
	if err != nil {
		return nil, err
	}

	return

}
