package sys_prem

import (
	"fmt"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/model"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/query"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/redis"
	"strings"
)

//根据ctx中的用户、表单名称、操作，进行权限校验
func (s *service) CheckPrem(ctx core.Context, table model.Table, act int64, action string) bool {
	//获取用户菜单
	menus, err := s.cache.GerUserMenu(ctx.SessionUserInfo().UserID, redis.WithTrace(ctx.Trace()))
	if err != nil {
		fmt.Println(err)
		return false
	}
	//校验权限
	for _, v := range *menus {
		if v.TableName == table.Name {
			if v.Permission >= act && strings.Contains(v.TableMask, action) {
				return true
			}
		}
	}
	return false
}

//根据ctx中的用户、表单名称、操作，进行权限校验
func (s *service) CheckPremData(ctx core.Context, table model.Table, act int64, action string, rowId int64) bool {
	//获取用户菜单
	var hasPerm = false
	var menu model.Menu
	menus, err := s.cache.GerUserMenu(ctx.SessionUserInfo().UserID, redis.WithTrace(ctx.Trace()))
	if err != nil {
		fmt.Println(err)
		return false
	}
	//校验功能权限
	for _, v := range *menus {
		if v.TableName == table.Name {
			if v.Permission >= act && strings.Contains(v.TableMask, action) {
				hasPerm = true
				menu = v
			}
		}
	}
	if hasPerm {
		if menu.DataPrem.Filter.Condition != "" {
			//构建查询结构
			qb := query.NewQueryBuilder(menu.TableName, s.cache, ctx)
			val := query.PreaseUserEnv(menu.DataPrem.Filter.Condition, ctx.SessionUserInfo())
			qb.Where(menu.DataPrem.Filter.Clink, val, mysql.Predicate(menu.DataPrem.Filter.Pdc))
			qb.Where("ID", rowId, mysql.EqualPredicate)

			//查询
			count, err := qb.Count(s.db.GetDbR())
			if err != nil || count <= 0 {
				hasPerm = false
			}
		}
	}

	return hasPerm
}
