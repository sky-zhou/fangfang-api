package sys_prem

import (
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/model"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/redis"
)

var _ Service = (*service)(nil)

type Service interface {
	i()
	CheckPrem(ctx core.Context, table model.Table, act int64, action string) bool
	CheckPremData(ctx core.Context, table model.Table, act int64, action string, rowId int64) bool
}

type service struct {
	cache redis.Repo
	db    mysql.Repo
}

func New(cache redis.Repo, db mysql.Repo) Service {
	return &service{
		cache: cache,
		db:    db,
	}
}

func (s *service) i() {}
