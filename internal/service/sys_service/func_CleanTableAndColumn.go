package sys_service

import (
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/sys_column"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/sys_table"

	"gorm.io/gorm"
)

func (s *service) CleanTableAndColumn(tablename string, tx *gorm.DB) (err error) {
	//1.查询配置表单中该表单的id
	//2.根据 sys_table_id 删除配置字段
	//3.删除配置表单中的行记录
	var sysTable sys_table.SysTable
	err = tx.Table("SYS_TABLE").Where(" name = ? ", tablename).Scan(&sysTable).Error
	if err != nil {
		return err
	}
	if sysTable.Id == 0 {
		return nil
	}

	//删除配置字段
	err = tx.Table("SYS_COLUMN").Where(" SYS_TABLE_ID = ? ", sysTable.Id).Delete(&sys_column.SysColumn{}).Error
	if err != nil {
		return err
	}

	//TODO:后期增加其他表单引用问题

	//删除配置表单信息
	err = tx.Table("SYS_TABLE").Where(" ID = ? ", sysTable.Id).Delete(&sysTable).Error
	return err
}
