package sys_service

import (
	"gitee.com/sky-zhou/fangfang-api/internal/model/consts"
	"gitee.com/sky-zhou/fangfang-api/internal/model/req"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/model"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/query"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/redis"
	"gitee.com/sky-zhou/fangfang-api/pkg/builder"
	"gitee.com/sky-zhou/fangfang-api/pkg/errors"
	"gorm.io/gorm"
)

//标准新增服务
//获取缓存中的表单配置
//构建临时结构体
//实例化结构体并赋值
//执行新增
//返回所新增的主记录ID
func (s *service) Create(ctx core.Context, reqData *req.CreateRequest) (id int64, err error) {
	s.ctx = ctx
	//从缓存中获取表单配置信息
	table, e := s.cache.GetTable(reqData.Table, redis.WithTrace(s.ctx.Trace()))
	if e != nil {
		return 0, e
	}
	//校验用户表单权限
	if !s.premService.CheckPrem(ctx, *table, consts.Write, consts.C) {
		return 0, errors.New("无权限")
	}
	st := table.CreateStruct().New()
	//设置默认值
	//SetDefault(st, table)
	//获取mdata中的数据
	Mdata := reqData.Mdata
	s.SetColumnValue(st, table, Mdata)

	//给主表设置ID
	id, ex := s.GetTableNewId(s.ctx, table.Name)
	if ex != nil {
		return 0, ex
	}
	st.SetInt64("ID", id)

	// 开始事务
	tx := s.db.GetDbW().Begin()

	err = tx.Table(reqData.Table).Create(st.Addr()).Error

	if err != nil {
		return 0, err
	}
	value, err := st.Field("ID")

	if err != nil || !value.CanInt() {
		//回滚操作
		tx.Rollback()
		return 0, err
	}

	//获取itemData数据
	err = s.CreateItem(reqData.Idata, table.Name, value.Int(), tx)

	//提交数据库
	tx.Commit()
	return value.Int(), nil
}

//根据字段情况设置默认值
func (s *service) SetColumnValue(row *builder.Instance, table *model.Table, mdata map[string]interface{}) {
	for _, v := range table.Cols {
		query.GetColumnValue(row, &v, mdata[v.Dbname], s.ctx.SessionUserInfo())
	}
}

//创建明细数据
func (s *service) CreateItem(items []map[string]interface{}, parent string, parentId int64, tx *gorm.DB) error {
	for _, v := range items {
		for k := range v {
			//从缓存中获取表单配置信息
			table, e := s.cache.GetTable(k, redis.WithTrace(s.ctx.Trace()))
			if e != nil {
				return e
			}
			//生成明细结构体行记录
			st := table.CreateStruct().New()
			//给明细行记录赋值数据
			s.SetColumnValue(st, table, v[k].(map[string]interface{}))
			//设置主记录id
			st.SetInt64(parent+"_ID", parentId)
			//获取明细记录id
			id, ex := s.GetTableNewId(s.ctx, k)
			if ex != nil {
				return ex
			}
			//给明细行设置ID
			st.SetInt64("ID", id)

			err := tx.Table(k).Create(st.Addr()).Error
			if err != nil {
				return err
			}
		}
	}
	return nil
}
