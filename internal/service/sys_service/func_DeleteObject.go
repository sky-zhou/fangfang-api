package sys_service

import (
	"errors"
	"gitee.com/sky-zhou/fangfang-api/internal/model/consts"
	"gitee.com/sky-zhou/fangfang-api/internal/model/res"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/model"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/query"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/redis"
	"gitee.com/sky-zhou/fangfang-api/pkg/builder"
	"strconv"
)

func (s *service) DeleteObject(ctx core.Context, Table string, ids []int64) *res.DeleteRes {
	s.ctx = ctx
	var data = new(res.DeleteRes)
	if len(ids) == 0 {
		data.ErrMessage = append(data.ErrMessage, errors.New("传入数据异常").Error())
		return data
	}

	//从缓存中获取表单配置信息
	table, e := s.cache.GetTable(Table, redis.WithTrace(s.ctx.Trace()))
	if e != nil {
		data.ErrMessage = append(data.ErrMessage, e.Error())
		return data
	}

	for _, v := range ids {
		//增加权限校验
		s.premService.CheckPremData(ctx, *table, consts.Write, consts.D, v)
		// 开始事务
		tx := s.db.GetDbW().Begin()

		dl := table.CreateStructForDelete().New()
		s.SetDeleteValue(dl, table)

		count := tx.Table(table.Name).Where(" ID = ? ", v).Updates(dl.Addr()).RowsAffected
		if count > 0 {
			data.SCount = data.SCount + 1
			tx.Commit()
		} else {
			if tx.Error != nil {
				data.ErrMessage = append(data.ErrMessage, tx.Error.Error())
			} else {
				data.ErrMessage = append(data.ErrMessage, errors.New("数据"+strconv.FormatInt(v, 10)+"未找到").Error())
			}
			data.ECount = data.ECount + 1
			tx.Rollback()
		}
		//提交数据库
		//tx.Commit()
	}

	return data
}

func (s *service) SetDeleteValue(row *builder.Instance, table *model.Table) {
	for _, v := range table.Cols {
		if v.Dbname == "DELETED_AT" {
			query.GetColumnValue(row, &v, nil, s.ctx.SessionUserInfo())
		}
		if v.Dbname == "DELETED_USER" {
			query.GetColumnValue(row, &v, nil, s.ctx.SessionUserInfo())
		}
		if v.Dbname == "IS_USED" {
			query.GetColumnValue(row, &v, "N", s.ctx.SessionUserInfo())
		}
		if v.Dbname == "IS_DELETED" {
			query.GetColumnValue(row, &v, "Y", s.ctx.SessionUserInfo())
		}
	}
}
