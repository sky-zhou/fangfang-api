package sys_service

import (
	"gitee.com/sky-zhou/fangfang-api/internal/model/consts"
	"gitee.com/sky-zhou/fangfang-api/internal/model/req"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/query"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/redis"
)

//列表查询
func (s *service) GetObject(req *req.GetObj, ctx core.Context) (interface{}, error) {
	s.ctx = ctx

	//从缓存中获取表单配置信息
	table, e := s.cache.GetTable(req.Table, redis.WithTrace(s.ctx.Trace()))
	if e != nil {
		return nil, e
	}

	//校验数据权限
	if s.premService.CheckPremData(ctx, *table, consts.Read, consts.R, req.OjbectId) {

	}
	qb := query.NewQueryBuilder(req.Table, s.cache, ctx)
	//设置查询条件
	qb.Where(table.Name+".ID", req.OjbectId, mysql.EqualPredicate)

	var data map[string]interface{}
	var err error
	if req.IsNeedFk {
		data, err = qb.GetObjectR(s.db.GetDbR())
	} else {
		data, err = qb.GetObjectR(s.db.GetDbR())
	}
	if err != nil {
		return nil, err
	}

	return data, err
}
