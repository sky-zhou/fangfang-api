package sys_service

import (
	"gitee.com/sky-zhou/fangfang-api/config"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/redis"
	"strconv"
)

//根据表单名称获取该表单下一个id(获取后不论是否使用，下次获取都是最新的)
func (s *service) GetTableNewId(c core.Context, tableName string) (int64, error) {
	var id int64
	//尝试从缓存中获取key
	str, err := s.cache.Get(config.RedisKeySysData + tableName)
	if err != nil || len(str) == 0 {
		//如果缓存中没有，则从数据库中获取最大id值
		err := s.db.GetDbR().Table(tableName).Select("IFNULL(max(ID),0)").Order("ID desc").Scan(&id).Error
		if err != nil {
			return 0, err
		}
		//拿到最大id值，重新设置给缓存
		e := s.cache.Set(config.RedisKeySysData+tableName, strconv.FormatInt(id, 10), -1, redis.WithTrace(c.Trace()))
		if e != nil {
			return 0, e
		}
	}
	//使用redis Incr获取最大id(经过测试，大量并发无忧)
	id = s.cache.IncrByProject(tableName, redis.WithTrace(c.Trace()))
	return id, nil
}
