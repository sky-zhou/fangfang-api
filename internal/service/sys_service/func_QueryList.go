package sys_service

import (
	"gitee.com/sky-zhou/fangfang-api/internal/model/req"
	"gitee.com/sky-zhou/fangfang-api/internal/model/res"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/query"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/redis"
)

//{
//    "table": "SYS_COLUMN",
//    "fixColumn": [
//        {
//            "col": "IS_USED",
//            "key": "Y",
//            "pdc": "="
//        },
//        {
//            "col": "C_ID",
//            "key": "1",
//            "pdc": "="
//        }
//    ],
//    "order": [{
//        "col":"ID",
//        "asc":false
//    }],
//    "isNeedFk": true,
//    "page": 1,
//    "row": 10,
//    "isNeedTot":true
//}

//列表查询
func (s *service) QueryList(req *req.Query, ctx core.Context) (interface{}, error) {
	s.ctx = ctx
	qb := query.NewQueryBuilder(req.Table, s.cache, ctx)
	//从缓存中获取表单配置信息
	table, e := s.cache.GetTable(req.Table, redis.WithTrace(s.ctx.Trace()))
	if e != nil {
		return 0, e
	}

	//TODO:后期增加多个值问题处理
	//根据查询条件进行构造查询语句
	for _, r := range req.FixColum {
		for _, v := range table.Cols {
			if v.Dbname == r.Col {
				if len(r.Pdc) > 0 {
					qb.Where(v.FullDbname, r.Key, r.Pdc)
				} else {
					pdc := query.GetPdc(v)
					qb.Where(v.FullDbname, r.Key, pdc)
				}
			}
		}
	}
	//分页查找
	if req.Row == 0 {
		qb.Limit(10)
	} else {
		qb.Limit(req.Row)
	}
	if req.Page == 0 {
		qb.Offset(1)
	} else {
		qb.Offset((req.Page - 1) * req.Row)
	}
	//如果存在排序则进行排序
	if len(req.Order) > 0 {
		for _, v := range req.Order {
			qb.OrderBy(table.Name+"."+v.Col, v.Asc)
		}
	}

	//根据判断是否需要返回显示键
	var data res.QueryRes
	var err error
	if req.IsNeedFk {
		data.Rows, err = qb.GetListR(s.db.GetDbR())
	} else {
		data.Rows, err = qb.GetList(s.db.GetDbR())
	}
	//判断是否需要返回合计数据
	if req.IsNeedTot {
		data.TotRow, err = qb.Count(s.db.GetDbR())
	}
	return data, err
}
