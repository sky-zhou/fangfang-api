package sys_service

import (
	"gitee.com/sky-zhou/fangfang-api/config"
	"gitee.com/sky-zhou/fangfang-api/internal/model/consts"
	"gitee.com/sky-zhou/fangfang-api/internal/model/req"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/sys_column"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/sys_table"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/redis"
	"gitee.com/sky-zhou/fangfang-api/pkg/utils"

	"gorm.io/gorm"
)

type Table struct {
	TableName    string
	TableComment string
}

type Column struct {
	ColumnName    string
	TableName     string
	DataType      string
	DataTypeLong  int
	ColumnComment string
	Orderno       int
	NumericScale  int
	DefaultValue  string
	Nullable      string
}

func (s *service) ReloadMysqlTable(ctx core.Context, reqData *req.ReloadTableRequest) (err error) {
	tables, e := s.GetTables(reqData.Tables)
	if e != nil {
		return e
	}
	//1.根据表单获取字段
	//2.删除对应的表单及字段
	//3.插入表单及字段
	for k := range tables {
		//判断该表单是否系统表单
		if s.CheckTableIsSys(tables[k].TableName) {
			continue
		}
		var cols []Column
		//获取表单字段
		cols, e = s.GetColumnByTable(tables[k].TableName)
		if e != nil {
			return e
		}

		// 开始事务
		tx := s.db.GetDbW().Begin()

		//新增表单
		//defer func() { tx.Rollback() }()

		sysTable, ex := s.CreateSysTable(tables[k], tx, ctx)
		if ex != nil {
			//回滚操作
			tx.Rollback()
			return ex
		}
		//获取数据库字段配置并生成字段配置数据
		for i := range cols {
			_, er := s.CreateSysColum(sysTable.Id, cols[i], tx, ctx)
			if er != nil {
				tx.Rollback()
				return er
			}
		}
		// 否则，提交事务
		tx.Commit()

	}
	return nil
}

//根据传入的表单名称，获取需要重载的表单
func (s *service) GetTables(tableList []string) (tables []Table, err error) {
	sql := `select UPPER(t.table_name) as TableName,t.TABLE_COMMENT TableComment  from information_schema.tables t where table_schema= ? `
	if len(tableList) > 0 {
		sql = sql + " and table_name in (" + utils.ListStringToString(tableList) + ") "
	}
	sql = sql + "order by TABLE_COMMENT"
	err = s.db.GetDbR().Raw(sql, config.Get().Mysql.Read.Name).Scan(&tables).Error
	return tables, err
}

//根据传入的表单名称，获取需要重载的表单
func (s *service) GetColumnByTable(tableName string) (columns []Column, err error) {

	//组建获取表单字段查询语句
	sql := `SELECT UPPER(COLUMN_NAME)        ColumnName,
       DATA_TYPE          DataType,
       UPPER(table_name)		  TableName,
       CASE DATA_TYPE
           WHEN 'longtext' THEN c.CHARACTER_MAXIMUM_LENGTH
           WHEN 'varchar' THEN c.CHARACTER_MAXIMUM_LENGTH
           WHEN 'double' THEN c.NUMERIC_PRECISION
           WHEN 'decimal' THEN  c.NUMERIC_PRECISION
           WHEN 'int' THEN c.NUMERIC_PRECISION
           WHEN 'bigint' THEN c.NUMERIC_PRECISION
					 WHEN 'tinyint' THEN c.NUMERIC_PRECISION
           WHEN 'char' THEN c.CHARACTER_MAXIMUM_LENGTH
           ELSE null END AS DataTypeLong,
		COLUMN_COMMENT     ColumnComment,
			 ordinal_position*10 Orderno,
			 case numeric_scale
				when 0 then NULL
				when null then NULL
				else numeric_scale end as NumericScale,
		c.COLUMN_DEFAULT DefaultValue,
			case IS_NULLABLE
			when 'NO' then 'N'
			when 'YES' then 'Y'
		end as Nullable
	FROM INFORMATION_SCHEMA.COLUMNS c
	WHERE table_name = ?
	  AND table_schema = ?`

	err = s.db.GetDbR().Raw(sql, tableName, config.Get().Mysql.Read.Name).Scan(&columns).Error
	return columns, err
}

func (s *service) CreateSysTable(table Table, tx *gorm.DB, ctx core.Context) (systable *sys_table.SysTable, err error) {
	//0.创建前，先删除历史创建的记录
	//1.构建sys_table 结构体
	//2.写入数据
	err = s.CleanTableAndColumn(table.TableName, tx)
	if err != nil {
		return nil, err
	}
	var tableObj sys_table.SysTable
	tableObj.Id, err = s.GetTableNewId(ctx, "SYS_TABLE")
	if err != nil {
		return systable, err
	}
	tableObj.Name = table.TableName
	tableObj.Disname = table.TableComment
	tableObj.Orderno = int(s.cache.IncrByProject("SYS_TABLE_ORDERNO", redis.WithTrace(ctx.Trace()))) * 10
	tableObj.IsUsed = consts.IsUsedY
	tableObj.IsDeleted = consts.IsDeleteN
	tableObj.UpdatedUser = "admin"
	tableObj.CreatedUser = "admin"
	tableObj.IsBig = consts.IsBigN
	tableObj.IsMenu = consts.IsMenuNo
	//默认情况cid未1
	tableObj.CId = 1
	//默认情况支持增删改查
	tableObj.TableMask = "CRUD"
	err = tx.Create(&tableObj).Error
	return &tableObj, err
}

func (s *service) CreateSysColum(tableId int64, col Column, tx *gorm.DB, ctx core.Context) (syscolumn *sys_column.SysColumn, err error) {
	var colObject sys_column.SysColumn
	colObject.Id, err = s.GetTableNewId(ctx, "SYS_COLUMN")
	if err != nil {
		return
	}
	colObject.Dbname = col.ColumnName
	colObject.Comment = col.ColumnComment
	colObject.FullDbname = col.TableName + "." + col.ColumnName
	colObject.Coltype = col.DataType
	colObject.Lenth = col.DataTypeLong
	colObject.Orderno = col.Orderno
	colObject.SysTableId = tableId
	colObject.Precision = col.NumericScale
	colObject.IsUsed = consts.IsUsedY
	colObject.IsDeleted = consts.IsDeleteN
	colObject.CreatedUser = "admin"
	colObject.UpdatedUser = "admin"
	colObject.DefaultValue = SetDefaultValueBycol(col.ColumnName, col.DefaultValue)
	colObject.IsAk = consts.IsNo
	colObject.IsDk = consts.IsNo
	colObject.IsPk = consts.IsNo
	colObject.IsNull = col.Nullable
	colObject.IsUpper = consts.IsNo
	colObject.CId = 1
	colObject.FkColumnId = setCidFkId(col.ColumnName)
	colObject.SysDictName = setDictName(col.ColumnName)
	//默认所有字段在所有接口中可见
	colObject.Mask = "11111111"
	err = tx.Create(&colObject).Error
	return &colObject, err
}

func setCidFkId(colName string) int64 {
	if colName == "C_ID" {
		return 59
	}
	return 0
}

func setDictName(colName string) string {
	switch colName {
	case "IS_USED":
		return "sys_is_used"
	case "IS_DELETED":
		return "sys_is_deleted"
	}
	return ""
}

func SetDefaultValueBycol(colName string, defaultVal string) string {
	if len(defaultVal) > 0 {
		return defaultVal
	}
	switch colName {
	case "C_ID":
		return "@UserCid@"
	case "CREATED_USER":
		return "@UserName@"
	case "UPDATED_USER":
		return "@UserName@"
	case "DELETED_USER":
		return "@UserName@"
	case "DELETED_AT":
		return "CURRENT_TIMESTAMP"
	case "CREATED_AT":
		return "CURRENT_TIMESTAMP"
	case "UPDATED_AT":
		return "CURRENT_TIMESTAMP"
	case "IS_USED":
		return "Y"
	case "IS_DELETED":
		return "N"
	}
	return ""
}

func (s *service) CheckTableIsSys(tablename string) bool {
	var tableList = []string{"SYS_TABLE", "SYS_COLUMN", "SYS_COMPANY",
		"SYS_CRON_TASK", "SYS_AUTHORIZED_API", "SYS_AUTHORIZED", "SYS_DICT",
		"SYS_DICTITEM", "SYS_MODEL", "SYS_USER"}
	for _, key := range tableList {
		if tablename == key {
			return true
		}
	}
	return false
}
