package sys_service

import (
	"encoding/json"
	"gitee.com/sky-zhou/fangfang-api/config"
	"gitee.com/sky-zhou/fangfang-api/internal/model/req"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/model"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/redis"
	"strconv"
)

func (s *service) ReloadRedisTable(ctx core.Context, reqData *req.ReloadTableRequest) (err error) {
	tables, e := s.GetTables(reqData.Tables)
	if e != nil {
		return e
	}
	//1.根据表单获取字段
	//2.删除缓存中的对应的表单及字段
	//3.插入表单及字段
	for k := range tables {
		var table model.Table
		//获取配置表单信息
		err = s.db.GetDbR().Table("SYS_TABLE").Where("NAME = ?  AND IS_USED='Y' ", tables[k].TableName).Scan(&table).Error
		if err != nil || table.Id == 0 {
			return err
		}

		var cols []model.Column
		//获取表单配置字段信息
		err = s.db.GetDbR().Table("SYS_COLUMN").Where("SYS_TABLE_ID = ?  AND IS_USED='Y' ", table.Id).Scan(&cols).Error
		if err != nil {
			return err
		}
		//循环配置字段获取数据字典
		for _, v := range cols {
			if len(v.SysDictName) > 0 {
				var dict model.Dict
				err = s.db.GetDbR().Table("SYS_DICT").Where("DICT_NAME = ? AND IS_USED='Y' ", v.SysDictName).Scan(&dict).Error
				if err != nil {
					return err
				}
				if dict.Id > 0 {
					var dictItem []model.Dictitem
					err = s.db.GetDbR().Table("SYS_DICTITEM").Where("SYS_DICT_ID = ? AND IS_USED='Y' ", dict.Id).Scan(&dictItem).Error
					if err != nil {
						return err
					}
					dict.Dictitem = dictItem
					dict.SetDefault()
				}
				v.SysDict = dict
				strDict, _ := json.Marshal(dict)
				s.cache.Set(config.RedisKeySysDict+dict.DictName, string(strDict), 0, redis.WithTrace(ctx.Trace()))
			}
			v.SetDefault()
			strCol, _ := json.Marshal(v)
			s.cache.Set(config.RedisKeySysColumn+strconv.Itoa(v.Id), string(strCol), 0, redis.WithTrace(ctx.Trace()))
		}
		table.Cols = cols
		strTable, _ := json.Marshal(table)
		err = s.cache.Set(config.RedisKeySysTable+table.Name, string(strTable), 0, redis.WithTrace(ctx.Trace()))
		err = s.cache.Set(config.RedisKeySysTable+strconv.Itoa(table.Id), string(strTable), 0, redis.WithTrace(ctx.Trace()))
		if err != nil {
			return err
		}
	}

	return nil
}
