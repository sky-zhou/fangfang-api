package sys_service

import (
	"fmt"
	"gitee.com/sky-zhou/fangfang-api/internal/model/consts"
	"gitee.com/sky-zhou/fangfang-api/internal/model/req"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/model"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/query"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/redis"
	"gitee.com/sky-zhou/fangfang-api/pkg/builder"
	"gitee.com/sky-zhou/fangfang-api/pkg/errors"
	"gitee.com/sky-zhou/fangfang-api/pkg/utils"
	"gorm.io/gorm"
	"strconv"
)

func (s service) Update(ctx core.Context, req *req.UpdateRequest) error {
	s.ctx = ctx
	//从缓存中获取表单配置信息
	table, e := s.cache.GetTable(req.Table, redis.WithTrace(s.ctx.Trace()))
	if e != nil {
		return e
	}

	var mId int64 = 0
	if utils.CheckKeyByMap(req.Mdata, "ID") {
		mId, _ = strconv.ParseInt(fmt.Sprintf("%1.0f", req.Mdata["ID"].(float64)), 10, 64)
	} else {
		return errors.New("no data found")
	}

	if mId == 0 {
		return errors.New("no data found")
	}

	//构建主表更新对象
	ur := table.CreateStructForUpdate(req.Mdata).New()

	//校验用户表单权限
	if !s.premService.CheckPremData(ctx, *table, consts.Write, consts.U, mId) {
		return errors.New("无权限")
	}
	//设置updateRow
	s.SetUpdateValue(ur, table, req.Mdata)

	// 开始事务
	tx := s.db.GetDbW().Begin()

	err := tx.Table(req.Table).Updates(ur.Addr()).Error
	if err != nil {
		tx.Rollback()
		return err
	}
	//获取itemData数据
	err = s.UpdateItem(req.Idata, table.Name, mId, tx)
	if err != nil {
		tx.Rollback()
		return err
	}
	//提交数据库
	tx.Commit()
	return nil
}
func (s *service) SetUpdateValue(row *builder.Instance, table *model.Table, mdata map[string]interface{}) {
	for _, v := range table.Cols {
		for k := range mdata {
			if v.Dbname == k {
				query.GetColumnValue(row, &v, mdata[v.Dbname], s.ctx.SessionUserInfo())
			}
		}
		if v.Dbname == "UPDATED_AT" {
			query.GetColumnValue(row, &v, nil, s.ctx.SessionUserInfo())
		}
		if v.Dbname == "UPDATED_USER" {
			query.GetColumnValue(row, &v, nil, s.ctx.SessionUserInfo())
		}
	}
}

//创建明细数据
func (s *service) UpdateItem(items []map[string]interface{}, parent string, parentId int64, tx *gorm.DB) error {
	for _, v := range items {
		for k := range v {
			//从缓存中获取表单配置信息
			table, e := s.cache.GetTable(k, redis.WithTrace(s.ctx.Trace()))
			if e != nil {
				return e
			}
			//尝试获取明细id
			var itemId int = 0
			if utils.CheckKeyByMap(v[k].(map[string]interface{}), "ID") {
				itemId, _ = strconv.Atoi(fmt.Sprintf("%1.0f", v[k].(map[string]interface{})["ID"].(float64)))
			}

			if itemId == 0 {
				//生成明细结构体行记录
				st := table.CreateStruct().New()
				//给明细行记录赋值数据
				s.SetColumnValue(st, table, v[k].(map[string]interface{}))
				//设置主记录id
				st.SetInt64(parent+"_ID", int64(parentId))
				//获取明细记录id
				id, ex := s.GetTableNewId(s.ctx, k)
				if ex != nil {
					return ex
				}
				//给明细行设置ID
				st.SetInt64("ID", id)

				err := tx.Table(k).Create(st.Addr()).Error
				if err != nil {
					return err
				}
			} else {
				st := table.CreateStructForUpdate(v[k].(map[string]interface{})).New()
				s.SetUpdateValue(st, table, v[k].(map[string]interface{}))

				//更新明细数据
				err := tx.Table(table.Name).Updates(st.Addr()).Error
				if err != nil {
					return err
				}
			}

		}
	}
	return nil
}
