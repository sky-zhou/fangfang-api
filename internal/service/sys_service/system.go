package sys_service

import (
	"gitee.com/sky-zhou/fangfang-api/internal/model/req"
	"gitee.com/sky-zhou/fangfang-api/internal/model/res"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/redis"
	"gitee.com/sky-zhou/fangfang-api/internal/service/sys_prem"
)

var _ Service = (*service)(nil)

type Service interface {
	i()

	//重载数据库配置数据
	ReloadMysqlTable(ctx core.Context, reqData *req.ReloadTableRequest) (err error)
	//重载缓存数据
	ReloadRedisTable(ctx core.Context, reqData *req.ReloadTableRequest) (err error)

	//新增服务
	Create(ctx core.Context, reqData *req.CreateRequest) (id int64, err error)

	//更新服务
	Update(ctx core.Context, req *req.UpdateRequest) error

	//删除服务
	DeleteObject(ctx core.Context, Table string, ids []int64) *res.DeleteRes

	//列表查询接口(带外键)
	QueryList(req *req.Query, ctx core.Context) (interface{}, error)

	//列表查询接口(带外键)
	GetObject(req *req.GetObj, ctx core.Context) (interface{}, error)
}

type service struct {
	db          mysql.Repo
	cache       redis.Repo
	ctx         core.Context
	premService sys_prem.Service
}

func New(db mysql.Repo, cache redis.Repo) Service {
	return &service{
		db:          db,
		cache:       cache,
		premService: sys_prem.New(cache, db),
	}
}

func (s *service) i() {}
