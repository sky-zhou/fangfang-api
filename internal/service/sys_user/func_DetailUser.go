package sys_user

import (
	"crypto/hmac"
	"crypto/md5"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"gitee.com/sky-zhou/fangfang-api/internal/model/req"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/sys_user"
)

const (
	saltPassword = "qkhPAGA13HocW3GAEWwb"
)

//获取用户信息
func (s *service) GetUserInfo(ctx core.Context, data req.LoginReq) (info *sys_user.SysUser, err error) {
	var password = generatePassword(data.Password)
	err = s.db.GetDbR().Table("sys_user").Where("(user_name = ? or phone = ? ) "+
		"and password = ? and is_used='Y' and is_deleted='N' ", data.Username, data.Phone, password).Scan(&info).Error
	return info, err
}

//根据id生成token
func (s *service) GenerateLoginToken(id int64) (token string) {
	m := md5.New()
	m.Write([]byte(fmt.Sprintf("%d%s", id, saltPassword)))
	token = hex.EncodeToString(m.Sum(nil))
	return
}

//传过来的明文密码进行加密后比对
func generatePassword(str string) (password string) {
	// md5
	m := md5.New()
	m.Write([]byte(str))
	mByte := m.Sum(nil)

	// hmac
	h := hmac.New(sha256.New, []byte(saltPassword))
	h.Write(mByte)
	password = hex.EncodeToString(h.Sum(nil))

	return
}
