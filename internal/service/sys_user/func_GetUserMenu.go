package sys_user

import (
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/model"
)

//获取用户菜单项
//先获取权限组明细中的表单及权限
//然后找到父级菜单进行返回
func (s *service) GetUserMenu(userId int64, ctx core.Context) ([]model.Menu, error) {
	var menu []model.Menu

	//根据语句找到用户表单及权限
	err := s.db.GetDbR().Raw(sql_pre, userId).Scan(&menu).Error
	if err != nil {
		return nil, err
	}

	//获取父记录的id
	var pIds []int64
	for _, v := range menu {
		pIds = append(pIds, v.ParentId)
	}

	//根据找到涉及的父记录并push给表单权限并返回
	var parent []model.Menu
	err = s.db.GetDbR().Raw(sql_menu, pIds).Scan(&parent).Error
	if err != nil {
		return nil, err
	}

	for _, v := range parent {
		menu = append(menu, v)
	}

	return menu, nil
}

var sql_pre = `
SELECT
	t.id,
	t.name,
	t.url,
	t.icon,
	t.orderno,
	t.parent_id,
	t.sys_table_id,
	t.type,
	c.permission,
	d.name table_name,
	c.data_prem,
	d.table_mask
FROM
	sys_menu t
LEFT JOIN sys_table d ON d.id = t.sys_table_id,
 (
	SELECT
		a.sys_menu_id,
		a.permission,
		a.data_prem
	FROM
		sys_role a,
		sys_rolegroup_user b
	WHERE
		a.sys_rolegroup_id = b.sys_rolegroup_id
	AND b.sys_user_id = ?
	GROUP BY
		a.sys_menu_id,
		a.permission
) c
WHERE
	t.id = c.sys_menu_id
order by t.orderno asc
`

var sql_menu = `
SELECT
	t.id,
	t.name,
	t.url,
	t.icon,
	t.orderno,
	t.parent_id,
	t.sys_table_id,
	t.type
FROM
	sys_menu t
WHERE
	t.id in (?)
order by t.orderno asc
`
