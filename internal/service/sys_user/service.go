package sys_user

import (
	"gitee.com/sky-zhou/fangfang-api/internal/model/req"
	"gitee.com/sky-zhou/fangfang-api/internal/pkg/core"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/model"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/mysql/sys_user"
	"gitee.com/sky-zhou/fangfang-api/internal/repo/redis"
)

var _ Service = (*service)(nil)

type Service interface {
	i()

	GetUserInfo(ctx core.Context, data req.LoginReq) (info *sys_user.SysUser, err error)

	GenerateLoginToken(id int64) (token string)

	GetUserMenu(userId int64, ctx core.Context) ([]model.Menu, error)
}

type service struct {
	db    mysql.Repo
	cache redis.Repo
}

func New(db mysql.Repo, cache redis.Repo) Service {
	return &service{
		db:    db,
		cache: cache,
	}
}

func (s *service) i() {}
