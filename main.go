package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"gitee.com/sky-zhou/fangfang-api/config"
	"gitee.com/sky-zhou/fangfang-api/internal/router"
	"gitee.com/sky-zhou/fangfang-api/pkg/env"
	"gitee.com/sky-zhou/fangfang-api/pkg/log"
	"gitee.com/sky-zhou/fangfang-api/pkg/shutdown"
	"gitee.com/sky-zhou/fangfang-api/pkg/timeutil"

	"go.uber.org/zap"
)

//go:generate go env -w GO111MODULE=on
//go:generate go env -w GOPROXY=https://goproxy.cn,direct
//go:generate go mod tidy
//go:generate go mod download

func main() {
	// 初始化 access logger
	accessLogger, err := log.NewJSONLogger(
		log.WithDisableConsole(),
		log.WithField("domain", fmt.Sprintf("%s[%s]", config.ProjectName, env.Active().Value())),
		log.WithTimeLayout(timeutil.CSTLayout),
		log.WithFileP(config.ProjectAccessLogFile),
	)
	if err != nil {
		panic(err)
	}

	// 初始化 cron logger
	cronLogger, err := log.NewJSONLogger(
		log.WithDisableConsole(),
		log.WithField("domain", fmt.Sprintf("%s[%s]", config.ProjectName, env.Active().Value())),
		log.WithTimeLayout(timeutil.CSTLayout),
		log.WithFileP(config.ProjectCronLogFile),
	)
	if err != nil {
		panic(err)
	}

	defer func() {
		_ = accessLogger.Sync()
		_ = cronLogger.Sync()
	}()
	// 初始化 HTTP 服务
	s, err := router.NewHTTPServer(accessLogger, cronLogger)
	if err != nil {
		panic(err)
	}

	server := &http.Server{
		Addr:    ":" + config.Get().System.Port,
		Handler: s.Mux,
	}

	go func() {
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			accessLogger.Fatal("http server startup err", zap.Error(err))
		}
	}()

	//优雅关闭
	shutdown.NewHook().Close(
		// 关闭 http server
		func() {
			ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
			defer cancel()

			if err := server.Shutdown(ctx); err != nil {
				accessLogger.Error("server shutdown err", zap.Error(err))
			}
		},

		// 关闭 db
		func() {
			if s.Db != nil {
				if err := s.Db.DbWClose(); err != nil {
					accessLogger.Error("dbw close err", zap.Error(err))
				}

				if err := s.Db.DbRClose(); err != nil {
					accessLogger.Error("dbr close err", zap.Error(err))
				}
			}
		},

		// 关闭 cache
		func() {
			if s.Cache != nil {
				if err := s.Cache.Close(); err != nil {
					accessLogger.Error("cache close err", zap.Error(err))
				}
			}
		},

		// 关闭 cron Server
		func() {
			if s.CronServer != nil {
				s.CronServer.Stop()
			}
		},
	)

}
