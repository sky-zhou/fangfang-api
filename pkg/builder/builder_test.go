package builder

import (
	"fmt"
	"testing"
)

//测试前说明，key值第一个字母，必须要大写！！！
func TestStruct(t *testing.T) {
	pe := NewBuilder().
		AddString("Name").
		AddInt64("Age").
		Build()
	p := pe.New()
	p.SetString("Name", "你好")
	p.SetInt64("Age", 32)
	fmt.Printf("%+v\n", p)
	fmt.Printf("%T,%+v\n", p.Interface(), p.Interface())
	fmt.Printf("%T,%+v\n", p.Addr(), p.Addr())

}
