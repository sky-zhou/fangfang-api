package env

import (
	"flag"
	"os"
	"strings"
)

var (
	active Environment
	dev    Environment = &environment{value: "dev"}
	test   Environment = &environment{value: "test"}
	fat    Environment = &environment{value: "fat"}
	pro    Environment = &environment{value: "pro"}
)

var _ Environment = (*environment)(nil)

// Environment 环境配置
type Environment interface {
	Value() string
	IsDev() bool
	IsTest() bool
	IsFat() bool
	IsPro() bool
	t()
}

type environment struct {
	value string
}

func (e *environment) Value() string {
	return e.value
}

func (e *environment) IsDev() bool {
	return e.value == "dev"
}

func (e *environment) IsTest() bool {
	return e.value == "test"
}

func (e *environment) IsFat() bool {
	return e.value == "fat"
}

func (e *environment) IsPro() bool {
	return e.value == "pro"
}

func (e *environment) t() {}

func init() {
	env := flag.String("ENV", "", "请输入运行环境:\n dev:开发环境\n test:测试环境\n fat:预上线环境\n pro:正式环境\n")
	flag.Parse()
	//var env = "dev"
	//增加针对环境变量的判断
	if strings.ToLower(strings.TrimSpace(*env)) == "" {
		e := strings.ToLower(os.Getenv("ENV"))
		env = &e
	}
	switch strings.ToLower(strings.TrimSpace(*env)) {
	case "dev":
		active = dev
	case "test":
		active = test
	case "fat":
		active = fat
	case "pro":
		active = pro
	default:
		active = dev
	}
}

// Active 当前配置的env
func Active() Environment {
	return active
}
