package utils

import (
	"encoding/json"
	"strings"
)

func CopeyMapToStruct(isNeedLower bool, input interface{}, output interface{}) error {
	//传入的map
	m := input.(map[string]interface{})
	//转小写后的map
	var r = make(map[string]interface{})

	if isNeedLower {
		for k := range m {
			r[strings.ToLower(k)] = m[k]
		}
	} else {
		r = m
	}

	//因为直接通过map转struct 会导致带_ 字段无数据
	//所以改用json方式进行转换
	val, err := json.Marshal(r)
	if err != nil {
		return err
	}

	err = json.Unmarshal(val, output)
	if err != nil {
		return err
	}
	return nil
}

func CopeyListMapToStruct(isNeedLower bool, input interface{}, output interface{}) error {
	//传入的map
	m := input.([]map[string]interface{})
	//转小写后的map
	var r = []map[string]interface{}{}

	if isNeedLower {
		for k := range m {
			obj := m[k]
			ri := make(map[string]interface{})
			for key := range obj {
				ri[strings.ToLower(key)] = obj[key]
			}
			r = append(r, ri)
		}
	} else {
		r = m
	}

	//因为直接通过map转struct 会导致带_ 字段无数据
	//所以改用json方式进行转换
	val, err := json.Marshal(r)
	if err != nil {
		return err
	}

	err = json.Unmarshal(val, output)
	if err != nil {
		return err
	}
	return nil
}
