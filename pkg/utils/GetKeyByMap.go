package utils

func CheckKeyByMap(m map[string]interface{}, key string) bool {
	for k := range m {
		if k == key {
			return true
		}
	}
	return false
}
