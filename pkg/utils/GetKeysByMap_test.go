package utils

import (
	"fmt"
	"testing"
)

func TestGetkeysByMap(t *testing.T) {
	var m = make(map[string]interface{})
	m["a"] = "a"
	m["b"] = 1
	m["c"] = 2.3

	var b = make(map[string]interface{})
	b["d"] = 12
	m["e"] = b

	f := GetkeysByMap(m)
	fmt.Print(f)
}
