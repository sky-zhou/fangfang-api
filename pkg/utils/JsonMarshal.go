package utils

import "encoding/json"

func JsonMarshal(data interface{}) (jsonRaw []byte) {
	jsonRaw, _ = json.Marshal(data)
	return
}
