package utils

import (
	"reflect"
	"strconv"
	"strings"
)

func ListStringToString(s []string) (str string) {
	for i := range s {
		if i+1 == len(s) {
			str = str + "'" + s[i] + "'"
		} else {
			str = str + "'" + s[i] + "'" + ","
		}

	}
	return str
}

func ListToString(s []string) (str string) {
	for i := range s {
		if i+1 == len(s) {
			str = str + s[i]
		} else {
			str = str + s[i] + ","
		}

	}
	return str
}

func ListIntToString(s []int) (str string) {
	var b []string
	for _, i := range s {
		b = append(b, strconv.Itoa(i))
	}
	return strings.Join(b, ",")
}

func PraseInterfaceToString(val interface{}) string {
	if val == nil {
		return ""
	}
	switch reflect.TypeOf(val).String() {
	case "float64":
		return strconv.FormatFloat(val.(float64), 'f', -1, 64)
	case "float32":
		return strconv.FormatFloat(float64(val.(float32)), 'f', -1, 64)
	case "int64":
		return strconv.FormatInt(val.(int64), 10)
	case "int":
		return strconv.Itoa(val.(int))
	case "string":
		return val.(string)
	}

	return ""
}
