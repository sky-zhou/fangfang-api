package main

import (
	"time"
)

type User struct {
	ID        uint   `json:"ID"`
	UserName  string `json:"UserName"`
	Password  string
	CreatedAt time.Time
	UpdatedAt time.Time
}

func (*User) TableName() string {
	return "sys_admin_user"
}

//func main() {
//
//	dsn := "root:Xhsoftware123@tcp(xh.skyzhou.cn:3306)/fangfang_api?charset=utf8mb4&parseTime=True&loc=Local"
//	db, _ := gorm.Open(mysql.Open(dsn), &gorm.Config{})
//
//	user := User{UserName: "Jinzhu", Password: "abc123"}
//
//	result := db.Create(&user).Error // 通过数据的指针来创
//	if result == nil {
//		str, _ := json.Marshal(user)
//		println(string(str))
//	}
//
//	defer func() {
//		sqlDB, err := db.DB()
//		if err == nil {
//			sqlDB.Close()
//		}
//	}()
//
//}
